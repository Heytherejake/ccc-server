package com.studio878.server.achievement;

public class AchievementManager {

	public enum Achievement {
		
		BuyGame (1, "Customer Appreciation Day", 1),
		SelfExplosion (2, "We Have Met The Enemy, And He Is Us", 100),
		BloodParticles (3, "And That's Why You Leave A Note", 10000),
		;
		
		int id, goal;
		String name;
		
		private Achievement(int id, String name, int goal) {
			this.id = id;
			this.name = name;
			this.goal = goal;
		}
	}
}
