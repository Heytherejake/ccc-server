package com.studio878.server.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.studio878.server.logging.Console;

public class API {

	public static JSONArray getArray(String url) {
	//	Console.log(url);
		try {
			URLConnection conn = null;

				URL urls = new URL(url);
				conn = urls.openConnection();
				conn.setDoOutput(true);
				conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
				conn.setConnectTimeout(1000);

				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line, total = "";
				while((line = rd.readLine()) != null) {
					total += line;
				}
				Object obj = JSONValue.parse(total);
				JSONArray content = (JSONArray) obj;
				return content;
		} catch (Exception e) {
			Console.error("Could not connect to api.studio878software.com");
			return null;
		}
		
	}
	
	public static JSONObject getObject(String url) {
		//Console.log(url);
		URLConnection conn = null;
		try {

			URL urls = new URL(url);
			conn = urls.openConnection();
			conn.setDoOutput(true);
			conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line, total = "";
			while((line = rd.readLine()) != null) {
				total += line;
			}
			Object obj = JSONValue.parse(total);
			JSONObject content = (JSONObject) obj;
			return content;
		} catch (Exception e) {
			Console.error("Could not connect to api.studio878software.com");
			return null;
		}
	}
}
