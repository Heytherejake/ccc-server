package com.studio878.server.util;

import java.util.HashMap;

public class Emote {

	public enum EmoteType {
		Happy (1),
		Angry (2),
		Disapproval (3),
		Left (4),
		Right (5),
		Stop (6),
		Thumbs_Up (7),
		Thumbs_Down(8),
		;
		
		int id;
		
		private static HashMap<Integer, EmoteType> idLookup = new HashMap<Integer, EmoteType>();

		private EmoteType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static EmoteType lookupId(int i) {
			return idLookup.get(i);
		}
		
		static {
			for(EmoteType t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}
}
