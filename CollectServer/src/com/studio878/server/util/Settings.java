package com.studio878.server.util;

import java.io.File;
import java.util.Random;

import com.studio878.server.logging.Console;


public class Settings {
	public static String SystemFolder;
	public static String Version = "1.32";
	public static String Build = "1";
	public static String BuildDate = "4/7/2012";

	public static int WorldHeight = 400;
	public static int WorldWidth = 400;
	public static int WorldBuildThreads;
	
	public static double Gravity = 0.13;
	public static double Friction = 0.90;
	

	public static PropertiesReader PropertiesFile;
	public static int MaxClients;
	public static int Port;
	public static String ServerName;
	public static boolean DebugMode;
	public static boolean StoryMode;
	public static String StoryMap;
	public static boolean ClientSideMovement;
	
	public static boolean FriendlyFire = false;
	public static int RespawnTime = 15;
	public static int NewRoundTime = 30;
	public static boolean AllowInAirMovement = true;
	
	public static long LaunchTime = System.currentTimeMillis();
	
	private static String SeedString;
	private static Random SeedGenerator;
	
	public static int[] StoryVariables = new int[100];
	


	static {
		if(System.getProperty("system_folder") != null) {
			SystemFolder = System.getProperty("system_folder") + File.separator;
		} else {
			SystemFolder = "";
		}
		PropertiesFile = new PropertiesReader(SystemFolder + "server_settings.properties");
		MaxClients = PropertiesFile.readInt("max-players", 10);
		Port = PropertiesFile.readInt("port", 31337);
		ServerName = PropertiesFile.readString("name", "My CCC Server!");
		SeedString = PropertiesFile.readString("seed");
		DebugMode = PropertiesFile.readBoolean("debug", false);
		FriendlyFire = PropertiesFile.readBoolean("friendlyfire", false);
		RespawnTime = PropertiesFile.readInt("respawntime", 7);
		NewRoundTime = PropertiesFile.readInt("newroundtime", 15);
		AllowInAirMovement = PropertiesFile.readBoolean("inairmovement", true);
		WorldWidth = PropertiesFile.readInt("width", 400);
		WorldHeight = PropertiesFile.readInt("height", 400);
		StoryMode = PropertiesFile.readBoolean("storymode", false);
		StoryMap = PropertiesFile.readString("map", "");
		WorldBuildThreads = PropertiesFile.readInt("buildthreads", 6);
		ClientSideMovement = PropertiesFile.readBoolean("client-movement", false);

		if(SeedString != null) {
			SeedGenerator = new Random(SeedString.hashCode());
		} else {
			SeedGenerator = new Random();
		}
	}


	public static void getNewSeed() {
		char[] digits = new char[12];
		digits[0] = (char) (SeedGenerator.nextInt(9) + '1');
		for (int i = 1; i < 12; i++) {
			digits[i] = (char) (SeedGenerator.nextInt(10) + '0');
		}
		SeedString = "" + Long.parseLong(new String(digits));
	}

	public static long getSeed() {
		if(SeedString == null || SeedString.length() == 0) {
			Random random = new Random();
			char[] digits = new char[12];
			digits[0] = (char) (random.nextInt(9) + '1');
			for (int i = 1; i < 12; i++) {
				digits[i] = (char) (random.nextInt(10) + '0');
			}
			return ("" + Long.parseLong(new String(digits))).hashCode();
		} else {
			return Settings.SeedString.hashCode();
		}
	}
}
