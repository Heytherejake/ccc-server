package com.studio878.server.util;

import java.util.HashMap;


public class Damage {
	public static enum DamageMethod {
		Grenade (0),
		FallDamage(1),
		Pistol (2),
		Shovel (3),
		Suicide (4),
		SMG (5),
		Shotgun (6),
		SniperRifle (7),
		Rocket (8),
		Block (9),
		Generic (10),
		Landmine (11),
		Wrench (12), 
		Sword(13),
		Hatchet(14),
		Lava(15),
		OutOfBounds(16),
		;
		private static HashMap<Integer, DamageMethod> idLookup = new HashMap<Integer, DamageMethod>();
		private int id;
		private DamageMethod(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static DamageMethod lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(DamageMethod t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}
}
