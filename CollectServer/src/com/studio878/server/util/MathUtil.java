package com.studio878.server.util;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class MathUtil {

	public static ArrayList<Line2D> getLinesFromRectangle(Rectangle r) {
		ArrayList<Line2D> lines = new ArrayList<Line2D>();
		lines.add(new Line2D.Double(r.getX(), r.getY(), r.getX() + r.getWidth(), r.getY()));
		lines.add(new Line2D.Double(r.getX() + r.getWidth(), r.getY(), r.getX() + r.getWidth(), r.getY() + r.getHeight()));
		lines.add(new Line2D.Double(r.getX(), r.getY() + r.getHeight() , r.getX() + r.getWidth(), r.getY() + r.getHeight()));
		lines.add(new Line2D.Double(r.getX(), r.getY() + r.getHeight() , r.getX(), r.getY()));
		return lines;

	}
}
