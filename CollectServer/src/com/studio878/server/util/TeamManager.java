package com.studio878.server.util;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

import com.studio878.server.app.Main;
import com.studio878.server.entities.Player;

public class TeamManager {

	public enum Team {
		Red (0),
		Blue (1),
		;
		
		int id;
		private static HashMap<Integer, Team> idLookup = new HashMap<Integer, Team>();

		private Team(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static Team lookupId(int i) {
			return idLookup.get(i);
		}
		
		static {
			for(Team t: values()) {
				idLookup.put(t.getId(), t);
			}
		}
	}

	private static Point Red_Respawn = new Point(0, 0);
	private static Point Blue_Respawn = new Point(0, 0);
	public static ArrayList<Player> getMembersOfTeam(Team t) {
		ArrayList<Player> result = new ArrayList<Player>();
		for(Player p : Main.getServer().getPlayers()) {
			if(p != null && p.getTeam() == t) {
				result.add(p);

			}
		}
		return result;
	}

	public static void assignTeam(Player p) {
		if(Settings.StoryMode) {
			p.setTeam(Team.Blue);
			return;
		}
		int rm = getTeamSize(Team.Red);
		int bm = getTeamSize(Team.Blue);
		if(rm > bm) {
			p.setTeam(Team.Blue);
		} else {
			p.setTeam(Team.Red);
		}
	}

	public static int getTeamSize(Team t) {
		return getMembersOfTeam(t).size();
	}

	public static void setTeamRespawnPoint(Team t, Point p) {
		if(t == Team.Red) {
			Red_Respawn = p;
		} else if (t == Team.Blue){
			Blue_Respawn = p;
		}
	}
	
	public static Point getTeamRespawnPoint(Team t) {
		if(t == Team.Red) {
			return Red_Respawn;
		} else if (t == Team.Blue) {
			return Blue_Respawn;
		}
		return null;
	}
	
	public static Team matchTeam(String s) {
		if(s.equalsIgnoreCase("Red")) {
			return Team.Red;
		} else if(s.equalsIgnoreCase("Blue")) {
			return Team.Blue;
		}
		return null;
	}
}
