package com.studio878.server.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

import com.studio878.server.block.BlockType;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.input.InputThread;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.ClientListener;
import com.studio878.server.io.ClientSender;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.io.ServerDispatch;
import com.studio878.server.listeners.PluginDispatch;
import com.studio878.server.logging.Console;
import com.studio878.server.thread.ElectricThread;
import com.studio878.server.thread.FluidThread;
import com.studio878.server.thread.LogicThread;
import com.studio878.server.util.API;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class Main {


	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static ServerDispatch dispatch;
	static Server server;
	static Timer timer = new Timer("main");
	static ServerSocket socket = null;
	public static boolean isRunning = true;
	public static ServerDispatch getDispatch() {
		return dispatch;
	}
	public static void main(String[] args) {
		
		try {
			Console.write("Collect, Construct, Conquer! Server (Version " + Settings.Version + ") Build " + Settings.Build + " - " + Settings.BuildDate);
			Console.write("(C)2011-2012 Studio878 Software Laboratories. Do not distribute.");
			Console.write("Preparing to setup...");
			Console.write("Loading settings");

			try {
				if(socket != null && socket.isClosed()) {
					socket = null;
				}
				socket = new ServerSocket(Settings.Port);
				Console.write("Server started on port " + Settings.Port);
			} catch (IOException e) {
				Console.error("Could not start server on port " + Settings.Port + ". Is another server running?");
				return;
			}

			File mapFolder = new File("maps");
			mapFolder.mkdirs();


			Console.log("Creating item database...");
			ItemID.populateDefaultItems();
			
			Console.log("Creating block database...");
			BlockType.populateDefaultBlocks();
			
			Console.log("Creating recipe database...");
			CraftingRecipeList.populateDefaultRecipes();
			
			dispatch = new ServerDispatch();
			dispatch.start();
			server = new Server(dispatch);

			InputThread input = new InputThread();
			input.start();

			LogicThread logic = new LogicThread();
			logic.start();
			
			FluidThread fluids = new FluidThread();
			fluids.start();
			
			ElectricThread electrics = new ElectricThread();
			electrics.start();



			Server.loadOps();
			PluginDispatch.loadAllPlugins();

			new Thread() {
				public void run() {

					try {
						URL getip = new URL("http://api.studio878software.com/util/ip");
						BufferedReader in = new BufferedReader(new InputStreamReader(getip.openStream()));
						String ext_ip = in.readLine();
						URLConnection conn = null;
						String ssn = Settings.ServerName.replace(' ', '_');
						API.getObject("http://api.studio878software.com/ccc/network/server/add/" + ext_ip + "/" + Settings.Port + "/" + ssn);
						Console.write("Added to server list.");

					} catch (Exception e) {
						Console.write("Unable to connect to studio878software.com. Server will not show up on the list.");
					}

				}
			}.start();
			
			if(Settings.StoryMode) {
				Grid.loadMap(Settings.StoryMap);
			} else {
				Server.startNewRound();
			}
			while(isRunning) {

				try {
					Socket s = socket.accept();
					NetPlayer ci = new NetPlayer();
					ci.setSocket(s);
					ClientListener cl = new ClientListener(ci, dispatch);
					ClientSender cs = new ClientSender(ci, dispatch);
					ci.setListener(cl);
					ci.setSender(cs);
					ci.setDispatch(dispatch);
					cl.start();
					cs.start();
					dispatch.addClient(ci);



				} catch (IOException ee) {
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Server getServer() {
		return server;
	}

	public static void registerTimerTask(TimerTask t, long delay) {
		timer.schedule(t, delay);
	}



	public static void softShutdown() {
		try {
			Console.write("Closing all connections...");
			socket.close();
			dispatch.interrupt();
			isRunning = false;
		} catch (IOException e) {
			Console.error("Failed to close connections");
			e.printStackTrace();
		}
	}
	public static void shutdown() {
		Console.write("Shutting down server...");
		System.exit(0);
	}


}
