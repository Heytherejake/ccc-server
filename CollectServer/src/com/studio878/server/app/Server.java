package com.studio878.server.app;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Generator;
import com.studio878.server.entities.Player;
import com.studio878.server.entities.ResearchBench;
import com.studio878.server.entities.Workbench;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.io.ServerDispatch;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet04SendGrid;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.packets.Packet10SendEntityList;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager;
import com.studio878.server.util.TeamManager.Team;
import com.studio878.server.world.Grid;

public class Server {
	protected ServerDispatch dispatch;

	public static CopyOnWriteArrayList<String> ops = new CopyOnWriteArrayList<String>();
	public Server(ServerDispatch d) {
		dispatch = d;
	}

	public ArrayList<Player> getPlayers() {
		ArrayList<NetPlayer> nps = dispatch.getClients();
		ArrayList<Player> results = new ArrayList<Player>();
		for(int i = 0; i < nps.size(); i++) {
			results.add(nps.get(i).getPlayer());
		}
		return results;
	}

	public void broadcastMessage(String message) {
		if(dispatch.getClients().size() > 0) {
			dispatch.broadcastPacket(new Packet08SendMessage(dispatch.getClients().get(0), message));
		}
	}

	public Player getPlayer(String name) {
		ArrayList<NetPlayer> nps = dispatch.getClients();
		for(int i = 0; i < nps.size(); i++) {
			if(nps.get(i).getPlayer().getName().equalsIgnoreCase("name")) {
				return nps.get(i).getPlayer();
			}
		}
		return null;
	}

	public static void startNewRound() {
		Console.write("Starting new round...");

		EntityManager.reset();

		Settings.getNewSeed();
		
		Block.fluidList.clear();
		Block.wireBlocks.clear();
		
		Grid.generateGrid(Settings.WorldWidth, Settings.WorldHeight);
		Console.write("Generating Blue spawn and generator");
		//Generate Blue Spawn and Objective
		int ystart = Grid.getSpawnLocation(300) - 20;

		Generator g = new Generator(300, ystart, Team.Blue);
		EntityManager.registerEntity(g);

		Workbench ws = new Workbench(300, ystart - 140);
		EntityManager.registerEntity(ws);

		ResearchBench rs = new ResearchBench(375, ystart - 140);
		EntityManager.registerEntity(rs);

		for(int i = 0; i < g.getWidth(); i+= 16) {

			Block b = Grid.getClosestBlock(300 + i, ystart + g.getHeight());
			b.setType(BlockType.lookupName("Titanium Block"));

			for(int j = 64; j < 160; j += 16) {
				Block bsf = Grid.getClosestBlock(300 + i, ystart - j);
				bsf.setType(BlockType.lookupName("Blue Barrier"));
			}

			for(int j = -112; j < 160; j += 16) {
				Block bsa = Grid.getClosestBlock(300 + i, ystart - j);
				bsa.setBackgroundType(BlockType.lookupName("Titanium Block"));
			}


			Block bsd = Grid.getClosestBlock(300 + i, ystart - 64);
			bsd.setType(BlockType.lookupName("Titanium Block"));

			Block bse = Grid.getClosestBlock(300 + i, ystart - 160);
			bse.setType(BlockType.lookupName("Titanium Block"));
			for(int j = -48; j < g.getHeight(); j += 16) {
				Block bs = Grid.getClosestBlock(300 + i, ystart + j);
				bs.setType(BlockType.lookupName("Air"));
			}
		}
		for(int i = 0 ; i < g.getHeight() + 160; i++) {
			Block b = Grid.getClosestBlock(300 - 16, ystart  - 160 + i);
			b.setType(BlockType.lookupName("Titanium Block"));
		}
		TeamManager.setTeamRespawnPoint(Team.Blue, new Point(300, ystart - 160));

		//Generate Red Spawn and Objective
		Console.write("Generating Red spawn and generator");

		int xstart = Settings.WorldWidth*16 - 300;
		ystart = Grid.getSpawnLocation(xstart) - 20;

		Generator gs = new Generator(xstart, ystart, Team.Red);
		EntityManager.registerEntity(gs);

		ws = new Workbench(xstart + 160, ystart - 140);
		EntityManager.registerEntity(ws);
		rs = new ResearchBench(xstart + 85, ystart - 140);
		EntityManager.registerEntity(rs);

		for(int i = 0; i < g.getWidth(); i+= 16) {

			Block b = Grid.getClosestBlock(xstart + i, ystart + g.getHeight());
			b.setType(BlockType.lookupName("Titanium Block"));

			for(int j = 64; j < 160; j += 16) {
				Block bsf = Grid.getClosestBlock(xstart + i, ystart - j);
				bsf.setType(BlockType.lookupName("Red Barrier"));
			}

			for(int j = -112; j < 160; j += 16) {
				Block bsa = Grid.getClosestBlock(xstart + i, ystart - j);
				bsa.setBackgroundType(BlockType.lookupName("Titanium Block"));
			}


			Block bsd = Grid.getClosestBlock(xstart + i, ystart - 64);
			bsd.setType(BlockType.lookupName("Titanium Block"));

			Block bse = Grid.getClosestBlock(xstart + i, ystart - 160);
			bse.setType(BlockType.lookupName("Titanium Block"));
			for(int j = -48; j < g.getHeight(); j += 16) {
				Block bs = Grid.getClosestBlock(xstart + i, ystart + j);
				bs.setType(BlockType.lookupName("Air"));
			}
		}
		for(int i = 0 ; i < g.getHeight() + 160; i++) {
			Block b = Grid.getClosestBlock(xstart + gs.getWidth() + 16, ystart  - 160 + i);
			b.setType(BlockType.lookupName("Titanium Block"));
		}
	/*	for(int i = 0; i < Settings.WorldWidth; i++) {
			if(i == 0 || i == Settings.WorldWidth - 1) {
				for(int j = 0; j < Settings.WorldHeight; j++) {
					Block b = Grid.getBlockAt(i, j);
					b.setType(BlockType.lookupName("Titanium Block"));
				}
			}
			Block b = Grid.getBlockAt(i, 0);
			b.setType(BlockType.lookupName("Titanium Block"));
			Block bs = Grid.getBlockAt(i, Settings.WorldHeight- 1);
			bs.setType(BlockType.lookupName("Titanium Block"));
		} */
		TeamManager.setTeamRespawnPoint(Team.Red, new Point(xstart + g.getWidth() - 16, ystart - 160));


		for(Player p : EntityManager.getPlayers()) {
			p.respawn();
		}

		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet10SendEntityList(Main.getDispatch().getClients().get(0)));
			Main.getDispatch().broadcastPacket(new Packet04SendGrid(Main.getDispatch().getClients().get(0)));


		}
		Console.write("...New round started!");
	}

	public static void addDefaultOp(String s) {
		System.out.println(s);
		ops.add(s);
		System.out.println(ops.size());
	}
	public static void loadOps() {
		ops.clear();
		File f = new File(Settings.SystemFolder + "ops.txt");
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				Console.error("Failed to create ops.txt");
			}
		} else {
			try{
				FileInputStream fs = new FileInputStream(f);
				DataInputStream din = new DataInputStream(fs);
				BufferedReader br = new BufferedReader(new InputStreamReader(din));
				String s;
				while ((s = br.readLine()) != null) {
					if(s.length() > 0 && !s.substring(0, 1).equals("#")) {
						ops.add(s);
					}
				}
				br.close();
				din.close();
				fs.close();
			}catch (Exception e){
				Console.error("An error occured while loading the operator list");
				e.printStackTrace();
			}
		}
	}
	public Player matchPlayer(String name) {
		try {
			ArrayList<Player> matchedPlayers = new ArrayList<Player>();
			ArrayList<Player> players = getPlayers();
			for(int i = 0; i < players.size(); i++) {
				String tgn = players.get(i).getName();
				if (tgn.length() > 0) {
					if (name.equalsIgnoreCase(tgn)) {
						return players.get(i);
					}
					if (tgn.toLowerCase().indexOf(name) != -1) {
						matchedPlayers.add(players.get(i));
					}
				}
			}
			try {
				return matchedPlayers.get(0);
			} catch (Exception e) {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static void saveOps() {
		try {
			FileWriter fw = new FileWriter("ops.txt");
			BufferedWriter w = new BufferedWriter(fw);
			for(String o : ops) {
				w.write(o);
				w.newLine();
			}
			w.close();
			fw.close();
		} catch (IOException e) {
			Console.write("An error occured saving the operator list.");
		}
	}

}
