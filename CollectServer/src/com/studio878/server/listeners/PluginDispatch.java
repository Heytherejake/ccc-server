package com.studio878.server.listeners;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.studio878.server.logging.Console;
import com.studio878.server.util.JarLoader;
import com.studio878.server.util.Settings;

public final class PluginDispatch {
	public static ArrayList<Plugin> listeners = new ArrayList<Plugin>();
	private static final Object lock = new Object();


	public static void addListener(Plugin e) {
		listeners.add(e);
	}

	public static void removeListener(Plugin e) {
		listeners.remove(e);
	}

	public static ArrayList<Plugin> getListeners() {
		return listeners;
	}

	public static void loadAllPlugins() {
		try {
			File f = new File(Settings.SystemFolder + "plugins" + File.separator);
			if(!f.exists()) {
				f.mkdirs();
			}
			String[] files = f.list();
			for(int i = 0; i < files.length; i++) {
				loadPlugin(files[i]);
			}
			Console.write("All plugins loaded");
		} catch (Exception e) {
			Console.write("Failed to load all plugins");
			e.printStackTrace();
		}
	}
	public static Plugin findPlugin(String filename) {
		for(int i = 0; i < listeners.size(); i++) {
			if(listeners.get(i).getPluginInfo().getFilename().equalsIgnoreCase(filename + ".jar")) {
				return listeners.get(i);
			}
		}
		return null;
	}
	public static boolean reloadPlugin(String file) {
		Plugin p = PluginDispatch.findPlugin(file);
		if(p != null) {
			if(offloadPlugin(file)) {
				loadPlugin(p.getPluginInfo().getFilename());
				return true;
			}
		}
		return false;

	}

	public static boolean offloadPlugin(String file) {
		Plugin p = PluginDispatch.findPlugin(file);
		if(p != null) {
			HookRegistry.removeListenersOfPlugin(p);
			synchronized(lock) {
				listeners.remove(p);
				p.onDisable();
			}
			return true;
		}
		return false;

	}
	public static ArrayList<Plugin> getPluginList() {
		return listeners;
	}

	public static boolean loadPlugin(String filename) {
		Plugin p = PluginDispatch.findPlugin(filename.split("\\.")[0]);
		if(p != null) {
			return false;
		}
		try {
			File f = new File("plugins/" + filename);
			if(!f.exists()) {
				Console.write(filename + " does not exist.");
				return false;
			}
			JarFile jar = new JarFile(f);

			

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			JarEntry entry = jar.getJarEntry("plugin.xml");
			if(entry == null) {
				Console.write(filename + " is missing plugin.xml! Contact the author immediately.");
				return false;
			}
			Document xml = docBuilder.parse(jar.getInputStream(entry));
			xml.getDocumentElement().normalize();
			Node n = xml.getElementsByTagName("info").item(0);
			Element elem = (Element) n;
			String name = getTagValue("name", elem);
			String main = getTagValue("main", elem);
			String version = getTagValue("version", elem);
			String author = getTagValue("author", elem);
			String desc = getTagValue("description", elem);
			URLClassLoader ucl = null;
			try {
				ucl = new JarLoader(new URL[]{f.toURI().toURL()}, Thread.currentThread().getContextClassLoader());
			} catch (MalformedURLException mce) {
				Console.write("Plugin " + filename + " is missing it's main class.");
				return false;
			}
			Class ctc = ucl.loadClass(main);
			Plugin np = (Plugin) ctc.newInstance();
			synchronized (lock) {
				np.info = new PluginInfo(name, main, version, author, desc, filename);
				np.onEnable();
			}
			listeners.add(np);
			return true;

		} catch (Exception e) {
			Console.write(filename + " failed to load.");
			e.printStackTrace();
			return false;
		}

	}
	private static String getTagValue(String sTag, Element eElement){
		NodeList nlList= eElement.getElementsByTagName(sTag).item(0).getChildNodes();
		Node nValue = (Node) nlList.item(0); 

		String s =  nValue.getNodeValue();  
		if(s != null) {
			return s;
		} else {
			return "";
		}
	}


}
