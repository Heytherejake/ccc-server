package com.studio878.server.listeners;

import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.listeners.Hooks.Priority;

public class RegistryObject implements Comparable<RegistryObject> {
	Priority priority;
	Plugin plugin;
	Hook hook;
	Listener listener;
	
	public RegistryObject(Priority p, Plugin pl, Hook h, Listener l) {
		priority = p;
		plugin = pl;
		hook = h;
		listener = l;
	}
	
	public Priority getPriority() {
		return priority;
	}
	
	public Plugin getPlugin() {
		return plugin;
	}
	
	public Hook getHook() {
		return hook;
	}
	
	public Listener getListener() {
		return listener;
	}

	public int compareTo(RegistryObject arg0) {
		return priority.compareTo(arg0.getPriority());
	}

}
