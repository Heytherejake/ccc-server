package com.studio878.server.listeners;

import com.studio878.server.events.EntityDamageEvent;
import com.studio878.server.events.EntityHealEvent;
import com.studio878.server.events.EntityKilledEvent;

public class EntityListener implements Listener{

	
	public void onDamage(EntityDamageEvent ev) {
		
	}
	
	public void onHeal(EntityHealEvent ev) {
		
	}
	
	public void onKill(EntityKilledEvent ev) {
		
	}
}
