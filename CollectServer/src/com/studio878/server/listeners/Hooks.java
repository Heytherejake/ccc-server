package com.studio878.server.listeners;

import java.util.ArrayList;

import com.studio878.server.events.BlockCreatedEvent;
import com.studio878.server.events.BlockDamagedEvent;
import com.studio878.server.events.BlockDestroyedEvent;
import com.studio878.server.events.CraftingSuccessEvent;
import com.studio878.server.events.EntityDamageEvent;
import com.studio878.server.events.EntityHealEvent;
import com.studio878.server.events.EntityKilledEvent;
import com.studio878.server.events.Event;
import com.studio878.server.events.InventoryRequestEvent;
import com.studio878.server.events.PlayerChangeTeamEvent;
import com.studio878.server.events.PlayerChatEvent;
import com.studio878.server.events.PlayerCommandEvent;
import com.studio878.server.events.PlayerJoinEvent;
import com.studio878.server.events.PlayerKickedEvent;
import com.studio878.server.events.PlayerKilledEvent;
import com.studio878.server.events.ProjectileThrownEvent;
import com.studio878.server.events.ServerCommandEvent;
import com.studio878.server.events.WeaponFiredEvent;
import com.studio878.server.logging.Console;

public final class Hooks {

	public static enum Priority {
		Critical (3),
		High (2),
		Medium (1),
		Low(0);

		private int p;
		private Priority(int i) {
			p = i;
		}

		public int getLevel() {
			return p;
		}
	}

	public static enum Type {
		Block,
		Player,
		Server,
		Entity,
		;
	}
	public static enum Hook {
		BLOCK_DESTROY (Type.Block),
		BLOCK_DAMAGED (Type.Block),
		BLOCK_CREATED (Type.Block),

		SERVER_COMMAND (Type.Server),

		PLAYER_CHAT (Type.Player),
		PLAYER_COMMAND (Type.Player),
		WEAPON_FIRED (Type.Player),
		CRAFTING_SUCCEEDED (Type.Player),
		PROJECTILE_THROWN (Type.Player),
		PLAYER_KILLED (Type.Player), 
		PLAYER_KICKED (Type.Player),
		PLAYER_CHANGED_TEAM (Type.Player),
		INVENTORY_REQUESTED (Type.Player),
		PLAYER_JOIN (Type.Player),

		ENTITY_DAMAGED (Type.Entity),
		ENTITY_HEALED (Type.Entity),
		ENTITY_KILLED (Type.Entity),
		;

		private Type type;

		private Hook(Type t) {
			type = t;
		}

		public Type getType() {
			return type;
		}
	}
	public static boolean call(Hook hook, Event ev) {
		try {
			ArrayList<Plugin> l = HookRegistry.getPluginsWithHook(hook);
			for(Plugin pl : l) {
				RegistryObject o = HookRegistry.getRegistryObject(pl, hook);

				switch(hook) {
				case BLOCK_DESTROY:
					((BlockListener) o.getListener()).onBlockDestroy((BlockDestroyedEvent) ev);
					break;
				case BLOCK_DAMAGED:
					((BlockListener) o.getListener()).onBlockDamage((BlockDamagedEvent) ev);
					break;
				case BLOCK_CREATED:
					((BlockListener) o.getListener()).onBlockCreated((BlockCreatedEvent) ev);
					break;
				case PLAYER_CHAT:
					((PlayerListener) o.getListener()).onPlayerChat((PlayerChatEvent) ev);
					break;
				case PLAYER_COMMAND:
					((PlayerListener) o.getListener()).onPlayerCommand((PlayerCommandEvent) ev);
					break;
				case SERVER_COMMAND:
					((ServerListener) o.getListener()).onServerCommand((ServerCommandEvent) ev);
					break;
				case WEAPON_FIRED:
					((PlayerListener) o.getListener()).onWeaponFired((WeaponFiredEvent) ev);
					break;
				case CRAFTING_SUCCEEDED:
					((PlayerListener) o.getListener()).onCraftingSuccess((CraftingSuccessEvent) ev);
					break;
				case PROJECTILE_THROWN:
					((PlayerListener) o.getListener()).onProjectileThrown((ProjectileThrownEvent) ev);
					break;
				case ENTITY_DAMAGED:
					((EntityListener) o.getListener()).onDamage((EntityDamageEvent) ev);
					break;
				case ENTITY_HEALED:
					((EntityListener) o.getListener()).onHeal((EntityHealEvent) ev);
					break;
				case ENTITY_KILLED:
					((EntityListener) o.getListener()).onKill((EntityKilledEvent) ev);
					break;
				case PLAYER_KILLED:
					((PlayerListener) o.getListener()).onPlayerKilled((PlayerKilledEvent) ev);
					break;
				case PLAYER_KICKED:
					((PlayerListener) o.getListener()).onPlayerKick((PlayerKickedEvent) ev);
					break;
				case PLAYER_CHANGED_TEAM:
					((PlayerListener) o.getListener()).onPlayerChangedTeam((PlayerChangeTeamEvent) ev);
					break;
				case INVENTORY_REQUESTED:
					((PlayerListener) o.getListener()).onInventoryRequested((InventoryRequestEvent) ev);
					break;
				case PLAYER_JOIN:
					((PlayerListener) o.getListener()).onPlayerJoin((PlayerJoinEvent) ev);
				}
			}

		} catch (Exception e) {
			Console.write("Failed to parse hook: " + hook);
			e.printStackTrace();
		}
		return !ev.isCancelled();
	}
}
