package com.studio878.server.listeners;

import com.studio878.server.events.CraftingSuccessEvent;
import com.studio878.server.events.InventoryRequestEvent;
import com.studio878.server.events.PlayerChangeTeamEvent;
import com.studio878.server.events.PlayerChatEvent;
import com.studio878.server.events.PlayerCommandEvent;
import com.studio878.server.events.PlayerJoinEvent;
import com.studio878.server.events.PlayerKickedEvent;
import com.studio878.server.events.PlayerKilledEvent;
import com.studio878.server.events.ProjectileThrownEvent;
import com.studio878.server.events.WeaponFiredEvent;

public class PlayerListener implements Listener {
	
	public void onPlayerChat(PlayerChatEvent ev) {
		
	}
	
	public void onPlayerCommand(PlayerCommandEvent ev) {
		
	}
	
	public void onWeaponFired(WeaponFiredEvent ev) {
		
	}
	
	public void onCraftingSuccess(CraftingSuccessEvent ev) {
		
	}
	
	public void onProjectileThrown(ProjectileThrownEvent ev) {
		
	}
	
	public void onPlayerKilled(PlayerKilledEvent ev) {
		
	}
	
	public void onPlayerKick(PlayerKickedEvent ev) {
		
	}

	public void onPlayerChangedTeam(PlayerChangeTeamEvent ev) {
		
	}
	
	public void onInventoryRequested(InventoryRequestEvent ev) {
		
	}
	
	public void onPlayerJoin(PlayerJoinEvent ev) {
		
	}

}
