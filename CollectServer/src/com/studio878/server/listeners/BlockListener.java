package com.studio878.server.listeners;

import com.studio878.server.events.BlockCreatedEvent;
import com.studio878.server.events.BlockDamagedEvent;
import com.studio878.server.events.BlockDestroyedEvent;

public class BlockListener implements Listener{
	
	public void onBlockDestroy(BlockDestroyedEvent ev) {
		
	}
	
	public void onBlockCreated(BlockCreatedEvent ev) {
		
	}
	
	public void onBlockDamage(BlockDamagedEvent ev) {
		
	}

}
