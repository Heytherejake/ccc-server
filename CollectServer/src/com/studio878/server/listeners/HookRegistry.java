package com.studio878.server.listeners;

import java.util.ArrayList;
import java.util.Collections;

import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.listeners.Hooks.Priority;

public final class HookRegistry {
	private static ArrayList<RegistryObject> registry = new ArrayList<RegistryObject>();
	public static void registerHook(Plugin l, Hook h, Priority p, Listener ls) {
		Listener ld = null;
		switch(h.getType()) {
		case Block:
			ld = (BlockListener) ls;
			break;
		case Player:
			ld = (PlayerListener) ls;
			break;
		case Server:
			ld = (ServerListener) ls;
			break;
		case Entity:
			ld = (EntityListener) ls;
		}
		registry.add(new RegistryObject(p, l, h, ld));
	}
	protected static ArrayList<Plugin> getPluginsWithHook(Hook h) {
		ArrayList<Plugin> plugins = new ArrayList<Plugin>();
		Collections.sort(registry);
		for(RegistryObject ro : registry) {
			if(ro.getHook().equals(h)) {
				plugins.add(ro.getPlugin());
			}
		}
		return plugins;
	}
	
	protected static void removeListenersOfPlugin(Plugin p) {
		for(int i = 0; i < registry.size(); i++) {
			if(registry.get(i).plugin == p) {
				registry.remove(i);
				i--;
			}
		}
	}
	protected static ArrayList<RegistryObject> getHooks(Plugin l) {
		ArrayList<RegistryObject> results = new ArrayList<RegistryObject>();
		for(RegistryObject o : registry) {
			if(o.getPlugin() == l) {
				results.add(o);
			}
		}
		return results;
	}
	
	protected static RegistryObject getRegistryObject(Plugin l, Hook h) {
		for(RegistryObject o : registry) {
			if(o.getPlugin() == l && o.getHook() == h) {
				return o;
			}
		}
		return null;
	}
	protected static boolean isHookRegistered(Plugin l, Hook h) {
		for(RegistryObject o : registry) {
			if(o.getPlugin() == l && o.getHook() == h) {
				return true;
			}
		}
		return false;
	}
	
	
}
