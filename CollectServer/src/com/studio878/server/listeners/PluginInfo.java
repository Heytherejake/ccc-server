package com.studio878.server.listeners;

public class PluginInfo {
	String name = "";
	String main = "";
	String version = "";
	String author = "";
	String description = "";
	String filename = "";
	
	public PluginInfo(String n, String m, String v, String a, String d, String f) {
		name = n;
		main = m;
		version = v;
		author = a;
		description = d;
		filename = f;
	}
	
	public String getPluginName() {
		return name;
	}
	
	public String getMainClass() {
		return main;
	}
	
	public String getPluginVersion() {
		return version;
	}
	
	public String getPluginAuthor() {
		return author;
	}
	
	public String getPluginDescription() {
		return description;
	}
	
	public String getFilename() {
		return filename;
	}
	
}
