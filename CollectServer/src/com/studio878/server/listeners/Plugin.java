package com.studio878.server.listeners;

import java.util.ArrayList;

import com.studio878.server.listeners.Hooks.Hook;

public abstract class Plugin {
	PluginInfo info;
	private ArrayList<RegistryObject> registry = new ArrayList<RegistryObject>();
	public void onEnable() {
		
	}
	
	public void onDisable() {
		
	}
	
	public final PluginInfo getPluginInfo() {
		return info;
	}
	



}
