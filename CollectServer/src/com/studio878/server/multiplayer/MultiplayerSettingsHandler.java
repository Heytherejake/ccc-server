package com.studio878.server.multiplayer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;
import com.studio878.server.multiplayer.MultiplayerInventoryObjectConverter.LoadoutItem;
import com.studio878.server.util.API;
import com.studio878.server.util.Settings;

public class MultiplayerSettingsHandler {

	Player player;

	public MultiplayerSettingsHandler(Player p) {
		this.player = p;
	}

	public void updateMultiplayerVariables() {
		try {
			updateAbilities();
			updateActiveCharacter();
		} catch (Exception e) {
			Console.error("Failed to connect to API to retrieve information for " + player.getName());

			e.printStackTrace();
		}
	}


	public void updateAbilities() throws Exception {
		JSONArray inv = API.getArray("http://api.studio878software.com/ccc/network/user/" + player.getName() + "/abilities/list/" + player.getSessionID());
		if(inv == null) return;
		for(Object o : inv) {
			JSONObject jo = (JSONObject) o;
			MultiplayerAbility li = new MultiplayerAbility((String) jo.get("data"),  (Long) jo.get("end_time"), (Long) jo.get("curr_time"), player);
			player.multiplayerAbilities.add(li);
		}
	}

	public void updateActiveCharacter() throws Exception {
		JSONObject inv = API.getObject("http://api.studio878software.com/ccc/network/user/" + player.getName() + "/characters/active/" + player.getSessionID());
		if(inv == null) return;
		if(((String) inv.get("message")) != null) {
			if(((String) inv.get("message")).equalsIgnoreCase("no characters")) {
				Console.write(player.getName() + " could not join (Reason: No active characters)");
				player.kick("You must have a character created to play online.");
				return;
			}
		} else {
			if(!Settings.StoryMode) {
			//	player.setSpecialization(Specialization.lookupId(Integer.parseInt((String) inv.get("type"))));
				player.setActiveCharacterName((String) inv.get("name"));
				player.Loadout.clear();
				if(((String) inv.get("loadout")).length() != 0) {
					String[] split = ((String) inv.get("loadout")).split(",");
					try {
						for(String s : split) {
							int id = Integer.parseInt(s);
							ItemID item = LoadoutItem.lookupId(id).getItem();
							int level = LoadoutItem.lookupId(id).getLevel();
							player.Loadout.add(new Item(item, 1, level, player));
						}
						if(player.Loadout.size() == 0) {
							player.Loadout.add(new Item(ItemID.lookupName("Pistol"), 1, 0, player));
						}
					} catch (Exception e) {
						Console.write(player.getName() + " could not join (Reason: Invalid loadout)");
						player.kick("Your loadout for this character is invalid.");
						if(Settings.DebugMode) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

}
