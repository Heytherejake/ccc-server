package com.studio878.server.multiplayer;

import java.util.HashMap;

import com.studio878.server.inventory.ItemID;

public class MultiplayerInventoryObjectConverter {

	public enum LoadoutItem {
		PistolMkI(1, ItemID.lookupName("Pistol"), 0),
		PistolMkII(2, ItemID.lookupName("Pistol"), 1),
		PistolMkIII(3, ItemID.lookupName("Pistol"), 2),
		PistolMkIV(4, ItemID.lookupName("Pistol"), 3),
		PistolMkV(5, ItemID.lookupName("Pistol"), 4),
		SmgMkI(6, ItemID.lookupName("Submachine Gun"), 0),
		SmgMkII(7, ItemID.lookupName("Submachine Gun"), 1),
		SmgMkIII(8, ItemID.lookupName("Submachine Gun"), 2),
		SmgMkIV(9, ItemID.lookupName("Submachine Gun"), 3),
		SmgMkV(10, ItemID.lookupName("Submachine Gun"), 4),	
		ShotgunMkI(20, ItemID.lookupName("Shotgun"), 0),
		ShotgunMkII(21, ItemID.lookupName("Shotgun"), 1),
		ShotgunMkIII(22, ItemID.lookupName("Shotgun"), 2),
		ShotgunMkIV(23, ItemID.lookupName("Shotgun"), 3),
		ShotgunMkV(24, ItemID.lookupName("Shotgun"), 4),
		RocketLauncherMkI(25, ItemID.lookupName("Rocket Launcher"), 0),
		RocketLauncherMkII(26, ItemID.lookupName("Rocket Launcher"), 1),
		RocketLauncherMkIII(27, ItemID.lookupName("Rocket Launcher"), 2),
		RocketLauncherMkIV(28, ItemID.lookupName("Rocket Launcher"), 3),
		RocketLauncherMkV(29, ItemID.lookupName("Rocket Launcher"), 4),
		SniperRifleMkI(30, ItemID.lookupName("Sniper Rifle"), 0),
		SniperRifleMkII(31, ItemID.lookupName("Sniper Rifle"), 1),
		SniperRifleMkIII(32, ItemID.lookupName("Sniper Rifle"), 2),
		SniperRifleMkIV(33, ItemID.lookupName("Sniper Rifle"), 3),
		SniperRifleMkV(34, ItemID.lookupName("Sniper Rifle"), 4),
		FlintlockPistolMkI(35, ItemID.lookupName("Flintlock Pistol"), 0),
		FlintlockPistolMkII(36, ItemID.lookupName("Flintlock Pistol"), 1),
		FlintlockPistolMkIII(37, ItemID.lookupName("Flintlock Pistol"), 2),
		FlintlockPistolMkIV(38, ItemID.lookupName("Flintlock Pistol"), 3),
		FlintlockPistolMkV(39, ItemID.lookupName("Flintlock Pistol"), 4),
		Wrench(44, ItemID.lookupName("Wrench"), 0),
		Sword(45, ItemID.lookupName("Sword"), 0),
		Hatchet(46, ItemID.lookupName("Hatchet"), 0),
		;

		int id, level;
		ItemID item;

		private static HashMap<Integer, LoadoutItem> idLookup = new HashMap<Integer, LoadoutItem>();

		private LoadoutItem(int id, ItemID item, int level) {
			this.id = id;
			this.item = item;
			this.level = level;
		}

		public int getLevel() {
			return level;
		}
		
		public int getId() {
			return id;
		}

		public ItemID getItem() {
			return item;
		}

		public static LoadoutItem lookupId(int i) {
			return idLookup.get(i);
		}

		static {
			for(LoadoutItem t: values()) {
				idLookup.put(t.getId(), t);
			}
		}

	}
}
