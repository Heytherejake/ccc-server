package com.studio878.server.multiplayer;

import java.util.TimerTask;

import com.studio878.server.app.Main;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Player;

public class MultiplayerAbility {

	long endTime, currTime;
	double multiplier;
	BonusItemType type;
	Player parent;
	
	public MultiplayerAbility(String data, long endTime, long currentTime, Player parent) {
		this.endTime = endTime;
		this.currTime = System.currentTimeMillis()/1000 - currentTime;

		this.parent = parent;

		String[] parts = data.split(",");
		type = BonusItemType.lookupId(Integer.parseInt(parts[0]));
		multiplier = Double.parseDouble(parts[1]);
		Main.registerTimerTask(new TimerTask() {

			public void run() {
				MultiplayerAbility.this.parent.multiplayerAbilities.remove(MultiplayerAbility.this);
			}
		}, 	(endTime + currTime - System.currentTimeMillis()/1000)*1000);
	}
	
	public long getTimeRemaining() {
		return (endTime + currTime - System.currentTimeMillis()/1000);
	}
	
	public BonusItemType getType() {
		return type;
	}
	
	public double getMultiplier() {
		return multiplier;
	}
}
