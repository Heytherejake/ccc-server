package com.studio878.server.world;


import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.entities.FallingBlock;
import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet15AddEntity;
import com.studio878.server.story.MapEvent;
import com.studio878.server.story.MapEvent.MapEventType;
import com.studio878.server.story.MapEventChain;
import com.studio878.server.story.maps.MapLoader;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager;
import com.studio878.server.util.TeamManager.Team;

public class Grid {
	
	public static int HighestLeftPoint = 0, HighestRightPoint = 0;
	static Block[][] blocks;
	static CopyOnWriteArrayList<MapEventChain> mapEvents = new CopyOnWriteArrayList<MapEventChain>();
	public static void generateGrid(int w, int h) {
		Console.write("- - - - Generating grid - - - -");
		int biome = 0;
		BlockType biomeBase = BlockType.lookupName("Dirt");
		BlockType[] biomeRef = new BlockType[w];
		ArrayList<Integer> biomeDelta = new ArrayList<Integer>();
		try {
			blocks = new Block[w][h];
			Random s = new Random();
			Noise n = new Noise(s, 1.0f, w, h);
			n.initialise();
			float[][] noise = n.getNoiseGrid();


			for(int i = 0; i < blocks.length; i++) {
				int stdr = s.nextInt(10000);
				if(stdr >= 9700) {
					biome++;
					biomeBase = getBiomeBase(biome);
					if(biomeBase == null) {
						biome = 0;
						biomeBase = getBiomeBase(biome);
					}
					biomeDelta.add(i);
				}
				biomeRef[i] = biomeBase;

				for(int j = 0; j < blocks[i].length; j++) {
					double spd = Math.abs(noise[i][j]*5);
					if(spd > 10) {
						spd /= 2;
					}
					int id = (int) spd;
					int sid = id;

					if(j > h/2) {
						BlockType p = BlockType.lookupName("Stone");

						if(j < (h/2) + 5) {
							p = biomeBase;
						}  else {
							if(spd >= 3.5 && spd <= 4) {
								p = biomeBase;
							}


						}

						addBlock(p, 0, i, j);
					} else {
						addBlock(BlockType.lookupName("Air"), 0, i, j);
					}
				}

			}
			Console.write("Roughing the terrain...");

			int hm = h/2;
			for(int i = 0; i < blocks.length; i++) {

				double spd = Math.abs(noise[i][hm]*5);
				if(spd >= 0) {
					int sd = s.nextInt(2) + 1;
					int dis = 0;
					if(s.nextInt(200) > 197) {
						dis = s.nextInt(w/((int) (4+sd)))+1;
					} else {
						dis = s.nextInt(10)+1;
					}
					int rw = 0;
					for(int j = 0; j < dis/2 + rw; j+= s.nextInt(s.nextInt(3)+1)+1) {
						for(int p = 0; p < dis-(rw*2); p++) {
							if(p+i < w && hm-j >= 0) {
								try {
									addBlock(biomeRef[p+i+j], 0, p+i+j, hm-rw);

								} catch (ArrayIndexOutOfBoundsException e) {

								}
							}
						}

						rw++;
					}
				}
			}
			Console.write("Smoothing biomes...");
			for(int i = 0; i < blocks.length; i++) {
				if(biomeDelta.contains(i) && i != 0 && i < blocks[i].length) {
					for(int j = 0; j < blocks[i].length; j++) {
						int dir = 0;
						if(i + 1 < blocks.length && blocks[i][j].getType() == blocks[i+1][j].getType()) {
							dir = 1;
						} else if(i > 0 && blocks[i][j].getType() == blocks[i-1][j].getType()) {
							dir = -1;
						}
						if(blocks[i + dir][j].getType() == biomeRef[i+dir]) {
							int str = s.nextInt(11);
							if(dir == -1) {
								int lds = i + str;
								if(lds >= w) {
									lds = w - 1;
								}
								for(int k = i; k < lds; k++) {
									addBlock(biomeRef[i], 0, k, j);

								}
							} else if(dir == 1) {
								int lds = i - str;
								if(lds < 0) {
									lds = 0;
								}
								for(int k = i; k > lds; k--) {
									addBlock(biomeRef[i], 0, k, j);
								}
							}
						}
					}
				}
			}
			Console.write("Lowering ground levels...");
			hm += 2;
			for(int i = 0; i < blocks.length; i++) {

				double spd = Math.abs(noise[i][hm]*5);
				if(spd >= 0) {
					int sd = s.nextInt(2) + 1;
					int dis = s.nextInt(3)+1;
					int rw = 0;
					for(int j = dis/2; j > -rw; j-= s.nextInt(s.nextInt(3)+1)+1) {
						for(int p = 0; p < dis+(rw*2); p++) {
							try {
								addBlock(biomeRef[p+i+j], 0, p+i-j, hm+rw);

							} catch (ArrayIndexOutOfBoundsException e) {

							}
						}
						rw++;
					}


				}
			}
			hm += 4;

			Console.write("Adding minerals...");
			double cl = s.nextInt(2);
			for(int i = 5; i < blocks.length - 5; i++) {
				for(int j = hm; j < blocks[i].length - 3; j++) {
					float spd = noise[i][j];
					if(j > blocks[i].length - 100) {
						if(s.nextInt(100) > 98) {
							populateVane(BlockType.lookupName("Sulfur"), i, j, s, 10);

						}
					}
					if( j > blocks[i].length - 200) {
						if(s.nextInt(1000) > 975) {
							populateVane(BlockType.lookupName("Iron"), i, j, s, 8);
						}
					}
					if(blocks[i][j].getType() == BlockType.lookupName("Stone")) {
						if(s.nextInt(1000) > 990) {
							populateVane(BlockType.lookupName("Coal"), i, j, s, 12);
						}
						if(s.nextInt(1000) > 997) {
							populateVane(BlockType.lookupName("Copper"), i, j, s, 6);
						}
					}
				}
			}

			System.out.println("Adding caves...");
			cl = s.nextInt(2);
			for(int i = 5; i < blocks.length - 5; i++) {
				for(int j = hm; j < blocks[i].length - 3; j++) {
					float spd = noise[i][j];
					double sipd = getTenthsPlace(spd);
					if(sipd >= 7.5 + cl) {
						BlockType psd = blocks[i][j].getType();
						addBlock(BlockType.lookupName("Air"), 0, i, j);
						blocks[i][j].setBackgroundType(psd);
					}
				}
			}
			Console.write("Adding stalagmites....");
			hm -= 2;
			for(int i = 0; i < blocks.length; i++) {

				double spd = Math.abs(noise[i][hm]*5);
				if(spd >= 0) {
					int sd = s.nextInt(2) + 1;
					int dis = s.nextInt(2)+1;
					int rw = 0;
					for(int j = dis/2; j > -rw; j-= s.nextInt(s.nextInt(3)+1)+1) {
						for(int p = 0; p < dis+(rw*2); p++) {
							try {
								addBlock(BlockType.lookupName("Stone"), 0, p+i-j, hm+rw);

							} catch (ArrayIndexOutOfBoundsException e) {

							}
						}
						rw++;
					}


				}
			}

			Console.write("Adding fluids...");
			for(int i = 0; i < blocks.length; i++) {
				if(s.nextInt(1000) > 990 && biomeRef[i] != BlockType.lookupName("Sand")) {
					for(int j = hm; j < blocks[i].length - 3; j++) {
						if(s.nextInt(1000) > 930) {
							createFloodArea(BlockType.lookupName("Water"), i, j, s, (int) Math.floor(Math.random()*10) + 30);

						}
					}
				} else if (s.nextInt(1000) > 990) {
					for(int j = hm; j < blocks[i].length - 3; j++) {
						if(s.nextInt(1000) > 990) {
							createFloodArea(BlockType.lookupName("Lava"), i, j, s, (int) Math.floor(Math.random()*10) + 40);

						}
					}
				}
			}

			Console.write("Filling in holes...");
			for(int i = 0; i < blocks.length; i++) {
				for(int j = blocks[i].length - 1; j >= 0 ; j--) {
					if(blocks[i][j] == null) {
						addBlock(BlockType.lookupName("Air"), 0, i, j);
					}
				}
			}
			Console.write("Simulating physics...");
			for(int i = 0; i < blocks.length; i++) {
				for(int j = blocks[i].length - 1; j >= 0; j--) {
					int offset = 1;
					if(j < blocks[i].length - 1 && blocks[i][j+1].getType() == BlockType.lookupName("Air") && blocks[i][j].getType().isPhysicsEnabled()) {
						immediatePhysicsUpdate(i, j+1);
					}
				}
			}
			Console.write("Growing grass and spawning trees...");
			for(int i = 0; i < blocks.length; i++) {
				for(int j = blocks[i].length - 1; j > 0; j--) {
					if(blocks[i][j].getType() == BlockType.lookupName("Dirt") && blocks[i][j-1] != null && blocks[i][j-1].getType() == BlockType.lookupName("Air")){
						addBlock(BlockType.lookupName("Grass"), 0, i, j);
						if(blocks[i][j-1].getBackgroundType() == BlockType.lookupName("Air")) {
							ArrayList<Block> adj = blocks[i][j-1].getAdjacentAndDiagonalBlocks();
							boolean cont = true;
							for(Block b : adj) {
								if(b.getType() == BlockType.lookupName("TreeStump") || b.getType() == BlockType.lookupName("Tree Top") || b.getType() == BlockType.lookupName("Tree Trunk")) {
									cont = false;
								}
							}
							if(cont) {
								int rnd = s.nextInt(1000);
								if(rnd > 750) {
									spawnTree(i, j - 1, s.nextInt(10) + 4);
								}
							}
						}


					}
				}
			}
			
			setEndRows();

			Console.write("- - - - Grid generated! - - - -");
		} catch (Exception e ) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void spawnTree(int x, int y, int height) {
		int pos = 0;
		try {
			blocks[x][y].setType(BlockType.lookupName("Tree Stump"));
			for(int i = 1; i < height; i++) {
				blocks[x][y - i].setType(BlockType.lookupName("Tree Trunk"));
				pos = i;
			}
			blocks[x][y - height].setType(BlockType.lookupName("Tree Top"));
		} catch (ArrayIndexOutOfBoundsException e) {
			blocks[x][y - pos].setType(BlockType.lookupName("Tree Top"));
		}
	}
	
	public static void createFloodArea(BlockType t, int sx, int sy, Random s, int tamt) {
		int tx = 0, ty = 0, ds = 1;
		int vs = s.nextInt(2);
		if(vs == 1) {
			ds = -1;
		}
		for(int k = 0; k < tamt; k++) {
			int direction = s.nextInt(2);
			int x, y;
			if(direction == 0) {
				x = 1*ds;
				y = 0;
			} else {
				x = 0;
				y = 1*ds;
			} 
			try {
				BlockType bg = blocks[sx + x + tx][sy + y + ty].getBackgroundType();
				addBlock(t, 0, sx + x + tx, sy + y + ty);
				blocks[sx + x + tx][sy + y + ty].setBackgroundType(bg);
				if(t.isFluid()) {
					blocks[sx + x + tx][sy + y + ty].setData(16);
				}
				tx += x;
				ty += y;
			} catch (IndexOutOfBoundsException e) {
			}
		}
	}
	
	public static void addBlock(BlockType t, int d, int x, int y) {
		blocks[x][y] = new Block(t, d, x, y, getBlockOffset(x, y));
	}
	public static void setBlockAt(int x, int y, Block b) {
		Block bn = blocks[x][y];
		bn.setData(b.getData());
		bn.setCondition(b.getCondition());

		bn.setType(b.getType());
		bn.setSquareLocation(b.getSquareLocation());

	}
	public static Block getBlockAt(int x, int y) {
		return blocks[x][y];
	}
	public static Block[][] getGrid() {
		return blocks;
	}
	public static void setBlock(Block b) {
		blocks[b.getX()][b.getY()] = b;
	}

	public static void setEndRows() {
		HighestLeftPoint = 0;
		HighestRightPoint = 0;
		for(int i = 0; i < blocks[0].length; i++) {
			if(HighestLeftPoint == 0 && !blocks[1][i].getType().getName().equals("Air") && !blocks[1][i].getType().getPermiability(null)) {
				HighestLeftPoint = i*16;
			}
			if(HighestRightPoint == 0 && !blocks[Settings.WorldWidth - 2][i].getType().getName().equals("Air") && !blocks[Settings.WorldWidth - 2][i].getType().getPermiability(null)) {
				HighestRightPoint = i*16;
			}
		}
	}
	public static void updatePhysicsAtBlock(int x, int y) {
		Block b = blocks[x][y];
		if(b == null || b.getType() != BlockType.lookupName("Air")) return;
		Block bd = null;
		try {
			while(--y != 0 && (bd = blocks[x][y]) != null && bd.getType().isPhysicsEnabled()) {
				FallingBlock e = new FallingBlock(x*16, y*16, bd.getType());
				EntityManager.registerEntity(e);
				if(Main.getDispatch().getClients().size() != 0) {
					Main.getDispatch().broadcastPacket(new Packet15AddEntity(Main.getDispatch().getClients().get(0), e));
				}
				bd.setType(BlockType.lookupName("Air"));
			}
			if(y != 0 && blocks[x][y].getType() == BlockType.lookupName("TreeStump")) {
				blocks[x][y].destroy(null);
			}
		} catch (IndexOutOfBoundsException e) {

		}
	}

	public static void forceFall(Block b) {
		FallingBlock e = new FallingBlock(b.getX()*16, b.getY()*16, b.getType());
		EntityManager.registerEntity(e);
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet15AddEntity(Main.getDispatch().getClients().get(0), e));
		}
		b.setType(BlockType.lookupName("Air"));
	}
	public static void immediatePhysicsUpdate(int x, int y) {
		int shift = 0;
		boolean cont = true;
		if(y == 0 || !blocks[x][y-1].getType().isPhysicsEnabled()) return;
		for(int i = y; i < blocks[x].length; i++) {
			if(blocks[x][i].getType() != BlockType.lookupName("Air")) {
				cont = false;
			} 
			if(cont) {
				shift++;
			}
		}
		for(int i = 1; i <= y; i++) {
			if((y-i-1) != -1  && blocks[x][y-i].getType() != BlockType.lookupName("Air")) {
				if(!blocks[x][y-i].getType().isPhysicsEnabled()) {
					blocks[x][y-i+1].setType(BlockType.lookupName("Air"));
					return;
				}
				blocks[x][y-i].shiftDown(shift);
			} else {
				if(!blocks[x][y-i].getType().isPhysicsEnabled()) {
					for(int j = 1; j < shift+1; j++) {
						blocks[x][y-i + j].setType(BlockType.lookupName("Air"));
					}

					return;
				}				
				for(int j = 0; j < shift; j++) {
					blocks[x][y-i + j].setType(BlockType.lookupName("Air"));
				}
			}
		}
	}

	public static void checkBuiried() {
		for(Player p : EntityManager.getPlayers()) {
			Block bs = Grid.getClosestBlock((int) p.getX(),(int) p.getY() + p.getHeight());
			if(bs == null) return;
			bs = bs.getBlockAdjacent(0, -1);
			Block bsd = bs.getBlockAdjacent(1, 0);
			if((bs != null && bsd != null) && (!bs.getType().getPermiability(p) && !bsd.getType().getPermiability(p))) {
				p.damage(5, DamageMethod.Block, p.getName());
			}
		}
	}
	public static int getSpawnLocation(int x) {
		for(int i = 0; i < Settings.WorldHeight; i++) {
			Block b = getClosestBlock(x, i*16);
			if(b != null && b.getType() != BlockType.lookupName("Air") && !b.getType().getPermiability(null)) {
				int trt = (i-5) * 16;
				if(trt < 0) {
					return 0;
				} else {
					return trt;
				}
			}
		}
		return 0;
	}

	private static void populateVane(BlockType t, int sx, int sy, Random s, int tamt) {
		int amount = s.nextInt(tamt);
		int tx = 0, ty = 0, ds = 1;
		int vs = s.nextInt(2);
		if(vs == 1) {
			ds = -1;
		}
		for(int k = 0; k < amount; k++) {
			int direction = s.nextInt(2);
			int x, y;
			if(direction == 0) {
				x = 1*ds;
				y = 0;
			} else {
				x = 0;
				y = 1*ds;
			} 
			try {
				BlockType psd = blocks[sx + x + tx][sy + y + ty].getType();
				addBlock(t, 0, sx + x + tx, sy + y + ty);
				blocks[sx + x + tx][sy + y + ty].setBackgroundType(psd);
				tx += x;
				ty += y;
			} catch (IndexOutOfBoundsException e) {
			}
		}
	}

	public static ArrayList<Block> getCircle(int xm, int ym, int radius) {
		ArrayList<Block> result = new ArrayList<Block>();
		if(xm + radius >= blocks.length) {
			xm-= radius;
		}
		if(xm - radius < 0) {
			xm = radius;
		}
		if(ym - radius < 0) {
			ym = radius;
		}
		if(ym + radius >= blocks[0].length) {
			ym -= radius;
		}

		for(int y=-radius; y<=radius; y++) {
			for(int x=-radius; x<=radius; x++) {
				if(x*x+y*y <= radius*radius) {
					try {
						result.add(blocks[xm+x][ym+y]);
					} catch (IndexOutOfBoundsException e) {

					}
				}
			}
		}
		return result;
	}
	public static void loadGrid(Block[][] g) {
		blocks = new Block[Settings.WorldHeight][Settings.WorldWidth];
		for(int i = 0; i < g.length; i++) {
			for(int j = 0; j < g[i].length; j++) {
				addBlock(g[i][j].getType(), g[i][j].getData(), g[i][j].getX(), g[i][j].getY());
				blocks[i][j].setCondition(g[i][j].getCondition());
			}
		}
	}
	public static Block getClosestBlock(int x, int y) {
		x = (x - (x%16))/16;
		y = (y - (y%16))/16;
		try {
			return blocks[x][y];
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
	

	public static ArrayList<Block> getBlockGroup(Block start, int width, int height, int direction) {
		ArrayList<Block> results = new ArrayList<Block>();
		int count = 0;
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				try {
					results.add(blocks[start.getX() + (direction*i)][start.getY() + j]);
					count++;
				} catch (Exception e) {
				}
			}
		}
		return results;
	}
	public static boolean isAreaOpen(int x, int y, int width, int height) {
		int dx = x;
		int dy = y;
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				System.out.println(blocks[dx + i][dy + j].getType());
				if(blocks[dx + i][dy + j] != null && blocks[dx + i][dy + j].getType() != BlockType.lookupName("Air")) {
					return false;
				}
			}
		}
		return true;
	}

	public static void loadMap(String file) {
		String fp = "maps" + File.separator + file;
		System.out.println("Loading " + fp);
		URL fs = null;
		try {
			fs = new File(fp).toURI().toURL();
		} catch (MalformedURLException e1) {
			System.out.println("Could not find file");
		}
		if(file.substring(0, 1).equals("~")) {
			fs = MapLoader.class.getResource(file.substring(1));
		}
		mapEvents.clear();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(fs.openStream()));
			String[] sections = in.readLine().split("\\{");
			String ws = sections[1].substring(0, sections[1].length() - 1);
			String[] parts = ws.split(",");
			Settings.WorldWidth = Integer.parseInt(parts[0]);
			Settings.WorldHeight = Integer.parseInt(parts[1]);
			TeamManager.setTeamRespawnPoint(Team.Blue, new Point(Integer.parseInt(parts[2])*16, Integer.parseInt(parts[3])*16));
			TeamManager.setTeamRespawnPoint(Team.Red, new Point(Integer.parseInt(parts[2])*16, Integer.parseInt(parts[3])*16));

			Grid.blocks = new Block[Settings.WorldWidth][Settings.WorldHeight];

			int pos = 1;
			ws = sections[2].substring(0, sections[2].length() - 1);
			parts = ws.split("\\[");
			for(int i = 0; i < Settings.WorldWidth; i++) {
				for(int j = 0; j < Settings.WorldHeight; j++) {
					ws = parts[pos].substring(0, parts[pos].length() - 1);
					String[] split = ws.split(",");
					Grid.blocks[i][j] = new Block(BlockType.lookupId(Integer.parseInt(split[3])), Integer.parseInt(split[5]), Integer.parseInt(split[0]), Integer.parseInt(split[1]), Byte.parseByte(split[2]));
					Grid.blocks[i][j].setBackgroundType(BlockType.lookupId(Integer.parseInt(split[4])));
					try {
						Grid.blocks[i][j].setForegroundType(BlockType.lookupId(Integer.parseInt(split[6])));
					} catch (Exception e) {
					}
					try {
						Grid.blocks[i][j].setDecorationType(BlockType.lookupId(Integer.parseInt(split[7])));
					} catch (Exception e) {
					}
					pos++;
				}
			}
			ws = sections[3].substring(0, sections[3].length() - 1);
			parts = ws.split("\\[");
			for(int i = 1; i < parts.length; i++) {
				ws = parts[i].substring(0, parts[i].length() - 1);
				String[] split = ws.split(":");
				String[] ps1 = split[0].split(",");
				MapEventChain mc = new MapEventChain(Integer.parseInt(ps1[0]), Integer.parseInt(ps1[1]), Integer.parseInt(ps1[2]), Integer.parseInt(ps1[3]));
				for(int j = 1; j < split.length; j++) {
					String[] sp2 = split[j].split(",");
					String data = sp2[1];
					data = data.replace("U+002C", ",");
					data = data.replace("U+003A", ":");
					data = data.replace("U+005B", "[");
					data = data.replace("U+005D", "]");
					data = data.replace("U+007B", "{");
					data = data.replace("U+007D", "}");
					MapEvent ms = MapEvent.matchEvent(MapEventType.lookupId(Integer.parseInt(sp2[0])), data, Integer.parseInt(sp2[2]), mc);
					mc.addEvent(ms);
				}
				mapEvents.add(mc);

			}
			ws = sections[4].substring(0, sections[4].length() - 1);
			parts = ws.split("\\[");
			for(int i = 1; i < parts.length; i++) {
				ws = parts[i].substring(0, parts[i].length() - 1);
				String[] split = ws.split(",");
				Entity e = EntityManager.cloneEntity(EntityName.lookupId(Integer.parseInt(split[0])), Integer.parseInt(split[1]), Integer.parseInt(split[2]), split[3]);
				e.setName(split[4]);
				if(e != null) {
					EntityManager.registerEntity(e);
				}
			}
			System.out.println("Map loaded");
		} catch (Exception e) {
			System.out.println("An error occured in import.");
			e.printStackTrace();
		}
	}

	public static CopyOnWriteArrayList<MapEventChain> getMapEvents() {
		return mapEvents;
	}
	private static BlockType getBiomeBase(int i) {
		switch(i) {
		case 0:
			return BlockType.lookupName("Dirt");
		case 1:
			return BlockType.lookupName("Sand");
		default:
			return null;
		}
	}

	private static double getTenthsPlace(double s) {
		return (s - Math.floor(s))*10;
	}

	public static byte getBlockOffset(int i, int j) {
		int xo = i%2;
		int yo = j%2;
		int offset;
		if(xo == 0 && yo == 0) {
			offset = 0;
		} else if (xo == 1 && yo == 0) {

			offset = 1;
		} else if (xo == 0 && yo == 1) {
			offset = 2;
		} else {
			offset = 3;
		}
		return (byte) offset;
	}
}
