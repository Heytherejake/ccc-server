package com.studio878.server.thread;

import java.util.ArrayList;

import com.studio878.server.block.Block;

public class ElectricThread extends Thread {

	public void run() {
		try {
			while(true) {
				for(Block b : Block.wireBlocks) {
					String ns = b.getWireType().getName();
					if(ns.equals("Wire") || ns.equals("Vertical Wire")) {
						b.setWireData(b.isConnectedToBattery(new ArrayList<Block>(), b) ?  1 : 0);
					}
				}
				this.sleep(250);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
