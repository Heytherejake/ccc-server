package com.studio878.server.thread;

import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;

public class FluidThread extends Thread {

	public void run() {
		try {
			while(true) {
				for(Block b : Block.fluidList) {
					int data = b.getData();
					Block bu = b.getBlockAdjacent(0, 1);
					if(bu != null && (bu.getType() == BlockType.lookupName("Air") || bu.getType() == b.getType()) && bu.getData() != b.getData() && !(bu.getType() == b.getType() && bu.getData() == 16)) {
						int sd = (int) b.getData() + bu.getData();
						if(sd != 16) sd = 16;
						bu.setType(b.getType());
						b.setType(BlockType.lookupName("Air"));
						bu.setData(sd);
						b.setData(0);
					} else {
						Block bl = b.getBlockAdjacent(-1, 0);
						if(bl != null && (bl.getType() == BlockType.lookupName("Air") || bl.getType() == b.getType()) && Math.abs(bl.getData() - b.getData()) > 1) {
							int sd = (int) (Math.ceil(b.getData() + bl.getData())/2.0);
							bl.setType(b.getType());
							bl.setData(sd);
							b.setData(sd);
						}
						if(bl != null && ((bl.getType() == BlockType.lookupName("Water") && b.getType() == BlockType.lookupName("Lava")) || (b.getType() == BlockType.lookupName("Water") && bl.getType() == BlockType.lookupName("Lava")))) {
							bl.setType(BlockType.lookupName("Stone"));
							b.setType(BlockType.lookupName("Stone"));
						}
						Block br = b.getBlockAdjacent(1, 0);
						if(br != null && (br.getType() == BlockType.lookupName("Air") || br.getType() == b.getType()) && Math.abs(br.getData() - b.getData()) > 1) {
							int sd = (int) (Math.ceil(b.getData() + br.getData())/2.0);
							br.setType(b.getType());
							br.setData(sd);
							b.setData(sd);
						}
						if(br != null && ((br.getType() == BlockType.lookupName("Water") && b.getType() == BlockType.lookupName("Lava")) || (b.getType() == BlockType.lookupName("Water") && br.getType() == BlockType.lookupName("Lava")))) {
							br.setType(BlockType.lookupName("Stone"));
							b.setType(BlockType.lookupName("Stone"));
						}
					}
					if(bu != null && ((bu.getType() == BlockType.lookupName("Water") && b.getType() == BlockType.lookupName("Lava")) || (b.getType() == BlockType.lookupName("Water") && bu.getType() == BlockType.lookupName("Lava")))) {
						bu.setType(BlockType.lookupName("Stone"));
						b.setType(BlockType.lookupName("Air"));
					}


				}
				this.sleep(10);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
