package com.studio878.server.thread;

import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.PhysicsAffectedEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet37SendPlayerInfo;
import com.studio878.server.packets.Packet39SendHandshakeRequest;
import com.studio878.server.world.Grid;

public class LogicThread extends Thread{

	long ticks = 0;

	public void run() {
		try {
			while(!isInterrupted()) {
				ArrayList<PhysicsAffectedEntity> es = EntityManager.getPhysicsAffectedEntities();
				for(PhysicsAffectedEntity e : es) {
						e.step();
				}
				ticks++;
				
				if(ticks % 50 == 0) {
					Grid.checkBuiried();
				}
				
				
				if(ticks % 100 == 0) {
					if(Main.getDispatch().getClients().size() != 0) {
						Main.getDispatch().broadcastPacket(new Packet39SendHandshakeRequest(Main.getDispatch().getClients().get(0)));
						Main.getDispatch().broadcastPacket(new Packet37SendPlayerInfo(Main.getDispatch().getClients().get(0)));
					}
				}
				Thread.sleep(10);
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
