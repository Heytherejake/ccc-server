package com.studio878.server.thread;

import com.studio878.server.block.Block;
import com.studio878.server.packets.Packet04SendGrid;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class WorldBuilderThread extends Thread {

	public Packet04SendGrid packet;
	public int position;
	Block[][] blocks;
	
	public WorldBuilderThread(int i, Packet04SendGrid packet) {
		this.position = i;
		this.packet = packet;
		blocks = Grid.getGrid();
		packet.threads[i] = new StringBuilder(blocks.length*blocks[0].length*40/Settings.WorldBuildThreads);
	}

	public void run() {
		for(int i = (int) Math.floor(position*((double)blocks.length/(double)Settings.WorldBuildThreads)); i < (int) Math.floor((position + 1)*((double)blocks.length/(double)Settings.WorldBuildThreads)); i++) {
			for(int j = 0; j < blocks[i].length; j++) {
				Block b = blocks[i][j];
				packet.threads[position].append(b.getType().getId());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getCondition());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getData());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getOffset());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getBackgroundType().getId());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getForegroundId());
				packet.threads[position].append(",");
				packet.threads[position].append(b.getDecorationId());
				packet.threads[position].append(":");
				//+ "," + b.getData() + "," + b.getOffset() + "," + b.getBackgroundType().getId() + "," + b.getForegroundId() + "," + b.getDecorationId() + ":");
			}
		}
		packet.finished[position] = true;
	}

}
