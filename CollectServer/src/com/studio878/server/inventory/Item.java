package com.studio878.server.inventory;

import java.util.TimerTask;

import com.studio878.server.app.Main;
import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet21SendInventory;
import com.studio878.server.packets.Packet60SendSlotUpdate;

public class Item {
	
	//Clip/Total/Size/Reload Time/Damage/ROF Modifier

	ItemID type;
	int amount;
	int slot = 0;
	int level = 0;
	String data = null;
	long reloadStart = 0;
	Player owner;
	
	int ammo, reserve;
	
	public boolean isReloading = false;
	
	public Item(ItemID i, int a, int level, Player owner) {
		type = i;
		try {
		ammo = i.getDefaultDataArray()[level].getClipSize();
		reserve = i.getDefaultDataArray()[level].getReserveSize();
		} catch (Exception e) {
		}
		
		amount = a;
		this.owner = owner;
		this.level = level;
		updateData();

	}
	
	public Item(ItemID i, int a, Player owner) {
		type = i;
		ammo = 0;
		reserve = 0;
		amount = a;
		this.owner = owner;
		updateData();

	}

	public Player getOwner() {
		return owner;
	}
	
	public void setData(String s) {
		data = s;
	}
	
	public int getLevel() {
		return level;
	}

	public String getData() {
		return data;
	}


	public ItemID getType() {
		return type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int a) {
		amount = a;
	}

	public void useAmmo(int amount) {
		ammo -= amount;
		if(ammo < 0) {
			ammo = 0;
		}
		updateData();
	}

	public void addAmmo(int amount, boolean overload) {
		reserve += amount;
		if(!overload && reserve > type.getDefaultDataArray()[level].getReserveSize()) {
			reserve = type.getDefaultDataArray()[level].getReserveSize();
		}
		updateData();
	}

	public int getTotalAmmo() {
		return reserve;
	}
	public int getDefaultClip() {
		return type.getDefaultDataArray()[level].getClipSize();
	}
	
	public int getDamage() {
		return type.getDefaultDataArray()[level].getDamage();
	}
	
	public int getClip() {
		return ammo;
	}
	
	public void setAmmo(int amount) {
		ammo = amount;
		updateData();
	}
	
	public void setReserve(int amount) {
		reserve = amount;
		updateData();
	}
	
	public int getReloadTime() {
		return type.getDefaultDataArray()[level].getReloadTime();
	}
	
	public void startReload() {
	    Main.registerTimerTask(new reloadTask(this), getReloadTime());
	}
	public void reload() {
		if(type.getWeaponType() == 1) {
			int c = ammo;
			int q = reserve;
			int t = getDefaultClip();
			if(t != 0) {
				q -= (t-c);
				c = t;
				if(q < 0) {
					int o = Math.abs(q);
					c -= o;
					q = 0;
				}
				ammo = c;
				reserve = q;
			}
			updateData();
			owner.getHandle().getSender().sendPacket(new Packet21SendInventory(owner.getHandle()));
		}
	}
	
	public double getRateOfFireModifier() {
		return type.getDefaultDataArray()[level].getRateOfFireModifier();
	}
	
	public void updateData() {
		if(type.getDefaultDataArray() != null && type.getDefaultDataArray().length > 0) {
			data = ammo + "/" + type.getDefaultDataArray()[level].getClipSize() + "/" + reserve + "/" + type.getDefaultDataArray()[level].getReloadTime() + "/" + getRateOfFireModifier();
		} else {
			data = " ";
		}
	}
	public int getSlot() {
		return slot;
	}

	public void setSlot(int i) {
		slot = i;
	}
	
	public void sendUpdate() {
		owner.getHandle().getSender().sendPacket(new Packet60SendSlotUpdate(owner.getHandle(), this));
	}
}

class reloadTask extends TimerTask{

	Item s;
	
	public reloadTask(Item s) {
		this.s = s;
		s.isReloading = true;
	}
	public void run() {
		s.reload();
		s.isReloading = false;
		s.sendUpdate();
	}
	
}
