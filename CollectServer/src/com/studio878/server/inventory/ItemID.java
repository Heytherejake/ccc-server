package com.studio878.server.inventory;

import java.awt.Point;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.studio878.server.block.BlockType;
import com.studio878.server.logging.Console;

public class ItemID {

	static int nextHighestId = 0;
	
	int id, stack;
	private static ConcurrentHashMap<Integer, ItemID> lookupId = new ConcurrentHashMap<Integer, ItemID>();
	private static ConcurrentHashMap<String, ItemID> lookupName = new ConcurrentHashMap<String, ItemID>();

	WeaponStatistics[] def;
	int weaponType;
	String source, description;
	int sellCost;
	String name;
	Point client_img;
	String[] displayNames;
	int buyCost;
	
	public ItemID(int id, String name, String description, int stack, WeaponStatistics[] def, int weaponType, String source, int cost, Point client_img, String[] displayNames, int buyCost) {
		this.id = id;
		this.stack = stack;
		this.description = description;
		this.def = def;
		this.weaponType = weaponType;
		this.source = source;
		this.sellCost = cost;
		this.name = name;
		this.client_img = client_img;
		this.displayNames = displayNames;
		this.buyCost = buyCost;
	}
	
	public int getId() {
		return id;
	}
	
	public int getBuyCost() {
		return buyCost;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getName() {
		return name;
	}
	
	public String[] getAlternativeNames() {
		return displayNames;
	}
	
	public Point getClientImagePoint() {
		return client_img;
	}
	
	public int getSellCost() {
		return sellCost;
	}

	public int getWeaponType() {
		return weaponType;
	}

	public int getStackSize() {
		return stack;
	}

	public BlockType getSource() {
		return source == null ? null : BlockType.lookupName(source);
	}

	public boolean isBlock() {
		return source != null;
	}

	public WeaponStatistics[] getDefaultDataArray() {
		return def;
	}
	
	public String toString() {
		return name;
	}
	
	
	public static ItemID lookupId(int id) {
		return lookupId.get(id);
	}

	public static ItemID lookupName(String name) {
		for(ItemID e : lookupId.values()) {
			if(e.toString().equalsIgnoreCase(name)) {
				return e;
			}
		}
		Console.error("Item not found: " + name);
		return null;
	}
	
	public static ConcurrentHashMap<Integer, ItemID> getItemList() {
		return lookupId;
	}

	public static void populateDefaultItems() {
		addNewItem("Dirt", "Bits and pieces of earth. It's pretty much useless.", 50, null, 0, "Dirt", 1, new Point(2, 1), new String[]{"Dirt"}, 10);
		addNewItem("Stone", "A big chunk of stone. How you got it so square is a mystery.", 50, null, 0, "Stone", 1, new Point(0, 1), new String[]{"Stone"}, 20);
		addNewItem("Grass", "A piece of dirt with some grass in it. If you watch it enough, maybe it'll grow.", 50, null, 0, "Dirt", 1, new Point(2, 1), new String[]{"Grass"}, -1);
		addNewItem("Iron", "A metal that's used in the construction of most firearms. It's dramatic and situational.", 25, null, 0, "Iron", 5, new Point(1, 1), new String[]{"Iron"}, 75);
		addNewItem("Sulfur", "An element commonly found in fertalizer. We use it for explosives.", 25, null, 0, "Sulfur", 10, new Point(2, 2), new String[]{"Sulfur"}, 100);
		addNewItem("Integrated Circuit", "A microchip that controls essential functions for electronics. It's value is Circuit * x + C", 10, null, 0, null, 25, new Point(1, 3), new String[]{"Integrated Circuit"}, -1);
		addNewItem("Sand", "Coarse bits of rock that get in everything. Literally everything.", 50, null, 0, "Sand", 1, new Point(3, 3), new String[]{"Sand"}, 10);
		addNewItem("Coal", "A fuel source. There's no time to worry about climate change when you're at war.", 25, null, 0, "Coal", 3, new Point(0, 4), new String[]{"Coal"}, 75);
		addNewItem("Silicon", "An element used for all modern electronics. Most of it is found in the valley.", 25, null, 0, null, 5, new Point(2, 3), new String[]{"Silicon"}, 100);
		addNewItem("Copper", "A sublime conductor. All aboard.", 25, null, 0, "Copper", 15, new Point(1, 4), new String[]{"Copper"}, 75);
		addNewItem("Copper Wire",  "A necessary component in most electronics. Despite the fact that copper is really common, we've marked it up a bunch to discourage buying. Start searching.", 10, null, 0, null, 10, new Point(2, 4), new String[]{"Copper Wire"}, -1);
		addNewItem("Coins", "Currency. You use this to buy things.", 500, null, 0, null, 0, new Point(3, 4), new String[]{"Coins"}, 1);
		addNewItem("Wood", "Tree-flesh. If you collect some, the opportunity for jokes is endless.", 50, null, 0, "Wood", 1, new Point(1, 5), new String[]{"Wood"}, 15);
		addNewItem("Torch", "A lighting device created from proprietary torch wood. It's outside the government and beyond the police.", 100, null, 0, "Torch", 1, new Point(2, 5), new String[]{"Torch"}, 15);
		addNewItem("Brick", "Square stones held together with a paste. It makes good walls, but walls can't attack.", 50, null, 0, "Brick", 5, new Point(2, 6), new String[]{"Brick"}, 50);
		addNewItem("Debug Water", "Water that we use for debugging. Kind of self explanatory.", 1000, null, 0, "Water", 0, new Point(0, 4), new String[]{"Debug Water"}, -1);
		addNewItem("Turret", "A device that locks on to enemies and shoots bullets at them. Auto-aim OP.", 5, null, 0, null, 0, new Point(0, 7), new String[]{"Turret"}, -1);
		addNewItem("Grenade", "An explosive canister that you can throw at people, bringing them pain and death. Grenades have a three second delay before explosion.", 20, null, 0, null, 0, new Point(0, 0), new String[]{"Grenade"}, -1);
		addNewItem("Pistol", "A standard pistol. Fires fast and accurately. You probably should be carrying one of these early on, unless you enjoy both losing and death.", 1, new WeaponStatistics[]{new WeaponStatistics(12, 24, 2000, 15, 0.0), new WeaponStatistics(12, 24, 2000, 15, 0.05), new WeaponStatistics(12, 24, 2000, 20, 0.05), new WeaponStatistics(16, 24, 2000, 20, 0.05), new WeaponStatistics(16, 30, 2000, 20, 0.05)}, 1, null, 0, new Point(1, 0), new String[]{"SMG Mk. I, Pistol Mk. II","Pistol Mk. III", "Pistol Mk. IV", "Pistol Mk. V"}, -1);
		addNewItem("Shovel", "A spade. If you've got the heart, and you're in the club.... diamond.", 1, new WeaponStatistics[]{new WeaponStatistics(0, 0, 750, 0, 0)}, 2, null, 0, new Point(2, 0), new String[]{"Shovel"}, -1);
		addNewItem("Submachine Gun", "A rapid-fire that fires a spray of bullets that hit for less damage.", 1, new WeaponStatistics[]{new WeaponStatistics(30, 45, 2000, 5, 0.0), new WeaponStatistics(30, 45, 2000, 5, 0.05), new WeaponStatistics(30, 45, 2000, 7, 0.05), new WeaponStatistics(40, 45, 2000, 7, 0.05), new WeaponStatistics(40, 60, 2000, 7, 0.05)}, 1, null, 0, new Point(3, 0),  new String[] {"SMG Mk. I", "SMG Mk. II","SMG Mk. III", "SMG Mk. IV", "SMG Mk. V"}, -1);
		addNewItem("Shotgun", "A high-damage, close-range firearm that fires five bullets in a wide spread. For passenger-side use only.", 1, new WeaponStatistics[]{new WeaponStatistics(6, 18, 2000, 15, 0.0), new WeaponStatistics(6, 18, 2000, 15, 0.10), new WeaponStatistics(6, 18, 2000, 17, 0.10), new WeaponStatistics(9, 18, 2000, 17, 0.10), new WeaponStatistics(9, 24, 2000, 17, 0.10)}, 1, null, 0, new Point(3, 1), new String[] {"Shotgun Mk. I", "Shotgun Mk. II","Shotgun Mk. III", "Shotgun Mk. IV", "Shotgun Mk. V"}, -1);
		addNewItem("Sniper Rifle", "A high-damage weapon that fires incredibly accurately. Make every shot count.", 1, new WeaponStatistics[]{new WeaponStatistics(1, 10, 3000, 75, 0.0), new WeaponStatistics(1, 10, 2500, 75, 0.00), new WeaponStatistics(1, 10, 2500, 85, 0.00), new WeaponStatistics(1, 14, 2500, 85, 0.0), new WeaponStatistics(1, 14, 2250, 85, 0.0)}, 1, null, 0, new Point(0, 2), new String[] {"Sniper Rifle Mk. I", "Sniper Rifle Mk. II", "Sniper Rifle Mk. III", "Sniper Rifle Mk. IV", "Sniper Rifle Mk. V"}, -1);
		addNewItem("Rocket Launcher", "Fires powerful missiles that damage enemies as well as blocks. Favorite weapon of soldiers and rebels everywhere.", 1, new WeaponStatistics[]{new WeaponStatistics(2, 8, 2000, 60, 0.0), new WeaponStatistics(2, 8, 2000, 60, 0.1), new WeaponStatistics(2, 8, 2000, 65, 0.1), new WeaponStatistics(3, 8, 2000, 65, 0.1), new WeaponStatistics(3, 12, 2000, 65, 0.10)}, 1, null, 0, new Point(1, 2), new String[] {"Rocket Launcher Mk. I", "Rocket Launcher Mk. II", "Rocket Launcher Mk. III", "Rocket Launcher Mk. IV", "Rocket Launcher Mk. V"}, -1);
		addNewItem("Landmine", "An electronic explosive that you can throw at the ground, bring suffering to those absent-minded enough to walk on it.", 20, null, 0, null, 0, new Point(3, 2), new String[]{"Landmine"}, -1);
		addNewItem("Flintlock Pistol", "An antique pistol that fires a devistating ball, but has to be reloaded after each use. It belongs in a museum.", 1, new WeaponStatistics[]{new WeaponStatistics(1, 10, 750, 50, 0.0), new WeaponStatistics(1, 10, 500, 50, 0.0), new WeaponStatistics(1, 10, 500, 60, 0.0), new WeaponStatistics(1, 14, 500, 60, 0.0), new WeaponStatistics(1, 18, 500, 6, 0.0)}, 1, null, 0, new Point(0, 5), new String[] {"Flintlock Pistol Mk. I", "Flintlock Pistol Mk. II", "Flintlock Pistol Mk. III", "Flintlock Pistol Mk. IV", "Flintlock Pistol Mk. V"}, -1);
		addNewItem("Wrench", "A metal tool used to turn things and upgrade sentries. It beats heads in nicely, but has very little reach.", 1, new WeaponStatistics[]{new WeaponStatistics(0, 0, 1000, 0, 0)}, 2, null, 0, new Point(3, 5), new String[]{"Wrench"}, -1);
		addNewItem("Sword", "Vanquish your enemies and bring justice upon thine lands.", 1, new WeaponStatistics[]{new WeaponStatistics(0, 0, 1000, 0, 0)}, 2, null, 0, new Point(0, 6), new String[]{"Sword"}, -1);
		addNewItem("Hatchet", "An axe used to cut trees down and limbs off. It's a miniature hatch.", 1, new WeaponStatistics[]{new WeaponStatistics(0, 0, 750, 0, 0)}, 2, null, 0, new Point(1, 6), new String[]{"Hatchet"}, -1);
	}
	
	public static void addNewItem(String name, String description, int stackSize, WeaponStatistics[] dataArray, int type, String source, int value, Point loc, String[] dsn, int buyCost) {
		ItemID item = new ItemID(nextHighestId, name, description, stackSize, dataArray, type, source, value, loc, dsn, buyCost);
		lookupId.put(nextHighestId, item);
		lookupName.put(name, item);
		Console.log("[Item] Registered " + name + " (" + nextHighestId + ")");
		nextHighestId++;
	}
}
