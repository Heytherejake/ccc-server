package com.studio878.server.inventory;

public class WeaponStatistics {
	
	int clip, reserve, damage, reloadTime;
	double rof;
	
	public WeaponStatistics(int clip, int reserve, int reloadTime, int damage, double rof) {
		this.clip = clip;
		this.reserve = reserve;
		this.reloadTime = reloadTime;
		this.damage = damage;
		this.rof = rof;
	}
	
	public int getClipSize() {
		return clip;
	}
	
	public int getReserveSize() {
		return reserve;
	}
	
	public int getReloadTime() {
		return reloadTime;
	}
	
	public int getDamage() {
		return damage;
	}
	
	public double getRateOfFireModifier() {
		return rof;
	}
	
	public String toString() {
		return clip + "/" + reserve + "/" + damage + "/" + rof;
	}

}
