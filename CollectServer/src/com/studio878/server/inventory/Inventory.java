package com.studio878.server.inventory;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.entities.Player;

public class Inventory {
	
	CopyOnWriteArrayList<Item> items = new CopyOnWriteArrayList<Item>();
	static int Slots = 10;
	Player owner;
	
	public Inventory(Player p) {
		owner = p;
	}
	public CopyOnWriteArrayList<Item> getItems() {
		return items;
	}
	
	public ArrayList<Item> getItems(ItemID i, int level) {
		ArrayList<Item> result = new ArrayList<Item>();
		for(Item it : items) {
			if(it.getType() == i && it.getLevel() == level) {
				result.add(it);
			}
		}
		return result;
	}
	
	public int countOpenSlots() {
		System.out.println(Slots - items.size());
		return Slots - items.size();
	}
	
	public Item getItemFromSlot(int i) {
		for(Item is : items) {
			if(is.getSlot() == i) {
				return is;
			}
		}
		return null;
	}
	public int countOpenPositions(ItemID i) {
		int amount = 0;
		for(Item it : items) {
			if(it.getType() == i && it.getType().getStackSize() >= it.getAmount()) {
				amount += it.getType().getStackSize() - it.getAmount();
			}
		}
		amount += (Slots-items.size() + 1)*i.getStackSize();
		return amount;
	}
	
	public int countOpenPositions(ItemID i, int level) {
		int amount = 0;
		for(Item it : items) {
			if(it.getType() == i && it.level == level && it.getType().getStackSize() >= it.getAmount()) {
				amount += it.getType().getStackSize() - it.getAmount();
			}
		}
		amount += (Slots-items.size() + 1)*i.getStackSize();
		return amount;
	}
	public Item getItem(ItemID i) {
		for(Item it : items) {
			if(it.getType() == i) {
				return it;
			}
		}
		return null;
	}
	
	public void clear() {
		items.clear();
	}
	
	public int countBlocks() {
		int count = 0;
		for(Item it : items) {
			if(it.getType().isBlock()) {
				count++;
			}
		}
		return count;
	}
	public int countWeapons() {
		int count = 0;
		for(Item it : items) {
			if(it.getType().getWeaponType() != 0) {
				count++;
			}
		}
		return count;
	}
	public Item getItem(ItemID i, int amount) {
		for(Item it : items) {
			if(it.getType() == i && it.getAmount() >= amount) {
				return it;
			}
		}
		return null;
	}
	
	public Item getItemFromSlot(ItemID i, int slot) {
		for(Item it : items) {
			if(it.getType() == i && it.getSlot() == slot) {
				return it;
			}
		}
		return null;
	}
	public boolean contains(ItemID i) {
		if(i == ItemID.lookupName("Coins")) {
			return (owner.getBalance() != 0);
		}
		for(Item it: items) {
			if(it.getType() == i) {
				return true;
			}
		}
		return false;
	}
	
	public boolean containsMatchLevel(ItemID i, int level) {
		for(Item it: items) {
			if(it.getType() == i && it.getLevel() == level) {
				return true;
			}
		}
		return false;
	}
	
	public boolean contains(ItemID i, int amount) {
		if(i == ItemID.lookupName("Coins")) {
			return owner.hasEnoughMoney(amount);
		}
		int total = 0;
		for(Item it: items) {
			if(it.getType() == i) {
				total += it.getAmount();
			}
		}
		return total >= amount;
	}
	
	public void removeItemFromSlot(int slot) {
		for(Item it: items) {
			if(it.getSlot() == slot) {
				items.remove(it);
			}
		}
	}
	public boolean containsSlot(ItemID i, int slot) {
		for(Item it: items) {
			if(it.getType() == i && it.getSlot() == slot) {
				return true;
			}
		}
		return false;
	}
	
	public void addItem(Item i) {
		if(i.getType() == ItemID.lookupName("Coins")) {
			owner.addMoney(i.getAmount());
			return;
		}
		ArrayList<Integer> slots = new ArrayList<Integer>();
			for(Item is : items) {
				slots.add(is.getSlot());
			}
			for(int j = 1; j < Slots; j++) {
				if(!slots.contains(j)) {
					items.add(i);
					i.setSlot(j);
					return;
				}
		}
			items.add(i);
			i.setSlot(0);
			owner.sendInventoryUpdate();

	}
	public void add(ItemID i, int level, int amount) {
		if(i == ItemID.lookupName("Coins")) {
			owner.addMoney(amount);
			return;
		}
		ArrayList<Item> its = getItems(i, level);
		for(Item it : its) {
			int amt =   i.getStackSize() - it.getAmount();
			if(amt <= amount) {
				it.setAmount(it.getAmount() + amt);
			} else {
				it.setAmount(it.getAmount() + amount);
			}
			amount -= amt;
		}
		while(amount > 0 && items.size() <= Slots) {
			int amt = amount - i.getStackSize();
			if(amt > i.getStackSize()) {
				amt = i.getStackSize();
			} else if (amt <= 0) {
				amt = amount;
			}
			addItem(new Item(i, amt, level, this.owner));
			amount -= amt;
		}
		owner.sendInventoryUpdate();
		

		
	}
	
	public void subtract(ItemID i, int level, int amount) {
		if(i == ItemID.lookupName("Coins")) {
			owner.subtractMoney(amount);
			return;
		}
		ArrayList<Item> its = getItems(i, level);
		for(Item it : its) {
			int amt = it.getAmount();
			if(amt > amount) {
				it.setAmount(amt- amount);
			} else {
				items.remove(it);
			}
			amount -= amt;
		}
		owner.sendInventoryUpdate();

	}
	
	
}
