package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet29SendBullet;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class Bullet {
	double x, y;
	Weapon source;
	double yspeed;
	double xspeed;
	protected Player player;
	public Bullet(Weapon source, double angle, double x, double y, Player player) {
		this.source = source;
		this.player = player;
		angle += Math.PI/2;
		yspeed = 10*Math.sin(angle);
		xspeed = 10*Math.cos(angle);
		this.x = x;
		this.y = y;
		startMovement();
		player.getHandle().getDispatch().broadcastPacket(new Packet29SendBullet(player.getHandle(), x, y, angle, source.getItem().getType()));


	}

	public void startMovement() {
		boolean canContinue = false;

		while(!canContinue) {
			x += xspeed;
			y += yspeed;
			for(HealthEntity e: EntityManager.getHealthEntities()) {
				if(x >= e.getX() && x <= e.getX() + e.getWidth() && y >= e.getY() && y <= e.getY() + e.getHeight()) {
					if(e instanceof HealthEntity) {
						HealthEntity h = (HealthEntity) e;
						if(h.getTeam() == null || h.getTeam() != player.getTeam()) {
							if(e instanceof Player) {
								Player p = (Player) e;
								if(!p.getName().equals(player.getName())) {
									source.onHit(e);
									canContinue = true;
								}
							} else {
								source.onHit(e);
								canContinue = true;
							}
						}

					}
				}
			}

			Block b = Grid.getClosestBlock((int)  x, (int) y);
			if(b != null && !b.getType().getPermiability(null)){
				source.onHit(b);
				canContinue = true;
			}

			if(x < 0 || y < 0 || x > Settings.WorldHeight*16 || y > Settings.WorldWidth*16) {
				canContinue = true;
			}
		}
	}
}
