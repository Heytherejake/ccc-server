package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;
import com.studio878.server.logging.Console;

public abstract class Weapon {
	int damage;
	Player owner;
	int time = 50;
	Item item;
	int reloadTime = 2000;
	int level = 0;

	public Weapon(Player owner, Item source) {
		this.owner = owner;
		item = source;
		try {
			this.level = source.getLevel();
		} catch (Exception e) {

		}
	}

	public abstract void onHit(Entity e);
	public abstract void onHit(Block b);

	public int getTimeOffset() {
		return time;
	}
	public void fireBullet(double angle, double d, double e) {
		Bullet f = new Bullet(this, angle, d, e, owner);

	}

	public Item getItem() {
		return item;
	}

	public static Weapon matchWeapon(Player p, Item id) {
		Weapon w;
		String name = id.getType().getName();
		if(name.equals("Pistol")) {
			w = new Pistol(p, id);
		} else if(name.equals("Shovel")) {
			w = new Shovel(p, id);
		} else if(name.equals("Submachine Gun")) {
			w = new SMG(p, id);
		} else if(name.equals("Shotgun")) {
			w = new Shotgun(p, id);
		} else if(name.equals("Sniper Rifle")) {
			w = new SniperRifle(p, id);
		} else if(name.equals("Rocket Launcher")) {
			w = new RocketLauncher(p, id);
		} else if(name.equals("Flintlock Pistol")) {
			w = new FlintlockPistol(p, id);
		} else if(name.equals("Wrench")) {
			w = new Wrench(p, id);
		} else if(name.equals("Sword")) {
			w = new Sword(p, id);
		} else if(name.equals("Hatchet")) {
			w = new Hatchet(p, id);
		} else {
			if(id.getType().isBlock()) {
				w = new BlockWeapon(p, id);
				return w;
			}
			Console.error("Invalid weapon name");
			return null;
		}
		return w;
	}
}
