package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;

public class BlockWeapon extends Weapon {

	public BlockWeapon(Player owner, Item is) {
		super(owner, is);
		time = 500;
	}


	public void onHit(Entity e) {
	}

	
	public void onHit(Block b) {
	}
	
	public void fireBullet(double angle, double d, double e) {
	}


}
