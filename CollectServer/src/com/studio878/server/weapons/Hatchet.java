package com.studio878.server.weapons;

import java.util.ArrayList;

import com.studio878.server.block.Block;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;
import com.studio878.server.packets.Packet09SendBlockDamage;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.world.Grid;

public class Hatchet extends MeleeWeapon {

	public Hatchet(Player owner, Item is) {
		super(owner, 50, is);
		time = 5;
	}


	public void onHit(Entity e) {
		HealthEntity h = (HealthEntity) e;
		h.damage((int) (item.getDamage() + owner.getCharacter().getMeleeDamage()), DamageMethod.Hatchet, owner.getName());
	}


	public void onHit(Block b) {
		ArrayList<Block> blocks = Grid.getCircle(b.getX(), b.getY(), 3);
		for(Block bs: blocks) {
			bs.hit(owner, 25, (byte) 4);
			owner.getHandle().getDispatch().broadcastPacket(new Packet09SendBlockDamage(owner.getHandle(), bs.getX(), bs.getY(), bs.getCondition()));
		}
	}

	public void fireBullet(double angle, double x, double y) {
		for(HealthEntity e: EntityManager.getHealthEntities()) {
			if(x >= e.getX() && x <= e.getX() + e.getWidth() && y >= e.getY() && y >= e.getY() + e.getHeight()) {
				onHit(e);
			}
		}
	}

}
