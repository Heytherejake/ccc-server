package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;
import com.studio878.server.packets.Packet09SendBlockDamage;
import com.studio878.server.util.Damage.DamageMethod;

public class FlintlockPistol extends Weapon {

	public FlintlockPistol(Player owner, Item is) {
		super(owner, is);
		time = 500;

	}


	public void onHit(Entity e) {
		HealthEntity h = (HealthEntity) e;
		h.damage((int) (item.getDamage() + owner.getCharacter().getRangedDamage()), DamageMethod.Pistol, owner.getName());
	}

	
	public void onHit(Block b) {
		b.hit(owner, 45, (byte) 4);
		owner.getHandle().getDispatch().broadcastPacket(new Packet09SendBlockDamage(owner.getHandle(), b.getX(), b.getY(), b.getCondition()));
	}
	
	public void fireBullet(double angle, double d, double e) {
		Bullet f = new Bullet(this, angle + Math.random()*(Math.PI/20) - Math.PI/20, d, e, owner);
	}


}
