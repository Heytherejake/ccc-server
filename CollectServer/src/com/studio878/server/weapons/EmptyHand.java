package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.Player;

public class EmptyHand extends Weapon {

	public EmptyHand(Player owner) {
		super(owner, null);
		time = 500;
		item = null;

	}


	public void onHit(Entity e) {
	}

	
	public void onHit(Block b) {
	}
	
	public void fireBullet(double angle, double d, double e) {
	}


}
