package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;

public class MeleeWeapon extends Weapon {

	int width;
	
	public MeleeWeapon(Player owner, int width, Item is) {
		super(owner, is);
		this.width = width;
	}
	
	public int getWidth() {
		return width;
	}

	public void onHit(Entity e) {
		
	}

	public void onHit(Block b) {
		
	}

}
