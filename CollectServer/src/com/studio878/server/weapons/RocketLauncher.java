package com.studio878.server.weapons;

import com.studio878.server.block.Block;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.entities.Rocket;
import com.studio878.server.inventory.Item;
import com.studio878.server.packets.Packet09SendBlockDamage;
import com.studio878.server.packets.Packet15AddEntity;
import com.studio878.server.util.Damage.DamageMethod;

public class RocketLauncher extends Weapon {

	public RocketLauncher(Player owner, Item is) {
		super(owner, is);
		time = 2000;
	}


	public void onHit(Entity e) {
		HealthEntity h = (HealthEntity) e;
		h.damage((int) (item.getDamage() + owner.getCharacter().getRangedDamage()), DamageMethod.SniperRifle, owner.getName());
	}

	
	public void onHit(Block b) {
		b.hit(owner, 60, (byte) 4);
		owner.getHandle().getDispatch().broadcastPacket(new Packet09SendBlockDamage(owner.getHandle(), b.getX(), b.getY(), b.getCondition()));
	}
	public void fireBullet(double angle, double d, double e) {
		Rocket r = new Rocket(d, e-15, angle,  owner.getName());
		angle += Math.PI/2;
		r.setXSpeed(10*Math.cos(angle));
		r.setYSpeed(10*Math.sin(angle));
		r.setData(owner.getName());
		EntityManager.registerEntity(r);
		owner.getHandle().getDispatch().broadcastPacket(new Packet15AddEntity(owner.getHandle(), r));

	}

}
