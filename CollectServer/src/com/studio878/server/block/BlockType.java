package com.studio878.server.block;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.studio878.server.entities.Entity;
import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;
import com.studio878.server.util.TeamManager.Team;

public class BlockType {
	
	static final byte Durability_Lowest = 1;
	static final byte Durability_Lower = 2;
	static final byte Durability_Low = 3;
	static final byte Durability_Medium = 4;
	static final byte Durability_High = 5;
	static final byte Durability_Higher = 6;
	static final byte Durability_Highest = 7;
	static final byte Durability_Indestructable = -1;
	
	public static int nextHighestId = 0;
	private static final ConcurrentHashMap<Integer, BlockType> lookupId = new ConcurrentHashMap<Integer, BlockType>();
	private static final ConcurrentHashMap<String, BlockType> lookupName = new ConcurrentHashMap<String, BlockType>();
	
	private int id, luminocity, position;
	private boolean physicsEnabled;
	private byte d;
	private boolean ps;
	private boolean fluid, electric, rounded, transparent;
	int sound_id;
	private String drop;
	String name;
	public BlockType(int id, String name, boolean p, byte d, boolean permiable, int sound_id, int position, boolean transparent, boolean rounded, boolean fluid, boolean electric, int luminocity, String drop) {
		this.id = id;
		this.name = name;
		this.physicsEnabled = p;
		this.position = position;
		this.d = d;
		this.ps = permiable;
		this.fluid = fluid;
		this.electric = electric;
		this.drop = drop;
		this.sound_id = sound_id;
		this.transparent = transparent;
		this.rounded = rounded;
		this.luminocity = luminocity;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public ItemID getDrop() {
		return drop == null ? null : ItemID.lookupName(drop);
	}

	public byte getDurability() {
		return d;
	}

	public boolean isFluid() {
		return fluid;
	}

	public boolean isPhysicsEnabled() {
		return physicsEnabled;
	}

	public boolean isElectric() {
		return electric;
	}
	
	public int getSoundId() {
		return sound_id;
	}

	public int getPosition() {
		return position;
	}
	
	public int getLuminocity() {
		return luminocity;
	}
	
	public boolean isRounded() {
		return rounded;
	}
	
	public boolean isTransparent() {
		return transparent;
	}
	
	public static BlockType lookupId(int i) {
		return lookupId.get(i);
	}
	
	public static BlockType lookupName(String name) {
		return lookupName.get(name);
	}
	
	public static ConcurrentHashMap<Integer, BlockType> getBlockList() {
		return lookupId;
	}
	
	public boolean getPermiability(Entity e) {
		if(e == null) {
			return ps;
		}

		if(this.getName().equals("Red Barrier")) {
			if(e instanceof Player) {
				Player p = (Player) e;
				if(p.getTeam() == Team.Red) {
					return true;
				} else {
					return false;
				}
			}
		}
		if(this.getName().equals("Blue Barrier")) {
			if(e instanceof Player) {
				Player p = (Player) e;
				if(p.getTeam() == Team.Blue) {
					return true;
				} else {
					return false;
				}
			}
		}
		return ps;
	}
	
	public static void populateDefaultBlocks() {
		addBlock("Air", false, Durability_Indestructable, true, 0, 0, true, false, false, false, 0, null);
		addBlock("Stone", false, Durability_Medium, false, 2, 1, false, true, false, false, 0, "Stone");
		addBlock("Dirt", true, Durability_Lower, false, 1,2, false, true, false, false, 0, "Dirt");
		addBlock("Grass", true, Durability_Lower, false, 1,3, false, true, false, false, 0, "Dirt");
		addBlock("Lava", false, Durability_Indestructable, true, 0, 4, true, false, true, false, 3, null);
		addBlock("Iron", false, Durability_Higher, false, 2, 5, false, true, false, false, 0, "Iron");
		addBlock("Titanium Block", false, Durability_Indestructable, false, 2, 7, false, false, false, false, 0, null);
		addBlock("Red Barrier", false, Durability_Indestructable, false, 0, 9, true, false, false, false, 0, null);
		addBlock("Blue Barrier", false, Durability_Indestructable, false, 0, 8, true, false, false, false, 0, null);
		addBlock("Sulfur", false, Durability_High, false, 2, 6, false, true, false, false, 0, "Sulfur"); 
		addBlock("Sand", false, Durability_Lower, false, 1, 10, false, true, false, false, 0, "Sand");
		addBlock("Coal", false, Durability_Higher, false, 2, 12, false, true, false, false, 0, "Coal");
		addBlock("Copper", false, Durability_Higher, false, 2, 11, false, true, false, false, 0, "Copper");
		addBlock("Wood", false, Durability_Higher, false, 2, 13, false, true, false, false, 0, "Wood");
		addBlock("Mossy Stone", false, Durability_Medium, false, 2, 14, false, true, false, false, 0, null);
		addBlock("Torch", false, Durability_Lowest, true, 0, 15, true, true, false, false, 10, "Torch");
		addBlock("Steel Girder",  false, Durability_Highest, false, 2, 16, true, true, false, false, 0, null);
		addBlock("Vertical Steel Girder", false, Durability_Highest, false, 2, 17, true, true, false, false, 0, null);
		addBlock("Steel Pipe", false, Durability_Highest, true, 2, 18, true, true, false, false, 0, null);
		addBlock("Vertical Steel Pipe", false, Durability_Highest, true, 2, 19, true, true, false, false, 0, null);
		addBlock("Floodlight", false, Durability_Higher, true, 0, 20, true, false, false, false, 15, null);
		addBlock("Chainlink Fence", false, Durability_Highest, true, 2, 21, false, false, false, false, 0, null);
		addBlock("Gray Background", false, Durability_Indestructable, false, 2, 22, false, false, false, false, 0, null);
		addBlock("Room-Fill Light Source", false, Durability_Indestructable, false, 0, 20, true, false, false, false, 10, null);
		addBlock("Tree Stump", false, Durability_Medium, true, 0, 23, true, false, false, false, 0, "Wood");
		addBlock("Tree Trunk", false, Durability_Medium, true, 0, 24, true, false, false, false, 0, "Wood");
		addBlock("Tree Top", false, Durability_Medium, true, 0, 25, true, false, false, false, 0, "Wood");
		addBlock("Brick", false, (byte) 30, false, 2, 26, false, true, false, false, 0, "Brick");
		addBlock("Crate", false, Durability_High,false, 0, 27, false, false, false, false, 0, null);
		addBlock("Water", false, Durability_Indestructable, true, 0, 28, true, false, true, false, 0, null);
		addBlock("Vertical Wire", false, Durability_Indestructable, true, 0, 29, true, false, false, true, 0, null);
		addBlock("Wire", false, Durability_Indestructable, true, 0, 30, true, false, false, true, 0, null);
		addBlock("And Gate", false, Durability_Indestructable, true, 0, 31, true, false, false, true, 0, null);
		addBlock("Or Gate", false, Durability_Indestructable, true, 0, 32, true, false, false, true, 0, null);
		addBlock("Not Gate", false, Durability_Indestructable, true, 0, 33, true, false, false, true, 0, null);
		addBlock("Nor Gate", false, Durability_Indestructable, true, 0, 34, true, false, false, true, 0, null);
		addBlock("Xor Gate", false, Durability_Indestructable, true, 0, 35, true, false, false, true, 0, null);
		addBlock("Battery", false, Durability_Indestructable, true, 0, 36, true, false, false, true,  0, null);
	}

	public static void addBlock(String name, boolean p, byte d, boolean permiable, int sounds, int position, boolean transparent, boolean rounded, boolean fluid, boolean electric, int luminocity, String drop) {
		BlockType i = new BlockType(nextHighestId, name, p, d, permiable, sounds, position, transparent, rounded, fluid, electric, luminocity, drop);
		lookupId.put(nextHighestId, i);
		lookupName.put(name, i);
		Console.log("[Block] Registered " + name + " (" + nextHighestId + ")");
		nextHighestId++;
	}


}
