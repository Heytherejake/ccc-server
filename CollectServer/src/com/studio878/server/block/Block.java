package com.studio878.server.block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.events.BlockCreatedEvent;
import com.studio878.server.events.BlockDamagedEvent;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.packets.Packet14SendBlockUpdate;
import com.studio878.server.util.Location;
import com.studio878.server.util.TeamManager.Team;
import com.studio878.server.world.Grid;



public class Block {



	static Random dropCalculator = new Random();
	public static CopyOnWriteArrayList<Block> fluidList = new CopyOnWriteArrayList<Block>();
	public static CopyOnWriteArrayList<Block> wireBlocks = new CopyOnWriteArrayList<Block>();

	int x;
	int y;
	BlockType type, foregroundType, decorationType, wireType;
	int dat, wireData;
	byte condition = 10;
	short ticks = 0;
	byte squareLocation;
	boolean dropOnBreak = true;
	BlockType bgt;

	//Square Location:
	// 0 1
	// 2 3

	public Block(BlockType t, int d, int x, int y, byte sql) {
		type = t;
		dat = d;
		this.x = x;
		this.y = y;
		squareLocation = sql;
		bgt = t;
		if(t.isFluid()) {
			fluidList.add(this);
		}
	}
	public Block(Block b) {
		type = b.getType();
		dat = b.getData();
		condition = b.getCondition();
		x = b.getX();
		y = b.getY();
		ticks = b.getTicks();
		squareLocation = b.getSquareLocation();
	}

	public void setType(BlockType t) {
		BlockType pt = type;
		type = t;
		condition = 10;
		ticks = 0;

		if(pt.isFluid()) {
			fluidList.remove(this);
		}
		if(t.isFluid()) {
			fluidList.add(this);
		}

		checkWallDrop(this);
		sendUpdate();

	}

	public void checkWallDrop(Block source) {
		ArrayList<Block> blocks = getAdjacentBlocks();
		boolean hasEdge = false;
		for(Block b : blocks) {
			if(!b.getType().getPermiability(null)) {
				hasEdge = true;
			}
			if(b.getType() == BlockType.lookupName("Torch") && source != this) {
				b.checkWallDrop(this);
			}
		}
		if(!hasEdge && type == BlockType.lookupName("Torch")) {
			setType(BlockType.lookupName("Air"));
			EntityManager.dropItem(ItemID.lookupName("Torch"), x*16, y*16);
		}
	}

	public ArrayList<Block> getAdjacentBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		Block b = getBlockAdjacent(0, -1);
		if(b != null) {
			result.add(b);
		}
		b = getBlockAdjacent(0, 1);
		if(b != null) {
			result.add(b);
		}
		b = getBlockAdjacent(1, 0);
		if(b != null) {
			result.add(b);
		}
		b = getBlockAdjacent(-1, 0);
		if(b != null) {
			result.add(b);
		}
		return result;
	}

	public ArrayList<Block> getAdjacentAndDiagonalBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				Block b = getBlockAdjacent(i, j);
				if(b != null) {
					result.add(b);
				}
			}
		}
		return result;
	}


	public byte getSquareLocation() {
		return squareLocation;
	}

	public void setBackgroundType(BlockType t) {
		bgt = t;
	}
	public Location getLocationOnGrid() {
		return new Location(x, y);
	}
	public short getTicks() {
		return ticks;
	}
	public void shiftDown(int shift) {
		Grid.setBlockAt(x, y + shift, this);
		sendUpdate();
		Grid.getBlockAt(x, y + shift).sendUpdate();
	}
	public Block getBlockAdjacent(int x, int y) {
		Location t = getLocationOnGrid();
		try {
			return (Grid.getGrid()[t.getX() + x][t.getY() + y]);
		} catch(ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}

	public ArrayList<Block> getSurroundingBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		result.add(this.getBlockAdjacent(1, 0));
		result.add(this.getBlockAdjacent(-1, 0));
		result.add(this.getBlockAdjacent(0, 1));
		result.add(this.getBlockAdjacent(0, -1));
		return result;
	}

	public void setData(int d) {
		dat = d;
		sendUpdate();
	}
	public void setCondition(byte c) {
		condition = c;
		sendUpdate();

	}
	public BlockType getBackgroundType() {
		return bgt;
	}

	public void setForegroundType(BlockType t) {
		foregroundType = t;
	}

	public void setDecorationType(BlockType t) {
		decorationType = t;
	}

	public int getForegroundId() {
		try {
			return foregroundType.getId();
		} catch (NullPointerException e) {
			return -1;
		}
	}

	public int getDecorationId() {
		try {
			return decorationType.getId();
		} catch (NullPointerException e) {
			return -1;
		}
	}

	public BlockType getType() {
		return type;
	}
	public int getData() {
		return dat;
	}
	public void setSquareLocation(byte s) {
		squareLocation = s;
	}
	public byte getCondition() {
		return condition;
	}
	public int getX() { 
		return x;
	}
	public int getY() {
		return y;
	}
	public byte getOffset() {
		return squareLocation;
	}
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}

	public int getWireData() {
		return wireData;
	}

	public void setWireData(int data) {
		int pd = wireData;
		wireData = data;
	}

	public void setWireType(BlockType type) {
		BlockType wt = wireType;
		this.wireType = type;
		if(type.isElectric()) {
			wireBlocks.add(this);
		}
		if(wt != null && wt.isElectric()) {
			wireBlocks.remove(this);
		}
	}

	public int getWireId() {
		try {
			return wireType.getId();
		} catch (NullPointerException e) {
			return -1;
		}
	}

	public ArrayList<Block> getAdjacentWires() {
		ArrayList<Block> result = new ArrayList<Block>();
		Block b = getBlockAdjacent(0, -1);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(0, 1);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(1, 0);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		b = getBlockAdjacent(-1, 0);
		if(b != null && b.getWireType() != null && b.getWireType().isElectric()) {
			result.add(b);
		}
		return result;
	}
	public boolean isConnectedToBattery(ArrayList<Block> source, Block prev) {
		boolean ic = false;
		ArrayList<Block> adj = getAdjacentWires();

		if(adj.size() == 4) {
			Block b1 = getBlockAdjacent(1, 0);
			boolean rc = (b1 != null && b1.getWireType() != null && b1.getWireType().isElectric());
			Block b2 = getBlockAdjacent(-1, 0);
			boolean lc = (b2 != null && b2.getWireType() != null && b2.getWireType().isElectric());
			Block b3 = getBlockAdjacent(0, 1);
			boolean dc = (b3 != null && b3.getWireType() != null && b3.getWireType().isElectric());
			Block b4 = getBlockAdjacent(0, -1);
			boolean tc = (b4 != null && b4.getWireType() != null && b4.getWireType().isElectric());
			boolean lr = rc || lc;
			boolean td = dc || tc;
			source.remove(this);
			if(lr && prev.getY() == this.y) {
				return b1.isConnectedToBattery(source, this) || b2.isConnectedToBattery(source, this);

			}
			if(td && prev.getX() == this.x) {
				return b3.isConnectedToBattery(source, this) || b4.isConnectedToBattery(source, this);

			}

		} else {
			if(adj.size() == 3) {
				source.remove(this);
			}
			for(Block b : adj) {
				boolean cont = source.contains(b) || b == prev;
				if(!cont) {
					source.add(b);
				}
				if(!cont) {
					Block b1 = b.getBlockAdjacent(0, -1);
					Block b2 = b.getBlockAdjacent(-1, 0);
					String ns = b.getWireType().getName();
					boolean b1v = isValidAndConnectedBlock(b1, source);
					boolean b2v = isValidAndConnectedBlock(b2, source);

					if(ns.equals("Battery")) {
						return true;
					} else if(ns.equals("And Gate")) {
						if(isValidAndConnectedBlock(b1, source) && isValidAndConnectedBlock(b2, source)) {
							return true;
						}
					} else if(ns.equals("Or Gate")) {
						if((isValidAndConnectedBlock(b2, source) && Math.abs(this.getX() - b.getX()) == 1) || (isValidAndConnectedBlock(b1, source) && this.getX() - b.getX() == 1)) {
							return true;
						}
					} else if(ns.equals("Nor Gate")) {
						if((!isValidAndConnectedBlock(b1, source) && !b2v && this.getX() - b.getX() == 1)) {
							return true;
						} else if(this.getX() - b.getX() == -1 && b2v) {
							return true;
						}
					} else if(ns.equals("Xor Gate")) {		
						if(((b1v && !b2v) || (!b1v && b2v)) && this.getX() - b.getX() == 1) {
							return true;
						} else if(this.getX() - b.getX() == -1 && b2v) {
							return true;
						}

					} else if(ns.equals("Not Gate")) {	
						if(this.getX() - b.getX() == 1) {
							return !isValidAndConnectedBlock(b2, source);
						}
					}

					if(b.isWire()) {
						try {
							if(b.isConnectedToBattery(source, this)) {

								ic = true;
							}
						} catch (StackOverflowError e) {
							System.out.println("Stack overflow in battery connection logic");
						}
					}
				}
			}
		}

		return ic;
	}


	public boolean isWire() {
		return wireType == BlockType.lookupName("Wire") || wireType == BlockType.lookupName("Vertical Wire");
	}

	public void powerSurroundingBlocks(boolean on) {
		for(Block b : getAdjacentBlocks()) {
			if(b.isWire()) {
				b.setWireData(on ? 1 : 0);
			}
		}
	}

	private boolean isValidAndConnectedBlock(Block b, ArrayList<Block> source) {
		return (b != null && b.getWireType() != null && b.getWireType().isElectric() && b.isConnectedToBattery(source, b));
	}

	public BlockType getWireType() {
		return wireType;
	}

	public void create(Player p) {
		if(!Hooks.call(Hook.BLOCK_CREATED, new BlockCreatedEvent(p, this))) {
			this.setType(BlockType.lookupName("Air"));
		}
	}

	public void setDropOnBreak(boolean b) {
		dropOnBreak = b;
	}

	public void hit(Player p, int tick, byte damage) {
		if(type.getDurability() != BlockType.Durability_Indestructable && condition > 0) {
			if(Hooks.call(Hook.BLOCK_DAMAGED, new BlockDamagedEvent(p, this))) {
				this.ticks += tick;
				while(this.ticks >= type.getDurability()*15 && type.getDurability() != BlockType.Durability_Indestructable) {
					this.condition = (byte) (condition - damage);
					if(this.getCondition() <= 0) {
						destroy(p);
					}
					this.ticks -= type.getDurability()*15;
				}
				sendUpdate();
			}
		}
	}

	public void damage(Player p, byte amount) {
		if(type.getDurability() != BlockType.Durability_Indestructable && condition > 0) {
			if(Hooks.call(Hook.BLOCK_DAMAGED, new BlockDamagedEvent(p, this))) {
				this.setCondition((byte) (this.getCondition() - amount));
				if(this.getCondition() <= 0) {
					destroy(p);
				}		
			}
		}
	}
	public void destroy(Player p) {
		if(this.getType().getDurability() == BlockType.Durability_Indestructable) {
			return;
		}

		dropItem();
		if(type == BlockType.lookupName("Tree Stump") || type == BlockType.lookupName("Tree Trunk")) {
			Block b;
			while(((b = getBlockAdjacent(0, -1)) != null) && (b.getType() == BlockType.lookupName("Tree Trunk") || b.getType() == BlockType.lookupName("Tree Top"))) {
				b.destroy(p);
			}
		}
		if(type == BlockType.lookupName("Tree Top")|| type == BlockType.lookupName("Tree Trunk")) {
			Block b = getBlockAdjacent(0, 1);
			if(b != null && b.getType() == BlockType.lookupName("Tree Trunk")) {
				b.setType(BlockType.lookupName("Tree Top"));
			}
		}
		this.setType(BlockType.lookupName("Air"));
		Grid.updatePhysicsAtBlock(x, y);
		this.condition = 10;

		Block b = getBlockAdjacent(0, 1);


	}

	public void sendUpdate() {
		if(Main.getDispatch() != null && Main.getDispatch().getClients().size() > 0) {
			Main.getDispatch().broadcastPacket(new Packet14SendBlockUpdate(Main.getDispatch().getClients().get(0), this));
		}
	}

	private void dropItem() {
		if(!dropOnBreak) {
			return;
		}

		ItemID drop = null;
		String ns = type.getName();
		if(ns.equals("Sand")) {
			drop = setDrop(ItemID.lookupName("Silicon"), 10);
		}

		if(drop == null) {
			if(this.getType().getDrop() != null) {
				drop = this.getType().getDrop();
			} else {
				return;
			}
		}
		EntityManager.dropItem(drop, x*16, y*16);

	}

	private ItemID setDrop(ItemID id, int pct) {
		if(dropCalculator.nextInt(100) < pct) {
			return id;
		}
		return null;
	}
}
