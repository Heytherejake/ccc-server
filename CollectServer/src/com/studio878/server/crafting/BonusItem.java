package com.studio878.server.crafting;

import java.util.HashMap;

import com.studio878.server.entities.Player;

public class BonusItem {

	public enum BonusItemType {
		Movement (0),
		RateOfFire (1),
		JumpHeight (2),
		BuildRadius (3),
		DigRadius (4),
		CurrencyOutput (5),
		RangedDamage (6),
		MeleeDamage(7),
		Strength(8),
		Dexterity(9),
		Intelligence(10),
		Health(11),
		Mana(12),
		Armor(13),
		MagicDefence(14),
		HealthRegen(15),
		ManaRegen(16),
		LifeSteal(17),
		;
		
		static HashMap<Integer, BonusItemType> lookupId = new HashMap<Integer, BonusItemType>();
		
		private int id;
		
		private BonusItemType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static BonusItemType lookupId(int i) {
			return lookupId.get(i);
		}
		
		static {
			for(BonusItemType t : values()) {
				lookupId.put(t.getId(), t);
			}
		}
	}
	
	BonusItemType type;
	double amount;
	
	public BonusItem(BonusItemType t, double a) {
		type = t;
		amount = a;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public BonusItemType getType() {
		return type;
	}

}
