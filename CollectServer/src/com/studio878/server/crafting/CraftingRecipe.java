package com.studio878.server.crafting;

import java.awt.Point;

import com.studio878.server.characters.PlayerCharacter;
import com.studio878.server.crafting.CraftingRecipeList.RecipeType;
import com.studio878.server.inventory.Item;


public class CraftingRecipe {
	
	int id;
	int buyCost;
	String name, description;
	Item[] resources;
	Item result;
	RecipeType type;
	BonusItem[] bonuses;
	PlayerCharacter[] characters;
	Point img;
	String[] up, down;
	
	public CraftingRecipe(int id, String name, String description, Item result, Item[] resources, BonusItem[] bonuses, RecipeType type, PlayerCharacter[] characters, int buyCost, Point img, String[] up, String[] down) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.result = result;
		this.resources = resources;
		this.type = type;
		this.bonuses = bonuses;
		this.characters = characters;
		this.buyCost = buyCost;
		this.img = img;
		this.up = up;
		this.down = down;
	}
	
	public String[] getBenefits() {
		return up;
	}
	
	public String[] getDownsides() {
		return down;
	}
	
	public int getBuyCost() {
		return buyCost;
	}
	
	
	public Point getImage() {
		return img;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public PlayerCharacter[] getRestrictedCharacters() {
		return characters;
	}
	
	public boolean isValidForClass(PlayerCharacter cs) {
		if(characters == null) {
			return true;
		}
		for(PlayerCharacter s : characters) {
			if(cs.getClass() == s.getClass()) {
				return false;
			}
		}
		return true;
	}
	
	public RecipeType getType() {
		return type;
	}
	
	public BonusItem[] getBonuses() {
		return bonuses;
	}
	
	public Item getResult() {
		return result;
	}

	public int getId() {
		return id;
	}
	
	public Item[] getRequirements() {
		return resources;
	}
}
