package com.studio878.server.crafting;

import java.awt.Point;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.characters.PlayerCharacter;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;

public class CraftingRecipeList {

	public enum RecipeType {
		Weapon(0),
		Material(1),
		Research(2);
		
		int id;
		
		private RecipeType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
	}
	
	static int nextHighestId = 0;
	public static CopyOnWriteArrayList<CraftingRecipe> Recipes = new CopyOnWriteArrayList<CraftingRecipe>();
	
	public static CraftingRecipe getRecipe(int id) {
		for(CraftingRecipe r : Recipes) {
			if(r.getId() == id) {
				return r;
			}
		}
		return null;
	}
	
	public static CraftingRecipe lookupRecipe(ItemID result) {
		for(CraftingRecipe r : Recipes) {
			if(r.getResult() != null && r.getResult().getType() == result) {
				return r;
			}
		}
		return null;
	}
	public static void addRecipe(String name, String description, Item result, Item[] resources, BonusItem[] bonuses, RecipeType type, PlayerCharacter[] specs, int buyCost, Point image, String[] up, String[] down) {
		Recipes.add(new CraftingRecipe(nextHighestId, name, description, result, resources, bonuses, type, specs, buyCost, image, up, down));
		Console.log("Recipe added: " + name+ " (" + nextHighestId + ")");
		nextHighestId++;
	}
	
	public static void populateDefaultRecipes() {
		addRecipe("Pistol", "A standard pistol. Fires fast and accurately. You probably should be carrying one of these early on, unless you enjoy both losing and death.", new Item(ItemID.lookupName("Pistol"), 1, null), new Item[]{new Item(ItemID.lookupName("Iron"), 10, null), new Item(ItemID.lookupName("Sulfur"), 5, null)}, null, RecipeType.Weapon, null, 400, null, new String[]{"Medium rate of fire", "Low spread"}, new String[]{"Small clip"});
		addRecipe("Submachine Gun", "A rapid-fire that fires a spray of bullets that hit for less damage.", new Item(ItemID.lookupName("Submachine Gun"), 1, null), new Item[]{new Item(ItemID.lookupName("Sulfur"), 10, null), new Item(ItemID.lookupName("Iron"), 15, null)}, null, RecipeType.Weapon, null, 750, null, new String[]{"High rate of fire", "Large clip"}, new String[]{"Very inaccurate"});
		addRecipe("Rocket Launcher", "Fires powerful missiles that damage enemies as well as blocks. Favorite weapon of soldiers and rebels everywhere.", new Item(ItemID.lookupName("Rocket Launcher"), 1, null), new Item[]{new Item(ItemID.lookupName("Grenade"), 4, null), new Item(ItemID.lookupName("Iron"), 25, null), new Item(ItemID.lookupName("Sulfur"), 20, null), new Item(ItemID.lookupName("Copper Wire"), 5, null)}, null, RecipeType.Weapon, null, 1500, null, new String[]{"Heavy damage", "Large radius"}, new String[]{"Small clip", "Slow rate of fire"});
		addRecipe("1-3 Diethylbenzene", "Although initially devloped by the Researcher as an aphrodesiac, this substance was shown to cause immense muscular growth in test subjects. Also, cancer.",  null, null, new BonusItem[]{new BonusItem(BonusItemType.Movement, 0.1)}, RecipeType.Research, null, 700, new Point(0, 0), new String[]{"Increases movement speed by 10%"}, new String[]{"Affects only you"});
		addRecipe("Triphenylphosphane", "Re-discovered in 1970, ancient inscriptions show that this substance was initially used by the ancient Romans as artifical snow during ski season. As a side effect, it causes uncontrollable arm spasms.", null, null,  new BonusItem[]{new BonusItem(BonusItemType.RateOfFire, 0.1)}, RecipeType.Research, null, 1200, new Point(1, 0), new String[]{"Increases rate of fire by 10%"}, new String[]{"Affects only you"});
		addRecipe("2-Amino-1-Nitropropane-1-1", "The first substance developed entirely by the machines, this substance was used to increase the rate of fire in kill-bots. Translated from it's native binary, it means \"Drink of the Ancient Owl\"", null, null, new BonusItem[]{new BonusItem(BonusItemType.JumpHeight, 0.2), new BonusItem(BonusItemType.Movement, -0.05)}, RecipeType.Research, null, 1200, new Point(2, 0), new String[]{"Increases jump height by 20%"}, new String[]{"Reduces movement speed by 5%", "Affects only you"});
		addRecipe("1,1,1,1-Methanetetrayltetrabenzene", "Codenamed \"Vertigo\", this substance induces a adrelaline-fueled rage in test subjects, followed by mental dystrophy. Uno, dos, tres, quatorte.", null, null, new BonusItem[]{new BonusItem(BonusItemType.RateOfFire, 0.05), new BonusItem(BonusItemType.Movement, 0.05)}, RecipeType.Research, null, 1500, new Point(3, 0), new String[]{"Increases rate of fire by 5%", "Increases move speed by 5%"}, new String[]{"Affects only to you"});
		//Recipes.add(new CraftingRecipe(7, null, new Item[]{new Item(ItemID.lookupName("Coins"), 250, null)}, new BonusItem[]{new BonusItem(BonusItemType.DigRadius, 1)}, RecipeType.Research, new Specialization[]{Specialization.Researcher}));
		addRecipe("Copper Wire", "A necessary component in most electronics. Despite the fact that copper is really common, we've marked it up a bunch to discourage buying. Start searching.", new Item(ItemID.lookupName("Copper Wire"), 2, null), new Item[]{new Item(ItemID.lookupName("Copper"), 1, null)}, null, RecipeType.Material, null, 150, null, new String[]{"Used to craft electronics"}, null);
		addRecipe("Integrated Circuit", "A microchip that controls essential functions for electronics. It's value is Circuit * x + C", new Item(ItemID.lookupName("Integrated Circuit"), 1, null), new Item[]{new Item(ItemID.lookupName("Copper Wire"), 1, null), new Item(ItemID.lookupName("Silicon"), 1, null)}, null, RecipeType.Material, null, 200, null, new String[]{"Used to craft electronics"}, null);
		addRecipe("Grenade", "An explosive canister that you can throw at people, bringing them pain and death. Grenades have a three second delay before explosion.", new Item(ItemID.lookupName("Grenade"), 3, null), new Item[]{new Item(ItemID.lookupName("Sulfur"), 6, null), new Item(ItemID.lookupName("Integrated Circuit"), 1, null)}, null, RecipeType.Weapon, null, 350, null, new String[]{"Damages a large area", "Very powerful"}, new String[]{"Timed detonation", "Consumable"});
		addRecipe("Landmine", "An electronic explosive that you can throw at the ground, bring suffering to those absent-minded enough to walk on it.", new Item(ItemID.lookupName("Landmine"), 4, null), new Item[]{new Item(ItemID.lookupName("Sulfur"), 5, null), new Item(ItemID.lookupName("Integrated Circuit"), 1, null), new Item(ItemID.lookupName("Iron"), 5, null), new Item(ItemID.lookupName("Copper"), 5, null)}, null, RecipeType.Weapon, null, 700, null, new String[]{"Detonates when a player walks over it", "Very powerful"}, new String[]{"Consumable"});
		addRecipe("Shotgun", "A high-damage, close-range firearm that fires five bullets in a wide spread. For passenger-side use only.", new Item(ItemID.lookupName("Shotgun"), 1, null), new Item[]{new Item(ItemID.lookupName("Iron"), 25, null), new Item(ItemID.lookupName("Sulfur"), 10, null), new Item(ItemID.lookupName("Coins"), 100, null)}, null, RecipeType.Weapon, null, 700, null, new String[]{"Heavy damage"}, new String[]{"Slow rate of fire", "Very inaccurate"});
		addRecipe("Flintlock Pistol", "An antique pistol that fires a devistating ball, but has to be reloaded after each use. It belongs in a museum.", new Item(ItemID.lookupName("Flintlock Pistol"), 1, null), new Item[]{new Item(ItemID.lookupName("Iron"), 50, null), new Item(ItemID.lookupName("Sulfur"), 25, null), new Item(ItemID.lookupName("Coins"), 200, null)}, null, RecipeType.Weapon, null, 1000, null, new String[]{"Heavy damage"}, new String[]{"Very small clip"});
		addRecipe("Wrench", "A metal tool used to turn things and upgrade sentries. It beats heads in nicely, but has very little reach.", new Item(ItemID.lookupName("Wrench"), 1, null), new Item[]{new Item(ItemID.lookupName("Iron"), 20, null)}, null, RecipeType.Weapon, null, 500, null, new String[]{"Heavy damage", "Melee Weapon"}, new String[]{"Slow attack rate"});
		addRecipe("Hatchet", "An axe used to cut trees down and limbs off. It's a miniature hatch.", new Item(ItemID.lookupName("Hatchet"), 1, null), new Item[]{new Item(ItemID.lookupName("Iron"), 20, null)}, null, RecipeType.Weapon, null, 500, null, new String[]{"Medium damage", "Faster swing", "Melee Weapon"}, new String[]{""});
		addRecipe("Torch", "A lighting device created from proprietary torch wood. It's outside the government and beyond the police.", new Item(ItemID.lookupName("Torch"), 5, null), new Item[]{new Item(ItemID.lookupName("Coal"), 1, null)}, null, RecipeType.Material, null, 50, null, new String[]{"Lights an area"}, new String[]{"Small lighting radius"});
	}
}
