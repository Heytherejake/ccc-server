package com.studio878.server.abilities;

import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;

public class Ability {
	
	String name;
	String desc;
	int cooldown;
	int[] manacost;
	long lastCast;
	
	public Ability(String name, int cooldown, int[] manacost, String desc) {
		this.name = name;
		this.desc = desc;
		this.cooldown = cooldown;
		this.manacost = manacost;
	}
	
	public void onCast(Player p) {
		Console.log(p.getName() + " cast ability: " + name);
	}
	
	public int getCooldown() {
		return cooldown;
	}
	
	public long getLastCastTime() {
		return lastCast;
	}
	
	public void setLastCastTime(long s) {
		lastCast = s;
	}
	
	
	public int getManaCost(int level) {
		return manacost[level];
	}

}
