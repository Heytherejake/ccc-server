package com.studio878.server.abilities;


public class Excavator4StonesThrow extends Ability {

	public Excavator4StonesThrow() {
		super("Stone's Throw", 15, new int[]{160, 180, 200, 240}, "Koen launches all the stone he is carrying at an enemy, dealing |10/15/20| damage per block. If an enemy character is killed this way, Koen gains |1/1/2| coins for each piece of stone fired.");
		
	}
}
