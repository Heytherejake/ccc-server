package com.studio878.server.abilities;


public class Excavator1MetalDetector extends Ability {

	public Excavator1MetalDetector() {
		super("Metal Detector", 10, new int[]{50, 60, 70, 80}, "Koen locates precious metals, reavling them regardless of nearby lighting for 4 seconds. \n\n Radius: |15/20/35/30| blocks");
	}
	
	public void onCast() {
		//Metal Detector
	}
}
