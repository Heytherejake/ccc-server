package com.studio878.server.characters;

import com.studio878.server.abilities.Ability;
import com.studio878.server.abilities.Excavator1MetalDetector;
import com.studio878.server.abilities.Excavator2Exhume;
import com.studio878.server.abilities.Excavator3DynamiteExcavation;
import com.studio878.server.abilities.Excavator4StonesThrow;
import com.studio878.server.entities.Player;

public class Researcher extends PlayerCharacter {

	public Researcher(Player p) {
		super(p);
		
		id = 2;
		
		fullName = "Yawln, the Researcher of the Rifts";
		shortName = "Researcher";		
		//abilities = new Ability[]{new Excavator1MetalDetector(), new Excavator2Exhume(), new Excavator3DynamiteExcavation(), new Excavator4StonesThrow()};
		rangedDamage = 65;
		meleeDamage = 43;
		
		strength = 10;
		dexterity = 10;
		intelligence = 20;
		
		health = 475;
		healthPerLevel = 120;
		
		mana = 550;
		manaPerLevel = 120;
		
		armor = 15;
		magicDefence = 20;
		
		healthRegen = 15;
		manaRegen = 20;
	}
}
