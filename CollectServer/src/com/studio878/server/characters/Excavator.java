package com.studio878.server.characters;

import com.studio878.server.abilities.Ability;
import com.studio878.server.abilities.Excavator1MetalDetector;
import com.studio878.server.abilities.Excavator2Exhume;
import com.studio878.server.abilities.Excavator3DynamiteExcavation;
import com.studio878.server.abilities.Excavator4StonesThrow;
import com.studio878.server.entities.Player;

public class Excavator extends PlayerCharacter {

	public Excavator(Player p) {
		super(p);
		
		id = 1;
		
		fullName = "Koen, the Arcane Excavator";
		shortName = "Excavator";		
		abilities = new Ability[]{new Excavator1MetalDetector(), new Excavator2Exhume(), new Excavator3DynamiteExcavation(), new Excavator4StonesThrow()};
		rangedDamage = 70;
		meleeDamage = 40;
		
		strength = 10;
		dexterity = 20;
		intelligence = 10;
		
		health = 525;
		healthPerLevel = 125;
		
		mana = 425;
		manaPerLevel = 100;
		
		armor = 15;
		magicDefence = 25;
		
		healthRegen = 10;
		manaRegen = 15;
	}
}
