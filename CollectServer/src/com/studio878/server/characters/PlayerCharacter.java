package com.studio878.server.characters;

import java.util.HashMap;
import java.util.Map.Entry;

import com.studio878.server.abilities.Ability;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Player;

public abstract class PlayerCharacter {

	public static HashMap<Integer, PlayerCharacter> characterList = new HashMap<Integer, PlayerCharacter>();
	
	int id = 0;
	
	Player player;
	
	String fullName, shortName;
	
	int level = 1;

	
	int meleeDamage = 75, rangedDamage = 75;
	
	double moveSpeed = 3.4;
	
	int strength = 10, dexterity = 10, intelligence = 10;
	
	int health = 100, mana = 100;
	
	int healthPerLevel = 100, manaPerLevel = 50;
	
	int armor = 15, magicDefence = 15;
	
	int healthRegen = 5, manaRegen = 5;
	
	int lifeSteal = 0;
	
	
	Ability[] abilities = new Ability[4];
	int[] levels = new int[4];
	
	public PlayerCharacter(Player p) {
		this.player = p;
	}
	
	public int getId() {
		return id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getHealthPerLevel() {
		return healthPerLevel;
	}
	
	public int getManaPerLevel() {
		return manaPerLevel;
	}
	
	public int getMana() {
		return mana + manaPerLevel*(level-1) + (int) player.calculateBonus(BonusItemType.Mana);
	}
	
	public int getArmor() {
		return armor + (int) player.calculateBonus(BonusItemType.Armor);
	}
	
	public int getMagicDefence() {
		return magicDefence + (int) player.calculateBonus(BonusItemType.MagicDefence);
	}
	
	public int getHealthRegen() {
		return healthRegen + (int) player.calculateBonus(BonusItemType.HealthRegen);
	}
	
	public int getManaRegen() {
		return manaRegen + (int) player.calculateBonus(BonusItemType.ManaRegen);
	}
	
	public int getLifeSteal() {
		return lifeSteal + (int) player.calculateBonus(BonusItemType.LifeSteal);
	}
	
	public double getMeleeDamage() {
		return meleeDamage + (int) player.calculateBonus(BonusItemType.MeleeDamage);
	}
	
	public double getRangedDamage() {
		return rangedDamage + (int) player.calculateBonus(BonusItemType.RangedDamage);
	}
	
	public int getHealth() {
		return health + (int) player.calculateBonus(BonusItemType.Health);
	}
	
	public double getMoveSpeed() {
		return moveSpeed*player.calculateBonus(BonusItemType.Movement);
	}
	
	public int getStrength() {
		return strength + (int) player.calculateBonus(BonusItemType.Strength);
	}
	
	public int getDexterity() {
		return dexterity + (int) player.calculateBonus(BonusItemType.Dexterity);
	}
	
	public int getIntelligence() {
		return intelligence + (int) player.calculateBonus(BonusItemType.Intelligence);
	}
	
	public Ability getAbility(int position) {
		return abilities[position];
	}
	
	public int getAbilityLevel(int position) {
		return levels[position];
	}
	
	public void run() {
		
	}
		
	
	
}
