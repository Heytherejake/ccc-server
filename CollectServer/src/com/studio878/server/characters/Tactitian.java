package com.studio878.server.characters;

import com.studio878.server.entities.Player;

public class Tactitian extends PlayerCharacter {

	public Tactitian(Player p) {
		super(p);
		
		id = 1;
		
		fullName = "Talon, the Xeliarian Tactitian";
		shortName = "Tactitian";		
	//	abilities = new Ability[]{new Excavator1MetalDetector(), new Excavator2Exhume(), new Excavator3DynamiteExcavation(), new Excavator4StonesThrow()};
		rangedDamage = 90;
		meleeDamage = 20;
		
		strength = 20;
		dexterity = 10;
		intelligence = 10;
		
		health = 655;
		healthPerLevel = 175;
		
		mana = 350;
		manaPerLevel = 75;
		
		armor = 25;
		magicDefence = 10;
		
		healthRegen = 12;
		manaRegen = 10;
	}
}
