package com.studio878.server.io;

import java.net.Socket;

import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.packets.Packet11RemovePlayer;
import com.studio878.server.util.ChatColors;
import com.studio878.server.world.Grid;

public class NetPlayer {
	Socket socket;
	ClientListener clientListener;
	ClientSender clientSender;
	ServerDispatch serverDispatch;
	Player player;
	public boolean hasDisconnected = false;

	public void setPlayer(String name) {
		this.player = new Player(this, name, 3200, Grid.getSpawnLocation(3200), 64, 32);
	}
	public void setSocket(Socket s) {
		socket = s;
	}

	public void setListener(ClientListener cl) {
		clientListener = cl;
	}

	public void setSender(ClientSender cs) {
		clientSender = cs;
	}
	public void setDispatch(ServerDispatch ds) {
		serverDispatch = ds;
	}
	public Socket getSocket() {
		return socket;
	}
 
	public ClientSender getSender() {
		return clientSender;
	}

	public ClientListener getListener() {
		return clientListener;
	}

	public ServerDispatch getDispatch() {
		return serverDispatch;
	}

	public Player getPlayer() {
		return player;
	}

	public void disconnectPlayer() {
		if(getPlayer() != null) {
			getDispatch().broadcastPacket(new Packet08SendMessage(this, ChatColors.Yellow + getPlayer().getName() + " has disconnected."));
			getDispatch().broadcastPacket(new Packet11RemovePlayer(this, getPlayer().getName(), getPlayer().getId()));
		}
		getListener().interrupt();
		getSender().interrupt();
		getDispatch().removeClient(this);
		EntityManager.unregisterEntity(getPlayer());
	}
}
