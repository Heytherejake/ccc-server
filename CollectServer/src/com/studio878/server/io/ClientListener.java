package com.studio878.server.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.studio878.server.app.Main;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet;
import com.studio878.server.packets.PacketConverter;
import com.studio878.server.util.Settings;

public class ClientListener extends Thread {
	private ServerDispatch serverDispatch;
	private NetPlayer player;
	private BufferedReader in;
	Packet prev;

	public ClientListener(NetPlayer p, ServerDispatch c) throws IOException{
		player = p;
		serverDispatch = c;
		Socket s = p.getSocket();
		in = new BufferedReader(new InputStreamReader(s.getInputStream()));
	}

	public void run() {
		try {
			while(!isInterrupted()) {
					String message = in.readLine();
					if(message == null && player.getPlayer() == null) {
						break;
					}
					if(message.equals("requestinfo")) {
						int num = 0;
						for(Player p : Main.getServer().getPlayers()) {
							if(p != null) {
								num++;
							}
						}
						player.clientSender.sendMessage(System.currentTimeMillis() + ":" + Settings.Version + ":" + num + ":" + Settings.MaxClients);
						this.interrupt();
						continue;
					}
					Packet p = PacketConverter.createPacket(player, message);
					p.process();
			}
		} catch (Exception e) {
			if(Settings.DebugMode) {
				e.printStackTrace();
			}
		}
		player.getSender().interrupt();
		EntityManager.unregisterEntity(player.getPlayer());
		if(player.getPlayer() != null) {
			Console.write(player.getPlayer().getName() + " has disconnected.");
		}
		serverDispatch.removeClient(player);
		player.hasDisconnected = true;


	}


}
