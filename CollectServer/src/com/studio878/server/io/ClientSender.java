package com.studio878.server.io;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.studio878.server.entities.EntityManager;
import com.studio878.server.packets.Packet;
import com.studio878.server.util.Settings;

public class ClientSender extends Thread{
	private ArrayList<Packet> queue = new ArrayList<Packet>();
	private ServerDispatch serverDispatch;
	private NetPlayer player;
	private PrintWriter out;

	public ClientSender(NetPlayer p, ServerDispatch d) throws IOException {
		player = p;
		serverDispatch = d;
		Socket s = player.getSocket();
		out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
	}

	public synchronized void sendPacket(Packet m) {
		queue.add(m);
		notify();
	}

	private synchronized Packet getNextPacket() throws InterruptedException {
		while(queue.size() == 0) {
			wait();
		}
		Packet m = queue.get(0);
		queue.remove(0);
		return m;
	}

	private void sendPacketToClient(Packet m) {
		out.println(m.getDataString());
		out.flush();
	}

	public synchronized void sendMessage(String m) {
		out.println(m);
		out.flush();
	}
	
	public void run() {
		try {
			while(!isInterrupted()) {
				Packet m = getNextPacket();
				sendPacketToClient(m);
			}
		} catch (InterruptedException e) {
			
		} catch (Exception e) {
			if(Settings.DebugMode) {
				e.printStackTrace();
			}
		}
			EntityManager.unregisterEntity(player.getPlayer());
			player.getListener().interrupt();
			serverDispatch.removeClient(player);
			player.hasDisconnected = true;
		
	}

}
