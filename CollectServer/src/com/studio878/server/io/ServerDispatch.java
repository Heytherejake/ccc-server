package com.studio878.server.io;

import java.util.ArrayList;

import com.studio878.server.packets.Packet;


public class ServerDispatch extends Thread {
	private ArrayList<Packet> queue = new ArrayList<Packet>();
	private ArrayList<NetPlayer> clients = new ArrayList<NetPlayer>();

	public synchronized void addClient(NetPlayer p) {
		clients.add(p);
	}

	public synchronized void removeClient(NetPlayer p) {
		int index = clients.indexOf(p);
		if(index != -1) {
			clients.remove(index);
		}
	}

	public synchronized void sendPacketToAllPlayers(Packet packet) {
		queue.add(packet);
		notify();
	}

	public synchronized ArrayList<NetPlayer> getClients() {
		return clients;
	}

	private synchronized Packet getNextPacket() throws InterruptedException {
		while(queue.size() == 0) {
			wait();
		}
		Packet message = queue.get(0);
		queue.remove(0);
		return message;
	}

	public synchronized void broadcastPacket(Packet m) {
		if(clients.size() == 0) return;
		for(int i = 0; i < clients.size(); i++) {
			if(clients.get(i).getPlayer() != null) {
				clients.get(i).getSender().sendPacket(m);
			}
		}
	}

	public void run() {
		try {
			while (true) {
				Packet message = getNextPacket();
				message.getHandle().getSender().sendPacket(message);
				message.process();
			}
		} catch (InterruptedException e) {

		}
	}

}
