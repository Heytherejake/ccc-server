package com.studio878.server.entities;

import com.studio878.server.app.Main;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.events.EntityDamageEvent;
import com.studio878.server.events.EntityHealEvent;
import com.studio878.server.events.EntityKilledEvent;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.packets.Packet19ChangeHealth;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager.Team;


public class HealthEntity extends PhysicsAffectedEntity{
	
	long lastEnvironmentalDamage = 0;
	Team team = null;
	int health;
	
	public HealthEntity(double x, double y, int height, int width, EntityName type) {
		super(x, y, height, width, type);	
	}


	public void setTeam(Team t) {
		team = t;
	}

	public Team getTeam() {
		return team;
	}


	public void damage(int amount, DamageMethod source, String killer) {
		System.out.println("Damaging" + source.name() + "|" + amount);
		if(killer != null && this instanceof HealthEntity) {
			Player ks = Main.getServer().matchPlayer(killer);
			if((ks != null && ks.getTeam() != null && this.getTeam() != null) && ks.getTeam() == this.getTeam() && !Settings.FriendlyFire) {
				if(this instanceof Player) {
					Player p = (Player) this;
					if(!p.getName().equals(killer)) {
						return;
					}
				} else {
					return;
				}
			}
		}
		if(!Hooks.call(Hook.ENTITY_DAMAGED, new EntityDamageEvent(this, amount))) {
			return;
		}
		health -= amount;
		Main.getDispatch().broadcastPacket(new Packet19ChangeHealth(Main.getDispatch().getClients().get(0), this.id, health, source, killer));
		if(health <= 0) {
			kill();
			if(this instanceof Player) {
				Player p = (Player) this;
				p.addDeaths();
				Player ks = Main.getServer().matchPlayer(killer);
				if(ks != null && !ks.getName().equals(p.getName())) {
					ks.addKill();
					int amt = 350;
					ks.sendCriticalMessage("Received " + amt + " |item=Coins| and $50 for killing " + p.getName());
					ks.addMoney(amt);
				}
		


			}
	
		}
	}
	
	public int getHealth() {
		return health;
	}

	public void heal(int amount, DamageMethod source, String killer) {
		if(!Hooks.call(Hook.ENTITY_HEALED, new EntityHealEvent(this, amount))) {
			return;
		}
		health += amount;
		Main.getDispatch().broadcastPacket(new Packet19ChangeHealth(Main.getDispatch().getClients().get(0), this.id, health, source, killer));

	}

	public void kill() {
		if(!Hooks.call(Hook.ENTITY_KILLED, new EntityKilledEvent(this))) {
			return;
		}
		EntityManager.unregisterEntity(this.id);
	}


}
