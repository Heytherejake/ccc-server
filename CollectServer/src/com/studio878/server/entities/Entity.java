package com.studio878.server.entities;



import java.awt.Rectangle;
import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet55SendEntityState;
import com.studio878.server.world.Grid;

public class Entity{
	int id, height, width, MoveSpeed;
	double x = -99999, y = -99999;
	Block b;
	EntityName type;
	double rotation = 0;
	String data = "";
	boolean didChange = false;
	int state = 0;
	String name = "";

	public Entity(double x, double y, int height, int width, EntityName type) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.id = EntityManager.getHighestId();
		this.type = type;
		Console.log("Entity created: " + type + " (" + id + ")");
	}

	public int getId() {
		return id;
	}

	public void setState(int s) {
		this.state = s;
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet55SendEntityState(Main.getDispatch().getClients().get(0), this));
		}
	}

	public int getState() {
		return state;
	}

	public EntityName getType() {
		return type;
	}


	public String getData() {
		return data;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double s) {
		rotation = s;
	}
	public double getX() {
		return x;
	}

	public Rectangle getHitbox() {
		return new Rectangle((int) x, (int) y, width, height);
	}

	public ArrayList<Block> getCollidingBlocks() {
		ArrayList<Block> result = new ArrayList<Block>();
		int dx = (int) (x - (x%16));
		int dy = (int) (y - (y%16));

		for(int j = dy; j < dy + height; j+= 16) {
			for(int i = dx; i < dx + width; i+= 16) {
				Block bs = Grid.getClosestBlock(i, j);
				if(bs != null) {
					result.add(bs);
				}
			}
		}
		return result;
	}

	public double getY() {
		return y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setData(String d) {
		data = d;
	}

	public ArrayList<Entity> getEntitiesWithinRange(double range) {
		ArrayList<Entity> results = new ArrayList<Entity>();
		for(Entity e : EntityManager.getEntities()) {
			if(Math.sqrt(Math.pow(e.x - this.x, 2) + Math.pow(e.y - this.y, 2)) < range) {
				results.add(e);
			}
		}
		return results;
	}
}
