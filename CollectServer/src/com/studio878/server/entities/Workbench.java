package com.studio878.server.entities;

import java.awt.Graphics;

import com.studio878.server.entities.EntityManager.EntityName;

public class Workbench extends PhysicsAffectedEntity {

	public Workbench(double x, double y) {
		super(x, y, 64, 64, EntityName.Workbench);
		movable = false;
	}

}
