package com.studio878.server.entities;

import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet13SendMovement;
import com.studio878.server.packets.Packet69SendVelocity;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;



public class PhysicsAffectedEntity extends Entity{
	double yspeed = 0;
	double xspeed = 0;
	Block b;
	int facingDirection;
	static final int TerminalVelocity = 16;
	double prevx = 0;
	double prevy = 0;
	boolean doesBounce = false;
	boolean doesFall = true;
	boolean movable = true;
	boolean sticky = false;
	boolean isFluid = false;


	public PhysicsAffectedEntity(double x, double y, int height, int width, EntityName type) {
		super(x, y, height, width, type);

	}

	public void setFacingDirection(int d) {
		facingDirection = d;
	}

	public int getFacingDirection() {
		return facingDirection;
	}


	public void setVelocity(double x, double y) {
		xspeed = x;
		yspeed = y;
		if(Main.getDispatch().getClients().size() > 0) {
			Main.getDispatch().broadcastPacket(new Packet69SendVelocity(Main.getDispatch().getClients().get(0), this.id, this.xspeed, this.yspeed));
		}
	}
	public double getXSpeed() {
		return xspeed;
	}
	public double getYSpeed() {
		return yspeed;
	}

	public void setXSpeed(double x) {
		if(movable) {
			xspeed = x;
		}
	}

	public void setYSpeed(double y) {
		if(movable) {
			yspeed = y;
		}
	}
	/*public boolean isLocationValid(double x, double y, int dir) throws NullPointerException {
		double dx, dy;
		if(dir == 1) {
			dx = ((x + xspeed) - ((x+xspeed)%16)) + 16;
			dy = y - (y%16);
		} else {
			dx = ((x + xspeed) - ((x + xspeed)%16)) + 1;
			dy = y - (y%16); 
		}
		Rectangle rs = new Rectangle((int) x, (int) y, width, height);
		int ct = 0;
		int std = (int) dy;
		for(int i = std; i < std + height; i += 16) {
			if(!Grid.getClosestBlock((int) dx, i).getType().getPermiability(this)) {
				Rectangle rg = new Rectangle((int) dx, (int) i, 16, 16);
				if(rg.intersects(rs)) {
					return false;
				}
			}
			ct++;
		}
		return true;
	}*/

	public boolean isLocationValid(double x, double y, int dir) throws NullPointerException {
		double dx, dy;
		if(dir == 1) {
			dx = ((x + xspeed) - ((x+xspeed)%16)) + 32;
			dy = y - (y%16);
		} else {
			dx = ((x + xspeed) - ((x + xspeed)%16));
			dy = y - (y%16); 
		}
		Block brd = Grid.getClosestBlock((int)dx,(int) dy + height);
		if(brd == null) {
			if(y + height <= Grid.HighestLeftPoint) {
				return true;
			}
			return false;
		}

		if(x >= Settings.WorldWidth*16 - width - 10) {
			if(y + height <= Grid.HighestRightPoint) {
				return true;
			}
			return false;
		}

		b =  brd.getBlockAdjacent(0, -1);

		for(int i = 0; i < ((int) (Math.ceil(height / 16))); i++) {
			if(b != null) {
				if(!b.getType().getPermiability(this)) {

					return false;
				}
			}
			b = b.getBlockAdjacent(0, -1);
		}
		return true;
	}

	public void moveTo(double xs, double ys) {
		this.x = xs;
		this.y = ys;
		if(Main.getDispatch().getClients().size() > 0) {
			Main.getDispatch().broadcastPacket(new Packet13SendMovement(Main.getDispatch().getClients().get(0), x, y, id, facingDirection, rotation));
		}
	}
	/*public void step() {
		try {
			boolean doMove = true;
			int dx = (int) (x - (x%16)) + 1;
			int dy = (int) (y - (y%16));
			//		Rectangle rs = new Rectangle((int) x, (int) y, width, height);
			if(yspeed > 0) {
				for(int i = dx; i < dx + width; i += 16) {
					if(doMove) {
						Block bs = Grid.getClosestBlock(i, dy + height);
						if(bs != null && !bs.getType().getPermiability(this)) {
							doMove = false;
						}
					}
				}
			}


			if(doMove) {
				if(doesFall) {
					yspeed += Settings.Gravity;
				}
			} else {
				if(this instanceof Player) {
					((Player) this).hasJumped = false;

				}
				y -= (y - dy);
				if(this instanceof HealthEntity) {
					if(yspeed > 10) {
						HealthEntity h = (HealthEntity) this;
						h.damage((int) (yspeed - 5), DamageMethod.FallDamage, h.getData());
					}

				}
				if(doesBounce && yspeed > 1) {
					yspeed = (-0.3*yspeed);


				} else {
					yspeed = 0;
				}


			}
			//maybe
			b = Grid.getClosestBlock(dx, dy);
			if(yspeed < 0.0 && b != null && (!b.getType().getPermiability(this) || !b.getBlockAdjacent(1, 0).getType().getPermiability(this))) {
				y -= 2*yspeed;
				yspeed = 0;
			} else {
				if(yspeed > TerminalVelocity) {
					y += TerminalVelocity;
				} else {
					y += yspeed;
				}
			}

			if(xspeed != 0) {
				if(isLocationValid(x, y, (int) (Math.abs(xspeed)/xspeed))) {
					x += xspeed;
					if(yspeed == 0) {
						if(!sticky && Math.abs(xspeed) >= Settings.Friction) {
							if(xspeed < 0) {
								xspeed += Settings.Friction;
							} else {
								xspeed -= Settings.Friction;
							}

						} else {
							xspeed = 0;
						}
					}
				} else {
					xspeed = 0;
				}
			}
			if(prevx != x || prevy != y) {
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet13SendMovement(Main.getDispatch().getClients().get(0), x, y, id, facingDirection, rotation));
				}
			}
			prevx = x;
			prevy = y;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public void step() {
		try {
			calculateDamage();
			boolean doMove = true;
			boolean offMap = false;
			int dx = (int) (x - (x%16));
			int dy = (int) (y - (y%16));
			if(facingDirection == 0 || facingDirection == 1) {
				dx += 16;
			}

			float dv = 1.0f;
			boolean fluid = false;
			ArrayList<Block> cs = getCollidingBlocks();

			for(int i = 0; i < cs.size(); i++) {
				if(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid()) {
					dv = 1.0f;
					fluid = true;
					isFluid = true;
					i = cs.size();
				} else {
					if(cs.get(i).getType().isFluid()) {
						fluid = true;
						isFluid = true;
						if(this instanceof Player) {
							((Player) this).hasJumped = false;
						}
						String ns = b.getType().getName();
						if(ns.equals("Lava")) {
							dv = 0.02f;
						} else if(ns.equals("Water")) {

							dv = 0.02f;
							break;
						} else {
							dv = 0.05f;
						}
					}
				}
			}
			if(isFluid && dv == 1.0f) {
				isFluid = false;
				xspeed = 0;
			}

			b =  Grid.getClosestBlock(dx, dy + height);
			if(b != null && x <= Settings.WorldWidth*16 - width) {

				Block ba = b.getBlockAdjacent(1, 0);
				if(ba == null) {
					this.x += -2*xspeed;
					xspeed = 0;
					return;
				}
				if(b != null && ba != null &&  (!b.getType().getPermiability(this) || !ba.getType().getPermiability(this)) && yspeed > 0) {
					doMove = false;

				}
			}

			if((x < 0 && y + height >= Grid.HighestLeftPoint) || (x > Settings.WorldWidth*16 - width && y + height >= Grid.HighestRightPoint)) {
				doMove = false;
				offMap = true;
			}

			if(doMove) {
				if(doesFall) {
					yspeed += Settings.Gravity;
				}
			} else {
				if(this instanceof Player) {
					((Player) this).hasJumped = false;

				}
				if(!offMap) {
					y -= (y - dy);
				}

				if(doesBounce && yspeed > 1) {
					yspeed = (-0.3*yspeed);


				} else {
					yspeed = 0;
				}


			}
			b = Grid.getClosestBlock(dx, dy);


			if(yspeed < 0.0 && b != null && (!b.getType().getPermiability(this) || !b.getBlockAdjacent(1, 0).getType().getPermiability(this))) {
				y -= 2*yspeed;
				yspeed = 0;
			} else {
				if(yspeed > TerminalVelocity) {
					y += TerminalVelocity*dv;
				} else {
					y += yspeed*dv;
				}
			}

			if(xspeed != 0) {
				if(isLocationValid(x, y, (int) (Math.abs(xspeed)/xspeed))) {
					x += xspeed*dv;
					if(yspeed == 0) {
						if(!sticky && Math.abs(xspeed) >= Settings.Friction) {
							if(xspeed < 0) {
								xspeed += Settings.Friction;
							} else {
								xspeed -= Settings.Friction;
							}

						} else {
							xspeed = 0;
						}
					}
				} else {
					xspeed = 0;
				}
			}
			if(prevx != x || prevy != y) {
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet13SendMovement(Main.getDispatch().getClients().get(0), x, y, id, facingDirection, rotation));
				}
			}
			prevx = x;
			prevy = y;

		} catch (Exception e) {

		}
	}

	public void calculateDamage() {
		boolean doMove = true;
		float dv = 1.0f;
		boolean fluid = false;

		int dx = (int) (x - (x%16));
		int dy = (int) (y - (y%16));
		if(facingDirection == 0 || facingDirection == 1) {
			dx += 16;
		}
		Block b =  Grid.getClosestBlock(dx, dy + height);
		if(b != null && x <= Settings.WorldWidth*16 - width) {

			Block ba = b.getBlockAdjacent(1, 0);
			if(b != null && ba != null &&  (!b.getType().getPermiability(this) || !ba.getType().getPermiability(this)) && yspeed > 0) {
				doMove = false;

			}
		}

		if((x < 0 && y + height >= Grid.HighestLeftPoint) || (x > Settings.WorldWidth*16 - width && y + height >= Grid.HighestRightPoint)) {
			doMove = false;
		}
		
		boolean inFluid = false;
		
		ArrayList<Block> cs = getCollidingBlocks();

		for(int i = 0; i < cs.size(); i++) {
			if(!(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid())) {
				if(cs.get(i).getType().isFluid()) {
					inFluid = true;
					String ns = b.getType().getName();

					if(ns.equals("Lava")) {
						if(this instanceof HealthEntity) {
							HealthEntity e = (HealthEntity) this;
							if(System.currentTimeMillis() - e.lastEnvironmentalDamage >= 500) {
								String source = null;
								if(e instanceof Player) {
									source = ((Player) e).getName();
								}
								e.damage(5, DamageMethod.Lava, source);
								e.lastEnvironmentalDamage = System.currentTimeMillis();
							}
						}
					}
				}
			}
		}

		if(!doMove && !inFluid) {
			if(this instanceof HealthEntity) {
				if(yspeed*dv > 10) {
					HealthEntity h = (HealthEntity) this;
					h.damage((int) (yspeed*dv), DamageMethod.FallDamage, h.getData());
				}

			}
		}
		


		if(this instanceof Player) {
			if(this.getX() < -500 || this.getX() > Settings.WorldWidth*16 + 500) {
				Player p = (Player) this;
				if(p.getHealth() > 0) {
					p.damage(9999, DamageMethod.OutOfBounds, p.getName());
				}
			}
		}
	}
}
