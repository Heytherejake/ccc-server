package com.studio878.server.entities;

import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class Rocket extends PhysicsAffectedEntity {

	private boolean hasExploded = false;
	public Rocket(double d, double e, double angle, String thrown) {
		super(d, e, 32, 32, EntityName.Rocket);
		this.rotation = angle;
		this.data = thrown;
		this.doesFall = false;
	}

	public void step() {
		if(!hasExploded) {
			super.step();
			boolean shouldExplode = false;
			for(Entity e: EntityManager.getEntities()) {
				if(e instanceof HealthEntity) {
					if(this.getX() >= e.getX() && this.getX() <= e.getX() + e.getWidth() && this.getY() >= e.getY() && this.getY() <= e.getY() + e.getHeight()) {
						shouldExplode = true;
						HealthEntity h = (HealthEntity) e;
						Player p = Main.getServer().matchPlayer(data);
						if(p != null) {
							if(p.getTeam() == h.getTeam()) {
								shouldExplode = false;
							}
						}
					}
				}
			}
			if(this.xspeed == 0 || this.yspeed == 0 || shouldExplode) {
				hasExploded = true;
				Block center = Grid.getClosestBlock((int) x, (int) y);
				ArrayList<Block> blocks = Grid.getCircle(center.getX(), center.getY(), 4);
				for(Block b: blocks) {
					b.setDropOnBreak(false);
					b.hit(Main.getServer().matchPlayer(data), 750, (byte) 1);
				}
				for(Entity e: EntityManager.getEntities()) {
					if(e instanceof HealthEntity) {
						double distance = Math.sqrt(Math.pow(this.x - e.getX(), 2) + Math.pow(this.y - e.getY(), 2));
						if(Math.abs(this.x - e.getX()) < 16*4 && Math.abs(this.y - e.getY()) < 16*4) {
							HealthEntity h = (HealthEntity) e;
							double angle = Math.atan2(h.x - this.x, this.y - h.y);
							if(h.x < this.x) {
								angle -= 90;
							} else {
								angle -= 45;
							}
							h.setVelocity(7*Math.cos(angle), 15*Math.sin(angle));
							h.damage((int) (50 * ((Math.sqrt(70*70*2) - distance)/Math.sqrt(70*70*2))), DamageMethod.Rocket, data);		


						}
					}
			}
				EntityManager.unregisterEntity(id);
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
				}
			}
			if(this.x <= 0 || this.y <= 0 || this.x >= Settings.WorldWidth*16 || this.y >= Settings.WorldHeight*16) {
				EntityManager.unregisterEntity(this.getId());
				Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.getId()));
			}
		}
	}

}
