package com.studio878.server.entities;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet13SendMovement;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class FallingBlock extends PhysicsAffectedEntity {

	BlockType block;

	public FallingBlock(double x, double y, BlockType block) {
		super(x, y, 16, 16, EntityName.FallingBlock);
		this.block = block;
		this.data = ""+block.getId();
		this.yspeed = 0.1;
	}

	public void setXSpeed(double x) {
		xspeed = 0;
	}

	public void step() {
		try {
			boolean doMove = true;
			int dx = (int) (x - (x%16));
			int dy = (int) (y - (y%16));
			b =  Grid.getClosestBlock(dx, dy + height);
			if(b != null && !b.getType().getPermiability(this) && yspeed > 0) {
				doMove = false;

			}

			if(doMove) {
					yspeed += Settings.Gravity;
			} else {

				y -= (y - dy);

				yspeed = 0;


			}
			b = Grid.getClosestBlock(dx, dy);
			if(yspeed < 0.0 && b != null && (!b.getType().getPermiability(this) || !b.getBlockAdjacent(1, 0).getType().getPermiability(this))) {
				y -= 2*yspeed;
				yspeed = 0;
			} else {
				if(yspeed > TerminalVelocity) {
					y += TerminalVelocity;
				} else {
					y += yspeed;
				}
			}


			if(prevx != x || prevy != y) {
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet13SendMovement(Main.getDispatch().getClients().get(0), x, y, id, facingDirection, rotation));
				}
			}
			prevx = x;
			prevy = y;

		} catch (Exception e) {

		}
		if(this.yspeed == 0 ) {
			Block b = Grid.getClosestBlock((int) this.x, (int) this.y);
			if(b != null) {
				b.setType(block);
				b.setDropOnBreak(true);
				EntityManager.unregisterEntity(id);
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
				}
			}
		}
	}
}
