package com.studio878.server.entities;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.achievement.AchievementManager.Achievement;
import com.studio878.server.app.Main;
import com.studio878.server.app.Server;
import com.studio878.server.block.Block;
import com.studio878.server.characters.Excavator;
import com.studio878.server.characters.PlayerCharacter;
import com.studio878.server.crafting.BonusItem;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.events.PlayerChangeTeamEvent;
import com.studio878.server.events.PlayerKickedEvent;
import com.studio878.server.events.PlayerKilledEvent;
import com.studio878.server.inventory.Inventory;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.logging.Console;
import com.studio878.server.multiplayer.MultiplayerAbility;
import com.studio878.server.multiplayer.MultiplayerSettingsHandler;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.packets.Packet15AddEntity;
import com.studio878.server.packets.Packet16KickPlayer;
import com.studio878.server.packets.Packet19ChangeHealth;
import com.studio878.server.packets.Packet21SendInventory;
import com.studio878.server.packets.Packet28SendWeaponChange;
import com.studio878.server.packets.Packet32PlayerDeath;
import com.studio878.server.packets.Packet37SendPlayerInfo;
import com.studio878.server.packets.Packet49SendReloadTime;
import com.studio878.server.packets.Packet50SendPlayerScore;
import com.studio878.server.packets.Packet56SendCriticalMessage;
import com.studio878.server.packets.Packet58SendBalance;
import com.studio878.server.packets.Packet66ForceLocationUpdate;
import com.studio878.server.remoteui.RemoteUIContainer;
import com.studio878.server.story.MapEvent;
import com.studio878.server.story.MapEvent.MapEventType;
import com.studio878.server.story.MapEventChain;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager;
import com.studio878.server.util.TeamManager.Team;
import com.studio878.server.weapons.EmptyHand;
import com.studio878.server.weapons.Weapon;
import com.studio878.server.world.Grid;

public class Player extends HealthEntity{


	PlayerCharacter character;

	String name;
	String session;
	String characterName;

	NetPlayer handle;
	boolean hasJumped = false;
	Inventory inventory = new Inventory(this);
	Weapon equipped = null;
	double gunRotation;
	long lastFireTime = 0;
	long latency = 0;
	long lastRotationSetTime = 0;
	long lastJumpTime = 0;

	int kills = 0;
	int deaths = 0;

	int balance = 0;

	boolean placementMode = false;

	MapEventChain activeChain = null;
	MapEventChain previousChain = null;

	HashMap<Achievement, Integer> achievements = new HashMap<Achievement, Integer>();

	CopyOnWriteArrayList<CraftingRecipe> bonuses = new CopyOnWriteArrayList<CraftingRecipe>();
	CopyOnWriteArrayList<CraftingRecipe> crafted = new CopyOnWriteArrayList<CraftingRecipe>();

	CopyOnWriteArrayList<RemoteUIContainer> containers = new CopyOnWriteArrayList<RemoteUIContainer>();

	MultiplayerSettingsHandler multiplayerSettings;

	public CopyOnWriteArrayList<MultiplayerAbility> multiplayerAbilities = new CopyOnWriteArrayList<MultiplayerAbility>();

	public CopyOnWriteArrayList<Item> Loadout = new CopyOnWriteArrayList<Item>();

	public Player(NetPlayer p, String name, int x, int y, int height, int width) {
		super(x, y, height, width, EntityName.Player);
		this.data = name;
		this.name = name;
		handle = p;
		MoveSpeed = 2;
		this.health = 100;
		this.team = Team.Red;
		if(Settings.StoryMode) {
			character = new Excavator(this);
			Loadout.add(new Item(ItemID.lookupName("Pistol"), 1, 0, this));
		}

		multiplayerSettings = new MultiplayerSettingsHandler(this);
		respawn();

	}


	public void setTeam(Team t) {
		Hooks.call(Hook.PLAYER_CHANGED_TEAM, new PlayerChangeTeamEvent(this, t));

		team = t;
		moveTo(TeamManager.getTeamRespawnPoint(t));
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet37SendPlayerInfo(Main.getDispatch().getClients().get(0)));
		}
	}

	public long getLatency() {
		return latency;
	}

	public void addMoney(int amount) {
		balance += amount;
	}

	public void subtractMoney(int amount) {
		balance -= amount;
	}

	public boolean hasEnoughMoney(int amount) {
		return (amount <= balance);
	}

	public PlayerCharacter getCharacter() {
		return character;
	}

	public void setCharacter(PlayerCharacter c) {
		this.character = c;
	}

	public int getBalance() {
		return balance;
	}

	public MultiplayerSettingsHandler getMultiplayerSettingsHandler() {
		return multiplayerSettings;
	}

	public void setBalance(int amount) {
		balance = amount;
	}

	public void setLatency(long l) {
		latency = l;
	}

	public Team getTeam() {
		return team;
	}

	public void setLastFireTime(long s) {
		lastFireTime = s;
	}

	public void addBonus(CraftingRecipe r) {
		bonuses.add(r);
	}

	public MapEventChain getActiveChain() {
		return activeChain;
	}

	public long getLastRotationSetTime() {
		return lastRotationSetTime;
	}

	public void setLastRotationTime(long time) {
		this.lastRotationSetTime = time;
	}

	public void setSessionID(String session) {
		this.session = session;
	}

	public String getSessionID() {
		return session;
	}

	public void setActiveCharacterName(String name) {
		this.characterName = name;
	}

	public String getActiveCharacterName() {
		return characterName;
	}

	public void processNextMapEvent() {
		if(activeChain != null) {
			activeChain.getEvents().removeAll(Collections.singleton(null));  
		}
		if(activeChain != null && activeChain.getEvents().size() == 0) {
			activeChain = null;
		}

		if(activeChain != null && activeChain.getEvents().size() != 0) {
			MapEvent e = activeChain.getEvents().get(0);
			activeChain.getEvents().remove(e);
			if(e.getType().isConditional() && e.getType() != MapEventType.Else) {
				if(e.process(this)) {
					boolean hasElse = false;
					int si = 0;
					for(int i = 0; i < activeChain.getEvents().size(); i++) {
						if(activeChain.getEvents().get(i).getType() == MapEventType.Else) {
							hasElse = true;
							si = i;
						}
					}
					if(hasElse) {
						si++;
						while(si < activeChain.getEvents().size() && activeChain.getEvents().get(si).getLevel() == e.getLevel() + 1) {
							activeChain.getEvents().remove(si);
							si++;
						}
					}
					processNextMapEvent();

				} else {
					boolean hasElse = false;
					for(int i = 0; i < activeChain.getEvents().size(); i++) {
						if(activeChain.getEvents().get(i).getType() == MapEventType.Else) {
							hasElse = true;
						}
					}
					if(hasElse) {
						while(activeChain.getEvents().size() != 0 && activeChain.getEvents().get(0).getType() != MapEventType.Else) {
							activeChain.getEvents().remove(0);
						}
					}
					if(activeChain.getEvents().size() == 0) {
						activeChain = null;
					}
					if(activeChain.getEvents().size() == 0) {
						activeChain = null;
					}
					if(activeChain != null && activeChain.getEvents().size() != 0) {
						processNextMapEvent();
					}
				}
			} else {
				e.process(this);
			}
		}

	}

	public void setActiveChain(MapEventChain c) {
		if(activeChain == null) {
			activeChain = c.clone();
			processNextMapEvent();
		}
	}

	public boolean getPlacementMode() {
		return placementMode;
	}

	public void setPlacementMode(boolean mode) {
		this.placementMode = mode;
	}

	public CopyOnWriteArrayList<CraftingRecipe> getBonuses() {
		return bonuses;
	}

	public long getLastFireTime() {
		return lastFireTime;
	}

	public void receiveAchievementValue(Achievement a, int amount) {
		if(achievements.get(a) != null) {
			achievements.remove(a);
		}
		achievements.put(a, amount);

	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public void addKill() {
		kills++;
		getHandle().getSender().sendPacket(new Packet50SendPlayerScore(getHandle(), this));
	}

	public void addDeaths() {
		deaths++;
		getHandle().getSender().sendPacket(new Packet50SendPlayerScore(getHandle(), this));

	}

	public void addToAchievementValue(Achievement a, int amount) {
		int prm = 0;
		if(achievements.get(a) != null) {
			prm = achievements.get(a);
			achievements.remove(a);
		}
		achievements.put(a, amount + prm);
	}

	private void updateAchievementValue(Achievement a) {
		//update thing
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void sendInventoryUpdate() {
		if(this.getHandle().getPlayer() != null) {
			this.getHandle().getSender().sendPacket(new Packet21SendInventory(this.getHandle()));
			this.getHandle().getSender().sendPacket(new Packet58SendBalance(this.getHandle()));
		}

	}

	public void setWeapon(Weapon w, int slot) {
		equipped = w;
		int id = 0;
		if(w.getItem() == null) {
			id = -1;
		} else {
			id = w.getItem().getType().getId();
		}
		getHandle().getDispatch().broadcastPacket(new Packet28SendWeaponChange(getHandle(), id, slot));

	}

	public void removeCrafted(CraftingRecipe r) {
		for(CraftingRecipe rs: crafted) {
			if(rs.getId() == r.getId()) {
				crafted.remove(rs);
			}
		}
	}
	public boolean hasCrafted(CraftingRecipe r) {
		for(CraftingRecipe rs: crafted) {
			if(rs.getId() == r.getId()) {
				return true;
			}
		}
		return false;
	}
	public NetPlayer getHandle() {
		return handle;
	}

	public String getName() {
		return name;
	}

	public Weapon getWeapon() {
		return equipped;
	}

	public double getArmRotation() {
		return gunRotation;
	}

	public void sendCriticalMessage(String message) {
		getHandle().getSender().sendPacket(new Packet56SendCriticalMessage(getHandle(), message));
	}

	public void setArmRotation(double angle) {
		gunRotation = angle;
	}

	public RemoteUIContainer getContainer(int container) {
		for(RemoteUIContainer cs : containers) {
			if(container == cs.getId()) {
				return cs;
			}
		}
		return null;
	}

	public void removeContainer(RemoteUIContainer c) {
		containers.remove(c);
	}

	public void addContainer(RemoteUIContainer c) {
		RemoteUIContainer cs = getContainer(c.getId());
		if(cs != null) {
			containers.remove(cs);
		}
		containers.add(c);
	}



	public void jump() {
		boolean fluid = false;
		boolean exit = false;
		ArrayList<Block> cs = getCollidingBlocks();

		for(int i = 0; i < cs.size(); i++) {
			if(fluid == false && i > cs.size() - 3 && cs.get(i).getType().isFluid()) {
				exit = true;
				fluid = true;
				i = cs.size();
			} else {
				if(cs.get(i).getType().isFluid()) {
					fluid = true;
				}
			}
		}
		if(!hasJumped && (yspeed == 0 || fluid) && System.currentTimeMillis() - lastJumpTime >= 50) {
			y -= 3;
			yspeed = fluid ? -0.1*calculateBonus(BonusItemType.JumpHeight):-5*calculateBonus(BonusItemType.JumpHeight);
			yspeed = exit ? yspeed : -5*calculateBonus(BonusItemType.JumpHeight);
			hasJumped = true;
			lastJumpTime = System.currentTimeMillis();
		}

	}

	public void moveTo(Point location) {
		this.x = location.getX();
		this.y = location.getY();   
		getHandle().getSender().sendPacket(new Packet66ForceLocationUpdate(getHandle(), this.x, this.y));
	}

	public void kick(String reason) {
		Hooks.call(Hook.PLAYER_KICKED, new PlayerKickedEvent(this));
		getHandle().getSender().sendPacket(new Packet16KickPlayer(getHandle(), reason));
		getHandle().disconnectPlayer();

	}
	public boolean isOp() {
		for(String s : Server.ops) {
			if(s.equalsIgnoreCase(this.getName())) {
				return true;
			}
		}
		return false;
	}

	public void kill() {
		if(character == null) return;
		
		if(!Hooks.call(Hook.PLAYER_KILLED, new PlayerKilledEvent(this))) {
			return;
		}
		if(getWeapon() != null && getWeapon().getItem() != null) {
			try {
				ItemEntity e = new ItemEntity(x, y, getWeapon().getItem());
				e.setYSpeed(yspeed);
				e.setXSpeed(xspeed);
				EntityManager.registerEntity(e);
				e.setData(getWeapon().getItem().getType().getId() + "," + getWeapon().getItem().getLevel() + ";" + getWeapon().getItem().getData());
				Main.getDispatch().broadcastPacket(new Packet15AddEntity(getHandle(), e));
			} catch (Exception e) {

			}
		}
		this.setVelocity(0, 0);
		if(!Settings.StoryMode) {
			inventory.clear();
		}
		getHandle().getSender().sendPacket(new Packet21SendInventory(getHandle()));
		getHandle().getSender().sendPacket(new Packet32PlayerDeath(getHandle()));
		moveTo(new Point(-99999, -99999));

		Main.registerTimerTask(new TimerTask() {
			public void run() {
				respawn();
			}	
		}, Settings.RespawnTime*1000);

		getHandle().getSender().sendPacket(new Packet49SendReloadTime(getHandle(), Settings.RespawnTime));
		new Thread() {
			public void run() {
				Player.this.multiplayerSettings.updateMultiplayerVariables();
			}
		}.start();
	}

	public void respawn() {
		if(character != null) {
			if(!Settings.StoryMode) {
				inventory.clear();
				inventory.add(ItemID.lookupName("Grenade"), 0, 5);
				inventory.add(ItemID.lookupName("Shovel"), 0, 1);
				for(CraftingRecipe i : crafted) {
					inventory.add(i.getResult().getType(), 0, 1);
				}
				for(Item i: Loadout) {
					inventory.add(i.getType(), i.getLevel(), i.getAmount());
				}
				if(Loadout.size() == 0) {
					inventory.add(ItemID.lookupName("Pistol"), 0, 1);
				}
			}

			moveTo(TeamManager.getTeamRespawnPoint(team));
			getHandle().getSender().sendPacket(new Packet21SendInventory(getHandle()));
			this.health = character.getHealth();
			getHandle().getSender().sendPacket(new Packet19ChangeHealth(getHandle(), id, health, DamageMethod.Generic, name));

			for(Item i: inventory.getItems()) {
				if(i.getType().getWeaponType() == 1) {
					this.setWeapon(Weapon.matchWeapon(this, i), i.getSlot());
				}
			}
			Console.log("Respawning " + name);
		} else {
			Console.log(name + " is in character select, and cannot be respawned.");
		}

	}
	public void sendMessage(String message) {
		handle.getSender().sendPacket(new Packet08SendMessage(handle, message));
	}

	public boolean isDead() {
		return health == 0;
	}

	public void addCrafted(CraftingRecipe r) {
		crafted.add(r);
	}

	public void step() {
		if(!Settings.ClientSideMovement) {
			super.step();
		} else {
			calculateDamage();
		}
		if(Settings.StoryMode) {
			boolean doesHit = false;
			for(MapEventChain m : Grid.getMapEvents()) {
				if(m.doesIntersect(this)) {
					doesHit = true;
					if(previousChain == null) {
						m.onIntersection(this);
						previousChain = m;
					}
				}
			}
			if(!doesHit) {
				previousChain = null;
			}

		}
		if(!isDead()) {
			if(placementMode == false) {
				if(inventory.countWeapons() == 0 && !(this.equipped instanceof EmptyHand)) {
					this.setWeapon(new EmptyHand(this), -1);
				} else if(this.equipped instanceof EmptyHand || this.equipped == null) {
					for(Item i: inventory.getItems()) {
						if(i.getType().getWeaponType() == 1) {
							this.setWeapon(Weapon.matchWeapon(this, i), i.getSlot());
						}
					}
				}
			} else {
				if(inventory.countBlocks() == 0 && !(this.equipped instanceof EmptyHand)) {
					this.setWeapon(new EmptyHand(this), -1);
				} else if(this.equipped instanceof EmptyHand || this.equipped == null) {
					for(Item i: inventory.getItems()) {
						if(i.getType().isBlock()) {
							this.setWeapon(Weapon.matchWeapon(this, i), i.getSlot());
						}
					}
				}
			}
		}
		if(character == null) {
			moveTo(new Point(-99999, -99999));
		}
	}

	public double calculateBonus(BonusItemType t) {
		double amt = 1.0;

		for(CraftingRecipe c : getBonuses()) {
			for(BonusItem i: c.getBonuses()) {
				if(i.getType() == t) {
					amt += i.getAmount();
				}
			}
		}
		for(MultiplayerAbility i: multiplayerAbilities) {
			if(i.getType() == t) {
				amt += i.getMultiplier();
			}
		}
		return amt;
	}
}
