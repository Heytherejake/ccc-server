package com.studio878.server.entities;

import java.util.TimerTask;

import com.studio878.server.app.Main;
import com.studio878.server.app.Server;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet51RoundOver;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager.Team;

public class Generator extends HealthEntity{
	NetPlayer handle;
	double gunRotation;

	public Generator(int x, int y, Team team) {
		super(x, y, 101, 227, EntityName.Generator);
		this.health = 2000;
		this.data = ""+health;
		this.team = team;

	}

	public void kill() {
		Team winningTeam = null;
		switch(team) {
		case Red:
			winningTeam = Team.Blue;
			break;
		case Blue:
			winningTeam = Team.Red;
			break;
		}
		Console.log("Round over, " + winningTeam + " wins!");
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet51RoundOver(Main.getDispatch().getClients().get(0), winningTeam, Settings.NewRoundTime));
		}
		Main.registerTimerTask(new TimerTask() {
			public void run() {
				Server.startNewRound();
			}
		}, Settings.NewRoundTime*1000);
	}
	public Team getTeam() {
		return team;
	}
	
	public void step() {
	}


}
