package com.studio878.server.entities;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.MathUtil;
import com.studio878.server.world.Grid;

public class Grenade extends PhysicsAffectedEntity {

	private boolean hasExploded = false;
	private long throwTime;
	public Grenade(double x, double y, String thrown) {
		super(x, y, 27, 14, EntityName.Grenade);
		this.data = thrown;
		this.doesBounce = true;
		throwTime = System.currentTimeMillis();
	}

	public void step() {
		if(!hasExploded) {
			super.step();
			if(yspeed > 0.5) {
				rotation += (int) (Math.abs(xspeed)/xspeed);
			}
			if(System.currentTimeMillis() - throwTime > 3000) {
				hasExploded = true;
				Block center = Grid.getClosestBlock((int) x, (int) y);
				if(center != null) {
					ArrayList<Block> blocks = Grid.getCircle(center.getX(), center.getY(), 5);
					for(Block b: blocks) {
							b.setDropOnBreak(false);
							b.hit(null, 1000, (byte) 1);
					}
				}
				for(Entity e: EntityManager.getEntities()) {
					if(e instanceof HealthEntity) {
						Rectangle r = new Rectangle((int) e.getX(), (int) e.getY(), e.getWidth(), e.getHeight());
						double md = 999;
						Point2D loc = new Point2D.Double(this.getX(), this.getY());
						for(Line2D l : MathUtil.getLinesFromRectangle(r)) {
							double disc = l.ptSegDist(loc);

							if(disc < md) {
								md = disc;
							}
						}
						if(md <= 16*5) {
							HealthEntity h = (HealthEntity) e;
							double angle = Math.atan2(h.x - this.x, this.y - h.y);
							if(h.x < this.x) {
								angle -= 90;
							} else {
								angle -= 45;
							}
							h.setVelocity(7*Math.cos(angle), 15*Math.sin(angle));
							h.damage((int) (50 * ((80 - md)/80)), DamageMethod.Grenade, data);		


						}
					}
				}
				EntityManager.unregisterEntity(id);
				if(Main.getDispatch().getClients().size() > 0) {
					Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
				}
			}
		}
	}

}
