package com.studio878.server.entities;

import com.studio878.server.app.Main;
import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.inventory.Item;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.packets.Packet21SendInventory;

public class ItemEntity extends PhysicsAffectedEntity {

	Item type;
	static final int Range = 75;
	long throwTime;
	boolean pickedUp = false;
	int level;
	public ItemEntity(double x, double y, Item type) {
		super(x, y, 24, 24, EntityName.Item);
		this.type = type;
		throwTime = System.currentTimeMillis();
		level = type.getLevel();
		data = ""+type.getType().getId() + "," + type.getLevel();
	}

	public void step() {
		super.step();
	
		if(System.currentTimeMillis() - throwTime > 60000) {
			EntityManager.unregisterEntity(this.getId());
			if(Main.getDispatch().getClients().size() != 0) {
				Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.getId()));
			}
		}
		if(System.currentTimeMillis() - throwTime > 1000 && !pickedUp) {
			for(Player p : EntityManager.getPlayers()) {
				if(Math.sqrt(Math.pow(p.x - x, 2) + Math.pow(p.y - y, 2)) <= Range && p.getInventory().countOpenPositions(type.getType(), level) != 0) {
					pickedUp = true;
					if(type != null) {
						if(type.getType().getWeaponType() == 1) {
							if(p.getInventory().containsMatchLevel(type.getType(), type.getLevel() )) {
								Item s = p.getInventory().getItem(type.getType());
								s.addAmmo((int) (Math.ceil(s.getDefaultClip()/2)), false); 
							} else {		
								Item is = new Item(type.getType(), 1, level, p);
								String[] parts = data.split(";");
								if(parts.length == 2) {
									is.setAmmo(Integer.parseInt(parts[1].split("/")[0]));
									is.setAmmo(Integer.parseInt(parts[1].split("/")[0]));
								}
								p.getInventory().addItem(is); 

							}
							String[] parts = data.split("~");
							if(parts.length == 2) {
								if(parts[1].equals(p.getName())) {
									CraftingRecipe r = CraftingRecipeList.lookupRecipe(this.type.getType());
									if(!p.hasCrafted(r)) {
										p.addCrafted(r);
									}
								}
							}
						} else {
							p.getInventory().add(type.getType(), level, 1);
						}
					}
					p.getHandle().getSender().sendPacket(new Packet21SendInventory(p.getHandle()));
					EntityManager.unregisterEntity(this.id);
					if(Main.getDispatch().getClients().size() > 0) {
						Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
					}
				}
			}
		}
	}


}
