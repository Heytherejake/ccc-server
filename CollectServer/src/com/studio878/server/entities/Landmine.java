package com.studio878.server.entities;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.events.EntityKilledEvent;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.util.MathUtil;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.world.Grid;

public class Landmine extends HealthEntity {

	boolean hasExploded = false;
	boolean hasSet = false;
	long hitTime = 0;

	public Landmine(double x, double y, String source) {
		super(x, y, 16, 32, EntityName.Landmine);
		this.health = 25;
		this.data = "" + source;
		this.sticky = true;
	}


	public void step() {
		if(!hasExploded) {
			super.step();
			if(this.xspeed == 0 && this.yspeed == 0 && !hasSet) {
				hasSet = true;
				movable = false;
				this.setState(1);
			}
			if(hasSet) {
				if(hitTime == 0) {
					for(HealthEntity e : EntityManager.getHealthEntities()) {
						if(e.getType() == EntityName.Landmine) continue;
						Rectangle r = new Rectangle((int) e.getX(), (int) e.getY(), e.getWidth(), e.getHeight());
						double md = 999;
						Point2D loc = new Point2D.Double(this.getX(), this.getY());
						for(Line2D l : MathUtil.getLinesFromRectangle(r)) {
							double disc = l.ptSegDist(loc);

							if(disc < md) {
								md = disc;
							}
						}
						if(md <= 16*5) {
							hitTime = System.currentTimeMillis();
							this.setState(2);
						}
					}
				}
				if(hitTime != 0 && System.currentTimeMillis() - hitTime >= 1000) {
					hasExploded = true;
					Block center = Grid.getClosestBlock((int) x, (int) y);
					ArrayList<Block> blocks = Grid.getCircle(center.getX(), center.getY(), 5);
					for(Block b: blocks) {
						b.setDropOnBreak(false);
						b.hit(null, 500, (byte) 10);
					}
					for(Entity e: EntityManager.getEntities()) {
						if(e instanceof HealthEntity) {
							Rectangle r = new Rectangle((int) e.getX(), (int) e.getY(), e.getWidth(), e.getHeight());
							double md = 999;
							Point2D loc = new Point2D.Double(this.getX(), this.getY());
							for(Line2D l : MathUtil.getLinesFromRectangle(r)) {
								double disc = l.ptSegDist(loc);

								if(disc < md) {
									md = disc;
								}
							}
							if(md <= 16*5) {
								HealthEntity h = (HealthEntity) e;
								double angle = Math.atan2(h.x - this.x, this.y - h.y);
								if(h.x < this.x) {
									angle -= 90;
								} else {
									angle -= 45;
								}
								h.setVelocity(7*Math.cos(angle), 15*Math.sin(angle));
								h.damage((int) (50 * ((80 - md)/80)), DamageMethod.Landmine, data);		


							}
						}
					}
					EntityManager.unregisterEntity(id);
					if(Main.getDispatch().getClients().size() > 0) {
						Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
					}
				}
			}
		}
	}
	public void kill() {
		if(!Hooks.call(Hook.ENTITY_KILLED, new EntityKilledEvent(this))) {
			return;
		}
		EntityManager.unregisterEntity(this.id);
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), this.id));
		}
	}
}
