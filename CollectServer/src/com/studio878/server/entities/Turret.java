package com.studio878.server.entities;

import com.studio878.server.app.Main;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.packets.Packet65UpdateEntityData;

public class Turret extends HealthEntity {
	double dir = 1;
	public Turret(double d, double e) {
		super(d, e, 16, 32, EntityName.Turret);
		this.data = "200";
		this.health = 500;
	}

	public void step() {
		super.step();
		if(dir > 0 && this.rotation > 45) {
			dir = -0.1;
		} else if (dir < 0 && this.rotation < -45) {
			dir = 0.1;
		}
		this.rotation += dir;
		if(Main.getDispatch().getClients().size() > 0) {
			Main.getDispatch().broadcastPacket(new Packet65UpdateEntityData(Main.getDispatch().getClients().get(0), this));
		}
	}

}
