package com.studio878.server.entities;

import com.studio878.server.block.Block;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.world.Grid;

public class NPC extends HealthEntity {

	public enum NPCBehavior {
		Static (0),
		Pacing (1),
		Aggressive(2),
		;
		int id;

		private NPCBehavior(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	int state = 0, prevDir = 1;
	Player tracking;

	NPCBehavior behavior = NPCBehavior.Pacing;

	float moveSpeed = 1.4f;

	int fov = 750;

	public NPC(double x, double y, int height, int width, EntityName type) {
		super(x, y, height, width, type);
		this.data = " ";
		this.health = 50;
	}

	public void step() {
		super.step();
		if(xspeed > 0) {
			facingDirection = 1;
			prevDir = 0;
		} else if (xspeed < 0) {
			facingDirection = -1;
			prevDir = 2;
		} else {
			facingDirection = prevDir;
		}
		switch(behavior) {
		case Pacing:
			int dx = (int) (x - (x%16));
			int dy = (int) (y - (y%16));
			if(facingDirection == 0 || facingDirection == 1) {
				dx += 64;
			}
			Block b = Grid.getClosestBlock(dx - 16, dy + height);
			Block bd = b.getBlockAdjacent(0, -1);
			if(b != null && bd != null) {
				if(!b.getType().getPermiability(null) && isLocationValid(x, y, (int) (Math.abs(xspeed)/xspeed))) {
					if(facingDirection == 0 || facingDirection == 1) {
						setXSpeed(moveSpeed);
					} else {
						setXSpeed(-moveSpeed);
					}
				} else {
					if(facingDirection == 0 || facingDirection == 1) {
						facingDirection = -1;
						prevDir = -1;
					} else {
						facingDirection = 1;
						prevDir = 1;
					}
				}


			}
			break;
		case Aggressive:
			if(tracking == null) {
				for(Player p : EntityManager.getPlayers()) {
					if(Math.sqrt(Math.pow(x - p.getX(), 2) + Math.pow(y - p.getY(), 2)) <= fov) {
						tracking = p;

					}
				}
			} else {

				dx = (int) (x - (x%16));
				dy = (int) (y - (y%16));
				if(facingDirection == 0 || facingDirection == 1) {
					dx += 32;
				}
				b = Grid.getClosestBlock(dx + 16, dy + height);
				System.out.println(b.getType() + "|" + (b != null) + "|" + !b.getType().getPermiability(null)+ "|" +  (Math.abs(tracking.x - x) >= 50));
				if((b != null && (b.getType().getPermiability(null) || !b.getBlockAdjacent(0, -1).getType().getPermiability(null)) && Math.abs(tracking.x - x) >= 50 && yspeed == 0) || (Math.abs(tracking.x - x) <= 100 && Math.abs(tracking.y - y) >= 50 && yspeed == 0)) {
					setYSpeed(-5);

				} 
				if(tracking.x - x >= 15) {
					setXSpeed(moveSpeed);
				} else if (tracking.x - x <= -15) {
					setXSpeed(-moveSpeed);

				} else {
					setXSpeed(0);
				}
		
				if(Math.sqrt(Math.pow(x - tracking.getX(), 2) + Math.pow(y - tracking.getY(), 2)) >= fov) {
					tracking = null;
				}

			}
		}
	}
}
