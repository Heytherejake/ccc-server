package com.studio878.server.entities;

import com.studio878.server.entities.EntityManager.EntityName;

public class ResearchBench extends PhysicsAffectedEntity {

	public ResearchBench(double x, double y) {
		super(x, y, 64, 64, EntityName.ResearchBench);
		movable = false;
	}

}
