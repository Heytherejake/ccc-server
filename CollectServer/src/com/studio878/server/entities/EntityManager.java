package com.studio878.server.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.block.BlockType;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet15AddEntity;
import com.studio878.server.packets.Packet18RemoveEntity;
import com.studio878.server.util.TeamManager;



public class EntityManager {

	public enum EntityName {
		Player (0), 
		Grenade (1),
		Item (2),
		Rocket (3),
		Generator (4),
		FallingBlock (5),
		Landmine (6),
		Workbench (7),
		ResearchBench (8),
		GenericNPC (9),
		Turret (10),
		;

		private static HashMap<Integer, EntityName> idLookup = new HashMap<Integer, EntityName>();

		private int id;
		private EntityName(int i) {
			id = i;
		}

		public int getId() {
			return id;
		}

		public static EntityName lookupId(int i) {
			return idLookup.get(i);
		}
		static {
			for(EntityName t: values()) {
				idLookup.put(t.getId(), t);
			}
		}		
	}

	static CopyOnWriteArrayList<Entity> entities = new CopyOnWriteArrayList<Entity>();
	static int highestId = 0;
	public static void registerEntity(Entity e) {
		entities.add(e);
		highestId++;
		Console.log("Entity registered: " + e.getType() + " (" + e.getId() + ")");

	}

	public static void dropItem(ItemID type, int x, int y) {
		ItemEntity es = new ItemEntity(x, y, new Item(type, 1, null));
		EntityManager.registerEntity(es);
		if(Main.getDispatch().getClients().size() > 0) {
			Main.getDispatch().broadcastPacket(new Packet15AddEntity(Main.getDispatch().getClients().get(0), es));
		}
	}

	public static void reset() {
		for(Entity e : entities) {
			if(!(e instanceof Player)) {
				entities.remove(e);
				if(Main.getDispatch().getClients().size() != 0) {
					Main.getDispatch().broadcastPacket(new Packet18RemoveEntity(Main.getDispatch().getClients().get(0), e.getId()));
				}
			}
		}
	}

	public static void unregisterEntity(int id) {
		ArrayList<Entity> toReplace = new ArrayList<Entity>();
		for(Entity e: entities) {
			if(e != null) {
				if(e.id != id) {
					toReplace.add(e);
				}
			}
		}
		entities.clear();
		entities.addAll(toReplace);
		Console.log("Entity unregistered: " + id);
	}

	public static Entity getEntity(int id) {
		for(Entity e: entities) {
			if(e.getId() == id) {
				return e;
			}
		}
		return null;
	}
	public static void unregisterEntity(Entity e) {
		try {
			entities.remove(e);
		} catch (Exception es) {
			es.printStackTrace();
		}
	}
	public static int getHighestId() {
		return highestId;
	}
	public static CopyOnWriteArrayList<Entity> getEntities() {
		return entities;
	}

	public static ArrayList<HealthEntity> getHealthEntities() {
		ArrayList<HealthEntity> results = new ArrayList<HealthEntity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof HealthEntity) {
				results.add((HealthEntity) entities.get(i));
			}
		}
		return results;
	}

	public static ArrayList<PhysicsAffectedEntity> getPhysicsAffectedEntities() {
		ArrayList<PhysicsAffectedEntity> results = new ArrayList<PhysicsAffectedEntity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof PhysicsAffectedEntity) {
				results.add((PhysicsAffectedEntity) entities.get(i));

			}
		}
		return results;
	}
	public static ArrayList<Player> getPlayers() {
		ArrayList<Player> results = new ArrayList<Player>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof Player) {
				results.add((Player) entities.get(i));

			}
		}
		return results;
	}

	public static Entity cloneEntity(EntityName type, int x, int y, String data) {
		try {
			switch(type) {
			case FallingBlock:
				return new FallingBlock(x, y, BlockType.lookupId(Integer.parseInt(data)));
			case Generator:
				return new Generator(x, y, TeamManager.matchTeam(data));
			case Item:
				String[] split = data.split("|");
				return new ItemEntity(x, y, new Item(ItemID.lookupId(Integer.parseInt(split[0])), 1, Integer.parseInt(split[1]), null));
			case Grenade:
				return new Grenade(x, y, data);
			case Landmine:
				return new Landmine(x, y, data);
			case ResearchBench:
				return new ResearchBench(x, y);
			case Workbench:
				return new Workbench(x, y);
			case GenericNPC:
				return new NPC(x, y, 64, 32, EntityName.GenericNPC);
			case Turret:
				return new Turret(x, y);
			}
		} catch (Exception e) {
			System.out.println("Invalid entity of type " + type + " with data " + data);
		}
		return null;
	}
	public static ArrayList<Entity> getDrawableEntities() {
		ArrayList<Entity> results = new ArrayList<Entity>();
		for(int i = 0; i < entities.size(); i++) {
			if(entities.get(i) instanceof Entity) {
				results.add((Entity) entities.get(i));

			}
		}
		return results;
	}




}
