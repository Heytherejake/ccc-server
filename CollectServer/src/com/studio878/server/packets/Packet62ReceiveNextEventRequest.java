package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet62ReceiveNextEventRequest extends Packet {

	public Packet62ReceiveNextEventRequest(NetPlayer p) {
		super(p);
	}

	@Override
	public String getDataString() {
		return "62";
	}

	@Override
	public void process() {
		p.getPlayer().processNextMapEvent();
	}

}
