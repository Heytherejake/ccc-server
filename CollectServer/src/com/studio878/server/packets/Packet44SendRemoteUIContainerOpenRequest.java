package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;

public class Packet44SendRemoteUIContainerOpenRequest extends Packet {

	RemoteUIContainer container;
	
	public Packet44SendRemoteUIContainerOpenRequest(NetPlayer p, RemoteUIContainer container) {
		super(p);
		this.container = container;
	}
	
	public String getDataString() {
		return "44:" + container.getId();
	}
	
	public void process() {
		
	}

}
