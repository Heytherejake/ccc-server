package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet38RequestTeamInfo extends Packet{

	public Packet38RequestTeamInfo(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "38";
	}

	public void process() {
		p.getSender().sendPacket(new Packet37SendPlayerInfo(p));
		
	}

}
