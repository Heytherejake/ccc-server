package com.studio878.server.packets;

import com.studio878.server.inventory.Inventory;
import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;

public class Packet21SendInventory extends Packet{
	
	public Packet21SendInventory(NetPlayer p) {
		super(p);
	}
	
	public String getDataString() {
		StringBuilder msg = new StringBuilder();
		msg.append("21:");
		if(p.getPlayer() == null) return "21";
		Inventory s =  p.getPlayer().getInventory();
		for(Item i : s.getItems()) {
			msg.append(i.getType().getId() + "," + i.getAmount() + "," + i.getSlot() + "," + i.getData() +  "," + i.getLevel() + ":");
		}
		return msg.toString();
	}

	public void process() {
		
	}

}
