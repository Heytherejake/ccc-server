package com.studio878.server.packets;

import com.studio878.server.app.Main;
import com.studio878.server.entities.Player;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.logging.Console;

public class Packet37SendPlayerInfo extends Packet{

	public Packet37SendPlayerInfo(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		StringBuilder msg = new StringBuilder();
		msg.append("37");
		for(Player p : Main.getServer().getPlayers()) {
			try {
				int cid = -1;
				if(p.getCharacter() != null) {
					cid = p.getCharacter().getId();
				}
			msg.append(":" + p.getId() + "," + p.getName() + "," + p.getTeam() + "," + p.getLatency() + "," + cid);
			} catch (NullPointerException e) {
				Console.log("Invalid player information for " + p.getName());
			}
		}
		return msg.toString();
	}

	
	public void process() {
		
	}

}
