package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet36SendCraftingConfirmation extends Packet{

	int id;
	String message;
	boolean didSucceed;
	public Packet36SendCraftingConfirmation(NetPlayer p, int id, String message, boolean didSucceed) {
		super(p);
		this.id = id;
		this.message = message;
		this.didSucceed = didSucceed;
	}

	public String getDataString() {
		return "36:" + id + ":" + message + ":" + didSucceed;
	}
	
	public void process() {
		
	}
	
	

}
