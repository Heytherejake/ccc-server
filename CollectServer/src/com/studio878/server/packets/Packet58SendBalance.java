package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet58SendBalance extends Packet {

	public Packet58SendBalance(NetPlayer p) {
		super(p);
	}

	@Override
	public String getDataString() {
		return "58:" + p.getPlayer().getBalance();
	}

	public void process() {
		
	}

}
