package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet39SendHandshakeRequest extends Packet{

	public Packet39SendHandshakeRequest(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "39:" + System.currentTimeMillis();
	}

	public void process() {
		
	}

}
