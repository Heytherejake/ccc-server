package com.studio878.server.packets;

import com.studio878.server.app.Main;
import com.studio878.server.events.PlayerChatEvent;
import com.studio878.server.events.PlayerCommandEvent;
import com.studio878.server.input.PlayerCommandParser;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.logging.Console;
import com.studio878.server.util.ChatColors;

public class Packet07ReceiveChat extends Packet{
	String username;
	String message;
	public Packet07ReceiveChat(NetPlayer p, String user, String message) {
		super(p);
		username = user;
		this.message = message;
	}


	public String getDataString() {
		return "07:" + username + ":" + message;
	}

	public void process() {
		if(message.substring(0, 1).equals("/")) {
			String[] split = message.split(" ");
			String[] parts = new String[split.length - 1];
			for(int i = 1; i < split.length; i++) {
				parts[i-1] = split[i];
			}
			if(Hooks.call(Hook.PLAYER_COMMAND, new PlayerCommandEvent(p.getPlayer(), split[0], parts))) {
				if(!PlayerCommandParser.processCommand(p.getPlayer(), message)) {
					getHandle().getSender().sendPacket(new Packet08SendMessage(p, ChatColors.Red + "Invalid command"));
				}
			}
		} else {
			if(Hooks.call(Hook.PLAYER_CHAT, new PlayerChatEvent(p.getPlayer(), message))) {
				Console.write(getHandle().getPlayer().getName() + ": " + message);
				for(NetPlayer ps : Main.getDispatch().getClients()) {
					if(ps.getPlayer().getTeam() == p.getPlayer().getTeam()) {
						ps.getSender().sendPacket(new Packet08SendMessage(ps, ChatColors.Green + getHandle().getPlayer().getName() + ": " + ChatColors.White + message));
					}
				}
			}
		}
	}

}
