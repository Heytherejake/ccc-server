package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.story.MapEvent;

public class Packet61SendMapEvent extends Packet {

	MapEvent event;
	
	public Packet61SendMapEvent(NetPlayer p, MapEvent event) {
		super(p);
		this.event = event;
	}

	public String getDataString() {
		return "61:" + event.getType().getId() + ":" + event.getData();
	}

	public void process() {
		
	}

}
