package com.studio878.server.packets;

import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Settings;
import com.studio878.server.util.Movement.MovementType;

public class Packet12ReceiveMovement extends Packet{

	MovementType type;

	public Packet12ReceiveMovement(NetPlayer p, MovementType t) {
		super(p);
		this.type = t;
	}

	public String getDataString() {
		return "12:" + type;
	}

	public void process() {
		try {
			switch(type) {
			case Jump:
				p.getPlayer().jump();
				break;
			case Left:
				if(Settings.AllowInAirMovement || (p.getPlayer().getYSpeed() == 0 || p.getPlayer().getXSpeed() == 0)) {
					p.getPlayer().setXSpeed(-2*p.getPlayer().calculateBonus(BonusItemType.Movement));
				}
				p.getPlayer().setFacingDirection(-1);
				break;
			case Right:
				if(Settings.AllowInAirMovement || (p.getPlayer().getYSpeed() == 0 || p.getPlayer().getXSpeed() == 0)) {
					p.getPlayer().setXSpeed(2*p.getPlayer().calculateBonus(BonusItemType.Movement));
				}
				p.getPlayer().setFacingDirection(1);

				break;
			case None:
				p.getPlayer().setFacingDirection(0);
				p.getDispatch().broadcastPacket(new Packet13SendMovement(p, p.getPlayer().getX(), p.getPlayer().getY(), p.getPlayer().getId(), 0, p.getPlayer().getRotation()));
				break;
			case FlippedNone:
				p.getPlayer().setFacingDirection(2);
				p.getDispatch().broadcastPacket(new Packet13SendMovement(p, p.getPlayer().getX(), p.getPlayer().getY(), p.getPlayer().getId(), 2, p.getPlayer().getRotation()));
				break;
			}

		} catch (Exception e) {

		}
	}
}
