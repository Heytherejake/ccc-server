package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet11RemovePlayer extends Packet{
	String name;
	int id;
	
	public Packet11RemovePlayer(NetPlayer p, String n, int id) {
		super(p);
		this.name = n;
		this.id = id;
	}
	public String getDataString() {
		return "11:" + name + ":" + id;
	}
	
	public void process() {
		
	}

}
