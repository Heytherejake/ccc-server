package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet18RemoveEntity extends Packet{
	int id;
	
	public Packet18RemoveEntity(NetPlayer p, int id) {
		super(p);
		this.id = id;
	}
	public String getDataString() {
		return "18:" + id;
	}
	
	public void process() {
		
	}

}
