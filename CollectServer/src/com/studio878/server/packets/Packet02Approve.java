package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Settings;

public class Packet02Approve extends Packet{

	public Packet02Approve(NetPlayer p) {
		super(p);
	}


	public String getDataString() {
		return "02:" + Settings.StoryMode + ":" + Settings.ClientSideMovement;
	}
	
	public void process() {	
	}

}
