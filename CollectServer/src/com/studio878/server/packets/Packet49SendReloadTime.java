package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet49SendReloadTime extends Packet{

	int seconds;
	
	public Packet49SendReloadTime(NetPlayer p, int seconds) {
		super(p);
		this.seconds = seconds;
	}

	public String getDataString() {
		return "49:" + seconds;
	}

	public void process() {
		
	}

	
}
