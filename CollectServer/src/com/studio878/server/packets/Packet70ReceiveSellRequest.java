package com.studio878.server.packets;

import com.studio878.server.inventory.Inventory;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;

public class Packet70ReceiveSellRequest extends Packet {

	int id, level;
	
	public Packet70ReceiveSellRequest(NetPlayer p, int id, int level) {
		super(p);
		this.id = id;
		this.level = level;
	}

	public String getDataString() {
		return "70:" + id + ":" + level;
	}

	public void process() {
		ItemID iid = ItemID.lookupId(id);
		Inventory inv = p.getPlayer().getInventory();
		if(inv.contains(iid)) {
			inv.subtract(iid, level, 1);
			p.getPlayer().addMoney(iid.getSellCost());
		}
		p.getPlayer().sendInventoryUpdate();
		
	}

}
