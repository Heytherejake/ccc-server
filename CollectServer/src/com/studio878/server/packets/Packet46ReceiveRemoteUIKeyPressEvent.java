package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;
import com.studio878.server.remoteui.RemoteUIContainerEventManager;
import com.studio878.server.remoteui.RemoteUIKeyPressEvent;

public class Packet46ReceiveRemoteUIKeyPressEvent extends Packet {

	int container, button, x, y;

	public Packet46ReceiveRemoteUIKeyPressEvent(NetPlayer p, int c, int x, int y, int b) {
		super(p);
		container = c;
		button = b;
		this.x = x;
		this.y = y;
	}

	public String getDataString() {
		return "46:" + container + ":" + x + ":" + y + ":" + button;
	}

	public void process() {
		RemoteUIContainer c = p.getPlayer().getContainer(container);
		if(c != null) {
			RemoteUIContainerEventManager em = c.getEventManager();
			if(em != null) {
				em.onKeyPress(new RemoteUIKeyPressEvent(p.getPlayer(), c,  x, y, button));
			}
		}
	}
}

