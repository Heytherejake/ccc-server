package com.studio878.server.packets;

import com.studio878.server.entities.Entity;
import com.studio878.server.io.NetPlayer;

public class Packet15AddEntity extends Packet{

	Entity entity;
	public Packet15AddEntity(NetPlayer p, Entity entity) {
		super(p);
		this.entity = entity;
	}
	public String getDataString() {
		String data = entity.getData();
		if(data == null || data.length() == 0) {
			data = "null";
		}
		return "15:" + entity.getId() + ":" + entity.getType().getId() + ":" + entity.getX() + ":" + entity.getY() + ":" + data;
	}

	public void process() {
		
	}


}
