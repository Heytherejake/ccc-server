package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet09SendBlockDamage extends Packet{
	int x, y, damage;
	public Packet09SendBlockDamage(NetPlayer p, int x, int y, int damage) {
		super(p);
		this.x = x;
		this.y = y;
		this.damage = damage;
	}

	public String getDataString() {
		return "09:" + p.getPlayer().getName() + ":" + x + ":" + y + ":" + damage;
	}

	public void process() {
		
	}

}
