package com.studio878.server.packets;

import java.util.ArrayList;

import com.studio878.server.block.Block;
import com.studio878.server.crafting.BonusItem;
import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.events.BlockDestroyedEvent;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.weapons.Shovel;
import com.studio878.server.weapons.Weapon;
import com.studio878.server.world.Grid;

public class Packet06ReceiveBlockDamage extends Packet{
	int x, y;
	byte damage;
	public Packet06ReceiveBlockDamage(NetPlayer p, int x, int y) {
		super(p);
		this.x = x;
		this.y = y;
	}
	public String getDataString() {
		return "06:" + x + ":" + y ;
	}

	public void process() {
		Block b = Grid.getBlockAt(x, y);
		Weapon w = getHandle().getPlayer().getWeapon();
		if(w instanceof Shovel) {
			double px = getHandle().getPlayer().getX();
			double py = getHandle().getPlayer().getY();
			if(x*16 >= px - 100 && x*16 <= px + 100 && y*16 >= py - 100 && y*16 <= py + 100) {
				int rad = (int) (p.getPlayer().calculateBonus(BonusItemType.DigRadius)) - 1;
				if(rad < 0) {
					rad = 0;
				}
				ArrayList<Block> bd = Grid.getCircle(b.getX(), b.getY(), rad);
				for(Block bs : bd) {

					if(Hooks.call(Hook.BLOCK_DESTROY, new BlockDestroyedEvent(p.getPlayer(), bs))) {
						bs.hit(getHandle().getPlayer(), 15, (byte) 1);
					//	getHandle().getDispatch().broadcastPacket(new Packet09SendBlockDamage(getHandle(), x, y, bs.getCondition()));
					}
				}
			}
		}
	}
}

