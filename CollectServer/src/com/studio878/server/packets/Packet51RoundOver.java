package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.TeamManager.Team;

public class Packet51RoundOver extends Packet {

	Team team;
	int time;
	
	public Packet51RoundOver(NetPlayer p, Team team, int time) {
		super(p);
		this.team = team;
		this.time = time;
	}

	public String getDataString() {
		return "51:" + team.getId() + ":" + time;
	}

	public void process() {
		
	}

}
