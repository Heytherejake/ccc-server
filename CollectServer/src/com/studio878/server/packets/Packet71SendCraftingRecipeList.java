package com.studio878.server.packets;

import java.awt.Point;

import com.studio878.server.characters.PlayerCharacter;
import com.studio878.server.crafting.BonusItem;
import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;

public class Packet71SendCraftingRecipeList extends Packet {

	public Packet71SendCraftingRecipeList(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		String msg = "";
		for(CraftingRecipe item : CraftingRecipeList.Recipes) {
			msg += item.getId() + "_!_" + item.getName() + "_!_" + item.getDescription() + "_!_";
			if(item.getResult() != null) {
				msg += item.getResult().getType().getId() + "_!_" + item.getResult().getAmount() + "_!_" + item.getType().getId() + "_!_";
			} else {
 				msg += "-1_!_-1_!_" + item.getType().getId() + "_!_";
			}
			
			Point p = item.getImage();
			if(item.getImage() == null) {
				p = new Point(-1, -1);
			}
			msg += p.x + "_!_" + p.y + "_!_";
			
			if(item.getRequirements() != null) {
			for(Item i : item.getRequirements()) {
				msg += i.getType().getId() + "_&_" + i.getAmount() + "_%_";
			}
			msg = msg.substring(0, msg.length() - 3) + "_!_";
			} else {
				msg += "null_!_";
			}
			
			if(item.getRestrictedCharacters() != null) {
				for(PlayerCharacter s : item.getRestrictedCharacters()) {
					msg += s.getId() + "_%_";
				}
				msg = msg.substring(0, msg.length() - 3) + "_!_";

			} else {
				msg += "null_!_";
			}
			
			for(String s : item.getBenefits()) {
				msg += s + "_%_";
			}
			msg = msg.substring(0, msg.length() - 3) + "_!_";
			
			if(item.getDownsides() != null) {
				for(String s : item.getDownsides()) {
					msg += s + "_%_";
				}
				msg = msg.substring(0, msg.length() - 3) + "_!_";
			} else {
				msg += "null_!_";
			}
			
			if(item.getBonuses() != null) {
				for(BonusItem i : item.getBonuses()) {
					msg += i.getType().getId() + "_&_" + i.getAmount() + "_%_";
				}
				msg = msg.substring(0, msg.length() - 3) + "_!_";
			} else {
				msg += "null_!_";
			}
			
			msg += item.getBuyCost();
			msg += "~";
		}
		return "71:" + msg.substring(0, msg.length() - 1);
	}

	public void process() {

	}

}
