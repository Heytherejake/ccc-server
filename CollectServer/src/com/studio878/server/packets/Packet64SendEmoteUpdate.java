package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Emote.EmoteType;

public class Packet64SendEmoteUpdate extends Packet {

	EmoteType type;
	int id;
	
	public Packet64SendEmoteUpdate(NetPlayer p, EmoteType type, int id) {
		super(p);
		this.type = type;
		this.id = id;
	}

	public String getDataString() {
		return "64:" + type.getId() + ":" + id;
	}

	public void process() {
		
	}

}
