package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;

public class Packet43SendRemoteUIContainerCloseRequest extends Packet{

	RemoteUIContainer container;
	
	public Packet43SendRemoteUIContainerCloseRequest(NetPlayer p, RemoteUIContainer container) {
		super(p);
		this.container = container;
	}

	public String getDataString() {
		return "43:" + container.getId();
	}

	public void process() {
		
	}

}
