package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.world.Grid;

public class Packet03WorldSize extends Packet{

	public Packet03WorldSize(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "03:" + Grid.getGrid().length + ":" + Grid.getGrid()[0].length;
	}
	
	public void process() {
		
	}

}
