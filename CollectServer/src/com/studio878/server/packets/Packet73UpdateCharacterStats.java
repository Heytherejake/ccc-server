package com.studio878.server.packets;

import com.studio878.server.characters.PlayerCharacter;
import com.studio878.server.io.NetPlayer;

public class Packet73UpdateCharacterStats extends Packet {

	public Packet73UpdateCharacterStats(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		PlayerCharacter c = p.getPlayer().getCharacter();
		return "73:" + c.getRangedDamage() + ":" + c.getMeleeDamage() + ":" + c.getMoveSpeed() + ":" + c.getStrength() + ":" + c.getDexterity() + ":" + c.getIntelligence() + ":" + c.getHealth() + ":" + c.getMana() + ":" + c.getHealthPerLevel() + ":" + c.getManaPerLevel() + ":" + c.getArmor() + ":" + c.getMagicDefence() + ":" + c.getHealthRegen() + ":" + c.getManaRegen() + ":" + c.getLifeSteal();
	}
	public void process() {
		
	}

}
