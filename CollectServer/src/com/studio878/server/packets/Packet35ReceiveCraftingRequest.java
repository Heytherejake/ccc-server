package com.studio878.server.packets;

import java.util.ArrayList;

import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.crafting.CraftingRecipeList.RecipeType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.entities.ResearchBench;
import com.studio878.server.entities.Workbench;
import com.studio878.server.events.CraftingSuccessEvent;
import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.logging.Console;

public class Packet35ReceiveCraftingRequest extends Packet{

	int id;

	public Packet35ReceiveCraftingRequest(NetPlayer p, int id) {
		super(p);
		this.id = id;
	}

	public String getDataString() {
		return "35:" + id;
	}

	public void process() {
		boolean canCraft = false;
		Player ps = p.getPlayer();
		CraftingRecipe r = CraftingRecipeList.getRecipe(id);

		for(Entity e : EntityManager.getEntities()) {
			if((r.getType() != RecipeType.Research && e instanceof Workbench) || (r.getType() == RecipeType.Research && e instanceof ResearchBench)) {
				if(Math.sqrt(Math.pow(e.getX() - ps.getX(), 2) + Math.pow(e.getX() - ps.getX(), 2)) <= 150) {
					canCraft = true;
				}
			}
		}
		if(!canCraft) {
			Console.log("Invalid crafting request: Out of range.");
			return;
		}
		if(r != null) {

				if(!r.isValidForClass(p.getPlayer().getCharacter())) {
					p.getSender().sendPacket(new Packet36SendCraftingConfirmation(p, r.getId(), "You cannot research this as your chosen Specialization", false));
					Console.log("Invalid crafting request: Class mismatch.");
					return;
				}
				CraftingSuccessEvent ev = new CraftingSuccessEvent(p.getPlayer(), r);
				if(!Hooks.call(Hook.CRAFTING_SUCCEEDED, ev)) {
					p.getSender().sendPacket(new Packet36SendCraftingConfirmation(p, r.getId(), ev.getFailMessage(), false));
					Console.log("Crafting stopped by plugin");
					return;
				}
				if(r.getType() != RecipeType.Research) {
					int cost = r.getBuyCost();
					ArrayList<Item> used = new ArrayList<Item>();
					for(Item i : r.getRequirements()) {
						if(p.getPlayer().getInventory().contains(i.getType(), i.getAmount())) {
							cost -= i.getType().getBuyCost()*i.getAmount();

							used.add(new Item(i.getType(), i.getAmount(), null));
						}
					}
					if(p.getPlayer().hasEnoughMoney(cost)) {
						p.getPlayer().subtractMoney(cost);
						for(Item i : used) {
							p.getPlayer().getInventory().subtract(i.getType(), 0, i.getAmount());
						}
						p.getPlayer().getInventory().add(r.getResult().getType(), r.getResult().getLevel(), r.getResult().getAmount());
						return;
					}
					Console.log("Invalid crafting request: Cannot afford.");
					p.getSender().sendPacket(new Packet36SendCraftingConfirmation(p, r.getId(), "You cannot afford this recipe.", false));

					return;
				} else {
					boolean success = true;
					for(Item i: r.getRequirements()) {
						if(!p.getPlayer().getInventory().contains(i.getType(), i.getAmount())) {
							success = false;
						}
					}
					if(!success) {
						return;
					}
					boolean sc = true; 
					for(CraftingRecipe rs : p.getPlayer().getBonuses()) {
						if(rs.getId() == r.getId()) {
							sc = false;
						}
					}
					if(sc) {
						for(Item i: r.getRequirements()) {
							p.getPlayer().getInventory().subtract(i.getType(), 0, i.getAmount());
						}
						p.getPlayer().addBonus(r);
						p.getPlayer().sendInventoryUpdate();
						return;
					} else {
						p.getSender().sendPacket(new Packet36SendCraftingConfirmation(p, r.getId(), "You have already researched this.", false));
						return;

					}
				}

		} 
		p.getSender().sendPacket(new Packet36SendCraftingConfirmation(p, -1, "Invalid recipe", false));
		Console.log("Invalid crafting request: Invalid recipe.");

	}


}
