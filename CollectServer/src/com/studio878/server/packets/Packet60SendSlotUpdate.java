package com.studio878.server.packets;

import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;

public class Packet60SendSlotUpdate extends Packet{

	Item item;
	
	public Packet60SendSlotUpdate(NetPlayer p, Item item) {
		super(p);
		this.item = item;
	}

	public String getDataString() {
		return "60:" + item.getType().getId() + "," + item.getAmount() + "," + item.getSlot() + "," + item.getData();
	}

	public void process() {
		
	}

}
