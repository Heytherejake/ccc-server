package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet57RequestBalance extends Packet {

	public Packet57RequestBalance(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "57";
	}

	public void process() {
		p.getSender().sendPacket(new Packet58SendBalance(p));
		
	}

}
