package com.studio878.server.packets;

import com.studio878.server.events.InventoryRequestEvent;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;

public class Packet20RequestInventory extends Packet{

	public Packet20RequestInventory(NetPlayer p) {
		super(p);
	}
	public String getDataString() {
		return "20";
	}
	
	public void process() {
		if(!Hooks.call(Hook.INVENTORY_REQUESTED, new InventoryRequestEvent(p.getPlayer(), p.getPlayer().getInventory()))) {
			return;
		}
		p.getSender().sendPacket(new Packet21SendInventory(p));
	}

}
