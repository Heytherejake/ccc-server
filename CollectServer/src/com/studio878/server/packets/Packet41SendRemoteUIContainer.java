package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;

public class Packet41SendRemoteUIContainer extends Packet{

	RemoteUIContainer container;
	
	public Packet41SendRemoteUIContainer(NetPlayer p, RemoteUIContainer container) {
		super(p);
		this.container = container;
	}
	
	public String getDataString() {
		return "41:" + container.getDataString();
	}
	
	public void process() {
		
	}

}
