package com.studio878.server.packets;

import com.studio878.server.block.Block;
import com.studio878.server.io.NetPlayer;

public class Packet54PlayPlaceSound extends Packet {

	Block block;
	
	public Packet54PlayPlaceSound(NetPlayer p, Block block) {
		super(p);
		this.block = block;
	}

	public String getDataString() {
		return "54:" + block.getX() + ":" + block.getY();
	}

	public void process() {
		
	}

}
