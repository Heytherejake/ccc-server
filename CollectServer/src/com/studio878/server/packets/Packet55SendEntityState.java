package com.studio878.server.packets;

import com.studio878.server.entities.Entity;
import com.studio878.server.io.NetPlayer;

public class Packet55SendEntityState extends Packet {

	Entity entity;
	
	public Packet55SendEntityState(NetPlayer p, Entity entity) {
		super(p);
		this.entity = entity;
	}

	public String getDataString() {
		return "55:" + entity.getId() + ":" + entity.getState();
	}

	public void process() {
		
	}
	

}
