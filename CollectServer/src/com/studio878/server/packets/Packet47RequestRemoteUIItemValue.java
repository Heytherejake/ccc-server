package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIItem;

public class Packet47RequestRemoteUIItemValue extends Packet{

	RemoteUIItem item;
	public Packet47RequestRemoteUIItemValue(NetPlayer p, RemoteUIItem item) {
		super(p);
		this.item = item;
	}

	public String getDataString() {
		return "47:" + item.getParent().getId() + ":" + item.getId();
	}

	public void process() {
		
	}

}
