package com.studio878.server.packets;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.studio878.server.app.Main;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.events.PlayerJoinEvent;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.logging.Console;
import com.studio878.server.util.API;
import com.studio878.server.util.ChatColors;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager;


public class Packet01Connect extends Packet{
	private String username;
	private String session;
	private String version;

	public Packet01Connect(NetPlayer p, String username, String session, String version) {
		super(p);
		this.username = username;
		this.session = session;
		this.version = version;
	}

	public String getDataString() {
		return "01:"+username + ":" + session + ":" + version;
	}

	public void process() {
		try {
			boolean isValid = false;
			String data = null;
			for(Player ps : Main.getServer().getPlayers()) {
				if(ps != null && ps.getName().equals(username)) {
					p.getSender().sendPacket(new Packet16KickPlayer(p, "This account is already logged into this server from a different location."));
					return;
				}
			}
			try {
				JSONObject content = API.getObject("http://api.studio878software.com/user/" + username + "/session/auth/" + session);
				if(content.get("auth").equals(true) && content.get("username").equals(username)) {
					isValid = true;
				}
			} catch (Exception e) {
				p.getSender().sendPacket(new Packet16KickPlayer(p, "Server is disconnected from studio878software.com and could not verify login credentials."));
				return;
			}

			PlayerJoinEvent ev = new PlayerJoinEvent(username);
			if(!Hooks.call(Hook.PLAYER_JOIN, ev)) {
				getHandle().getSender().sendPacket(new Packet16KickPlayer(p, ev.getKickMessage()));
				getHandle().disconnectPlayer();
				return;

			}
			if(isValid) {
				if(getHandle().getDispatch().getClients().size() > Settings.MaxClients) {
					getHandle().getSender().sendPacket(new Packet16KickPlayer(p, "Server is full."));
					getHandle().disconnectPlayer();
					Console.write(username + " could not join (Reason: Server capacity reached)");
					return;
				} else if (!version.equalsIgnoreCase(Settings.Version)) {
					getHandle().getSender().sendPacket(new Packet16KickPlayer(p, "This server is running version " + Settings.Version + " (You are running " + version + ")"));
					getHandle().disconnectPlayer();
					Console.write(username + " could not join (Reason: Version mismatch)");
					return;
				}

				getHandle().setPlayer(username);
				getHandle().getPlayer().setSessionID(session);

				getHandle().getPlayer().getMultiplayerSettingsHandler().updateMultiplayerVariables();

				TeamManager.assignTeam(p.getPlayer());
				getHandle().getSender().sendPacket(new Packet67SendItemList(getHandle()));
				getHandle().getSender().sendPacket(new Packet68SendBlockList(getHandle()));
				getHandle().getSender().sendPacket(new Packet71SendCraftingRecipeList(getHandle()));
				getHandle().getSender().sendPacket(new Packet03WorldSize(getHandle()));
				getHandle().getSender().sendPacket(new Packet04SendGrid(getHandle()));
				getHandle().getSender().sendPacket(new Packet10SendEntityList(getHandle()));

				getHandle().getSender().sendPacket(new Packet02Approve(getHandle()));


				Console.write(username + " has connected (" + getHandle().getSocket().getInetAddress().getHostAddress() + ")");
				getHandle().getDispatch().broadcastPacket(new Packet08SendMessage(getHandle(), ChatColors.Yellow + getHandle().getPlayer().getName() + " has joined the game."));
				getHandle().getDispatch().broadcastPacket(new Packet15AddEntity(getHandle(), p.getPlayer()));
				getHandle().getDispatch().broadcastPacket(new Packet37SendPlayerInfo(getHandle()));
				EntityManager.registerEntity(getHandle().getPlayer());
				getHandle().getPlayer().respawn();
			} else {
				p.getSender().sendPacket(new Packet16KickPlayer(p, "Invalid username or session ID"));
			}
		} catch (Exception e) {
			if(Settings.DebugMode) {
				e.printStackTrace();
			}
		}
	}

}
