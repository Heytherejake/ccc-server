package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public abstract class Packet {
	NetPlayer p;
	public Packet(NetPlayer p) {
		this.p = p;
	}
	public abstract String getDataString();
	
	public NetPlayer getHandle() {
		return p;
	}
	
	public abstract void process();
}

 