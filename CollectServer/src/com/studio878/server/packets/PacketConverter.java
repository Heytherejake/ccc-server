package com.studio878.server.packets;

import java.io.IOException;

import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.logging.Console;
import com.studio878.server.util.Emote.EmoteType;
import com.studio878.server.util.Movement.MovementType;

public class PacketConverter {
	public static Packet createPacket(NetPlayer p, String m) {
		int id = 0;
		try {
			String[] split = m.split(":");
			id = Integer.parseInt(split[0]);
			switch (id) {
			case 1:
				return new Packet01Connect(p, split[1], split[2], split[3]);
			case 5:
				return new Packet05Disconnect(p);
			case 6:
				return new Packet06ReceiveBlockDamage(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 7:
				String message = "";
				for(int i = 2; i < split.length; i++) {
					message += split[i] + ":";
				}
				message = message.substring(0, message.length() - 1);
				return new Packet07ReceiveChat(p, split[1], message);
			case 12:
				return new Packet12ReceiveMovement(p, MovementType.valueOf(split[1]));
			case 17:
				return new Packet17ThrowProjectile(p, Double.parseDouble(split[1]), ItemID.lookupId(Integer.parseInt(split[2])));
			case 20:
				return new Packet20RequestInventory(p);
			case 22:
				return new Packet22RelocateItem(p, ItemID.lookupId(Integer.parseInt(split[1])), Integer.parseInt(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]));
			case 23:
				return new Packet23FireWeapon(p, Double.parseDouble(split[1]));
			case 24:
				return new Packet24ReceiveArmRotation(p, Double.parseDouble(split[1])); 
			case 26:
				return new Packet26SetWeapon(p, Integer.parseInt(split[1]));
			case 27:
				return new Packet27RequestSuicide(p);
			case 31:
				return new Packet31ReloadRequest(p);
			case 33:
				return new Packet33ReceiveServerCommand(p, split[1]);
			case 35:
				return new Packet35ReceiveCraftingRequest(p, Integer.parseInt(split[1]));
			case 38:
				return new Packet38RequestTeamInfo(p);
			case 40:
				return new Packet40HandshakeResponse(p, Long.parseLong(split[1]));
			case 45:
				return new Packet45ReceiveRemoteUIButtonPressEvent(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]), Integer.parseInt(split[5]));
			case 46:
				return new Packet46ReceiveRemoteUIKeyPressEvent(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]), Integer.parseInt(split[4]));
			case 48:
				return new Packet48ReceiveRemoteUIItemValue(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]), split[3]);
			case 52:
				return new Packet52PlaceBlock(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]), ItemID.lookupId(Integer.parseInt(split[3])));
			case 53:
				return new Packet53SetPlayerMode(p, Boolean.parseBoolean(split[1]));
			case 57:
				return new Packet57RequestBalance(p);
			case 59:
				return new Packet59ReceiveAbsolutePosition(p, Double.parseDouble(split[1]), Double.parseDouble(split[2]), Integer.parseInt(split[3]), Double.parseDouble(split[4]), Double.parseDouble(split[5]));
			case 62:
				return new Packet62ReceiveNextEventRequest(p);
			case 63:
				return new Packet63ReceiveEmoteRequest(p, EmoteType.lookupId(Integer.parseInt(split[1])));
			case 70:
				return new Packet70ReceiveSellRequest(p, Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			case 72:
				return new Packet72ReceivePurchaseItemRequest(p, ItemID.lookupId(Integer.parseInt(split[1])));
			default:
				Console.write("Packet: " + m);
				System.out.println("Unknown packet ID");
			}
		} catch (Exception e) {
			try {
				Console.error(p.getPlayer().getName() + " has sent an illegal packet of id " + id);
				p.getSender().sendPacket(new Packet16KickPlayer(p, "End of Stream"));
				e.printStackTrace();
				p.getSocket().close();
			} catch (IOException e1) {
				Console.error("Failed to kick player");
			}
		}
		return null;
	}
}
