package com.studio878.server.packets;

import com.studio878.server.app.Main;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Emote.EmoteType;

public class Packet63ReceiveEmoteRequest extends Packet {

	EmoteType type;
	public Packet63ReceiveEmoteRequest(NetPlayer p, EmoteType type) {
		super(p);
		this.type = type;
	}

	public String getDataString() {
		return "63:" + type.getId();
	}

	public void process() {
		if(Main.getDispatch().getClients().size() != 0) {
			Main.getDispatch().broadcastPacket(new Packet64SendEmoteUpdate(Main.getDispatch().getClients().get(0), type, p.getPlayer().getId()));
		}
	}

}
