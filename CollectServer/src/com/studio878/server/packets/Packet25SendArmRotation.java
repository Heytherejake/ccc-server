package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;


public class Packet25SendArmRotation extends Packet{
	double angle;
	int id;
	public Packet25SendArmRotation(NetPlayer p, int id, double angle) {
		super(p);
		this.angle = angle;
		this.id = id;
	}
	
	public String getDataString() {
		return "25:" + id + ":" + angle;
	}
	
	public void process() {
	}
}
