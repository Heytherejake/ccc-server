package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet66ForceLocationUpdate extends Packet {

	double x, y;
	
	public Packet66ForceLocationUpdate(NetPlayer p, double x, double y) {
		super(p);
		this.x = x;
		this.y = y;
	}

	public String getDataString() {
		try {
			int id = p.getPlayer().getId();
			return "66:" + id  + ":" + x + ":" + y;

		} catch (NullPointerException e) {
			return "66:-1:-99999:-99999";

		}
	}

	public void process() {
		
	}

}
