package com.studio878.server.packets;

import com.studio878.server.entities.Entity;
import com.studio878.server.io.NetPlayer;

public class Packet65UpdateEntityData extends Packet {

	Entity entity;
	
	public Packet65UpdateEntityData(NetPlayer p, Entity entity) {
		super(p);
		this.entity = entity;
	}

	public String getDataString() {
		return "65:" + entity.getId() + ":" + entity.getData();
	}

	public void process() {
		
	}

}
