package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet34SendServerCommandResponse extends Packet {

	String command;
	
	public Packet34SendServerCommandResponse(NetPlayer p, String s) {
		super(p);
		command = s;
	}
	public String getDataString() {
		return "34:" + command;
	}

	public void process() {
	}

}
