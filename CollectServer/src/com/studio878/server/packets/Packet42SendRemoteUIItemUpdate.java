package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;
import com.studio878.server.remoteui.RemoteUIItem;

public class Packet42SendRemoteUIItemUpdate extends Packet{

	RemoteUIItem item;
	
	public Packet42SendRemoteUIItemUpdate(NetPlayer p, RemoteUIItem item) {
		super(p);
		this.item = item;
	}

	public String getDataString() {
		return "42:" + item.getParent().getId() + ":" + item.getDataString();
	}

	public void process() {
		
	}

}
