package com.studio878.server.packets;

import com.studio878.server.entities.Player;
import com.studio878.server.io.NetPlayer;

public class Packet50SendPlayerScore extends Packet {

	Player player;
	
	public Packet50SendPlayerScore(NetPlayer p, Player player) {
		super(p);
		this.player = player;
	}

	public String getDataString() {
		return "50:" + player.getId() + ":" + player.getKills() + ":" + player.getDeaths();
	}

	public void process() {
		
	}

}
