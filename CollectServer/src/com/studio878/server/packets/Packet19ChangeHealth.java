package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Damage.DamageMethod;

public class Packet19ChangeHealth extends Packet{

	int id, health;
	DamageMethod source;
	String killer;
	public Packet19ChangeHealth(NetPlayer p, int id, int health, DamageMethod source, String killer) {
		super(p);
		this.id = id;
		this.health = health;
		this.source = source;
		this.killer = killer;
	}
	
	public String getDataString() {
		return "19:" + id + ":" + health + ":" + source.getId() + ":" + killer;
	}
	
	public void process() {
		
	}

}
