package com.studio878.server.packets;

import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;

public class Packet72ReceivePurchaseItemRequest extends Packet {

	ItemID item;
	
	public Packet72ReceivePurchaseItemRequest(NetPlayer p, ItemID item) {
		super(p);
		this.item = item;
	}

	public String getDataString() {
		return "72:" + item.getId();
	}

	public void process() {
		int cost = item.getBuyCost();
		if(p.getPlayer().hasEnoughMoney(cost)) {
			p.getPlayer().getInventory().add(item, 0, 1);
			p.getPlayer().subtractMoney(cost);
			p.getPlayer().sendInventoryUpdate();
		}
		
	}

}
