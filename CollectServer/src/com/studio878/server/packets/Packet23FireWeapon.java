package com.studio878.server.packets;

import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Player;
import com.studio878.server.events.WeaponFiredEvent;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.world.Grid;

public class Packet23FireWeapon extends Packet{

	double angle;

	public Packet23FireWeapon(NetPlayer p, double angle) {
		super(p);
		this.angle = angle;
	}

	public String getDataString() {
		return "23";
	}

	public void process() {
		if(!Hooks.call(Hook.WEAPON_FIRED, new WeaponFiredEvent(p.getPlayer(), p.getPlayer().getWeapon().getItem()))) {
			return;
		}

		double offset = p.getPlayer().calculateBonus(BonusItemType.RateOfFire) + p.getPlayer().getWeapon().getItem().getRateOfFireModifier();
		offset = 2 - offset;
		if(offset <= 0.1) {
			offset = 0.1;
		}
		Player ps = p.getPlayer();
		Block b = Grid.getClosestBlock((int) ps.getX(), (int) ps.getY() + ps.getHeight() - 16);
		if(b == null || b.getType() == BlockType.lookupName("Blue Barrier") || b.getType() == BlockType.lookupName("Red Barrier")) {
			if(ps.getWeapon().getItem().getType() != ItemID.lookupName("Shovel")) {
				return;
			}
		}
		if(System.currentTimeMillis() - p.getPlayer().getLastFireTime() >= p.getPlayer().getWeapon().getTimeOffset()*offset) {
			if(p.getPlayer().getInventory().containsMatchLevel(p.getPlayer().getWeapon().getItem().getType(), p.getPlayer().getWeapon().getItem().getLevel())) {
				p.getPlayer().setLastFireTime(System.currentTimeMillis());
				Item s = p.getPlayer().getWeapon().getItem();
				if(s.getType().getWeaponType() == 1) {
					if(s.getClip() > 0 && !s.isReloading) {
						s.useAmmo(1);
						p.getSender().sendPacket(new Packet21SendInventory(p));
					} else {
						return;
					}
				}
				p.getPlayer().getWeapon().fireBullet(angle, p.getPlayer().getX() + 7, p.getPlayer().getY() + 25);
				p.getDispatch().broadcastPacket(new Packet30WeaponFired(p, p.getPlayer().getX() + 7, p.getPlayer().getY() + 25, p.getPlayer().getWeapon().getItem()));

			}
		}

	}
}
