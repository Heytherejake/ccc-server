package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet08SendMessage extends Packet{
	
	String message;
	public Packet08SendMessage(NetPlayer p, String message) {
		super(p);
		this.message = message;
	}
	public String getDataString() {
		return "08:" + message;
	}

	public void process() {
		
	}

}
