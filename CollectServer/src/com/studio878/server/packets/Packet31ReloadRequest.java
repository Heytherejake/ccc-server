package com.studio878.server.packets;

import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;

public class Packet31ReloadRequest extends Packet{

	
	public Packet31ReloadRequest(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "31";
	}

	public void process() {
		Item id = p.getPlayer().getWeapon().getItem();
		if(id != null && id.getType().getWeaponType() == 1) {
			id.startReload();
		}
	
	}

}
