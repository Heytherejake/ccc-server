package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet28SendWeaponChange extends Packet{

	int id;
	int slot;
	
	public Packet28SendWeaponChange(NetPlayer p, int id, int slot) {
		super(p);
		this.id = id;
		this.slot = slot;
	}

	public String getDataString() {
		return "28:" + p.getPlayer().getId() + ":" + id + ":" + slot;
	}

	public void process() {
	}

}
