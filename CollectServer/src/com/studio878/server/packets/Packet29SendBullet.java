package com.studio878.server.packets;

import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;

public class Packet29SendBullet extends Packet{

	double x, y, angle;
	ItemID id;
	
	public Packet29SendBullet(NetPlayer p, double x, double y, double angle, ItemID id) {
		super(p);
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.id = id;
	}

	public String getDataString() {
		return "29:" + x + ":" + y + ":" + angle + ":" + id.getId();
	}

	public void process() {
		
	}

}
