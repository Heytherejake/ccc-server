package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIContainer;
import com.studio878.server.remoteui.RemoteUIItem;
import com.studio878.server.remoteui.RemoteUIItemValueChangedEvent;

public class Packet48ReceiveRemoteUIItemValue extends Packet {

	public int container, id;
	public String value;
	public Packet48ReceiveRemoteUIItemValue(NetPlayer p, int container, int id, String value) {
		super(p);
		this.container = container;
		this.id = id;
		this.value = value;
	}

	public String getDataString() {
		return "48:" + container + ":" + id + ":" + value;
	}

	public void process() {
		try {
			RemoteUIContainer c = p.getPlayer().getContainer(container);
			if(c != null) {
				RemoteUIItem i = c.getItem(id);
				if(i != null) {
					i.setValue(value);
					if(i.getEventManager() != null) {
						i.getEventManager().onItemValueChanged(new RemoteUIItemValueChangedEvent(p.getPlayer(), 0, 0, i));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
