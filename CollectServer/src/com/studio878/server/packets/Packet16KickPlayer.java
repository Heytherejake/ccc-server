package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet16KickPlayer extends Packet {

	private String message;
	
	public Packet16KickPlayer(NetPlayer p, String message) {
		super(p);
		this.message = message;
	}
	
	public String getDataString() {
		return "16:" + message;
	}
	
	public void process() {

	}

}
