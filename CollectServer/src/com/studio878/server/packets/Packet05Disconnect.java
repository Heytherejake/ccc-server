package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet05Disconnect extends Packet{

	public Packet05Disconnect(NetPlayer p) {
		super(p);
	}

	@Override
	public String getDataString() {
		return "05";
	}
	
	public void process() {
		getHandle().disconnectPlayer();

	}

}
