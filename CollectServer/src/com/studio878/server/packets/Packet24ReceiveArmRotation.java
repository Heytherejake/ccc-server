package com.studio878.server.packets;

import java.awt.geom.Line2D;

import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.weapons.MeleeWeapon;

public class Packet24ReceiveArmRotation extends Packet {

	double angle;

	public Packet24ReceiveArmRotation(NetPlayer p, double angle) {
		super(p);
		this.angle = angle;
	}

	public String getDataString() {
		return "24:"+angle;
	}

	public void process() {
		double da = (getHandle().getPlayer().getArmRotation() - angle)/(System.currentTimeMillis() - p.getPlayer().getLastRotationSetTime());
		getHandle().getPlayer().setArmRotation(angle);
		p.getPlayer().setLastRotationTime(System.currentTimeMillis());
		getHandle().getDispatch().broadcastPacket(new Packet25SendArmRotation(p, getHandle().getPlayer().getId(), angle));
		if(p.getPlayer().getWeapon() != null && p.getPlayer().getWeapon().getItem() != null && p.getPlayer().getWeapon().getItem().getType().getWeaponType() == 2 && Math.abs(da) > 0.01) {
			if(System.currentTimeMillis() - p.getPlayer().getLastFireTime() >= p.getPlayer().getWeapon().getItem().getReloadTime()) {
				//g.drawLine((float) Settings.ActivePlayer.getX() + 14,(float)  Settings.ActivePlayer.getY() + 31, (float) Settings.ActivePlayer.getX() + 14 +  (float) (-20*sin), (float)  Settings.ActivePlayer.getY() + 31 + (float) (20*cos));
				double angle = p.getPlayer().getArmRotation();
				double cos = Math.cos(angle), sin = Math.sin(angle);
				int width = ((MeleeWeapon) p.getPlayer().getWeapon()).getWidth();
				Line2D line = new Line2D.Double(p.getPlayer().getX() + 14, p.getPlayer().getY() + 31, p.getPlayer().getX() + 14 - width*sin, p.getPlayer().getY() + 31 + width*cos);
				for(HealthEntity e : EntityManager.getHealthEntities()) {
					if(e.getId() != p.getPlayer().getId() && e.getHitbox().intersectsLine(line)) {
						p.getPlayer().getWeapon().onHit(e);
						p.getPlayer().setLastFireTime(System.currentTimeMillis());
					}
				}
			}
		}
	}

}
