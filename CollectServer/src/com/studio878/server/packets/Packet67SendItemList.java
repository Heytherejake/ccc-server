package com.studio878.server.packets;

import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;

public class Packet67SendItemList extends Packet {

	public Packet67SendItemList(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		String msg = "";
		for(ItemID item : ItemID.getItemList().values()) {
			msg += item.getId() + "_!_" + item.getName() + "_!_";
			for(String s : item.getAlternativeNames()) {
				msg += s + "_%_";
			}
			msg = msg.substring(0, msg.length() - 3);
			msg += "_!_" + item.getWeaponType() + "_!_" + (item.getSource() != null ? item.getSource().getName() : "null") + "_!_" + item.getClientImagePoint().x + "_!_" + item.getClientImagePoint().y + "_!_" + item.getSellCost() + "_!_" + item.getDescription() + "_!_" + item.getBuyCost();
			msg += "~";
		}
		return "67:" + msg.substring(0, msg.length() - 1);
	}

	public void process() {
		
	}

}
