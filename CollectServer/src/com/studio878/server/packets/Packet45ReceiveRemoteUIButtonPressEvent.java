package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.remoteui.RemoteUIClickEvent;
import com.studio878.server.remoteui.RemoteUIContainer;
import com.studio878.server.remoteui.RemoteUIEventManager;
import com.studio878.server.remoteui.RemoteUIItem;
import com.studio878.server.remoteui.RemoteUIManager;
import com.studio878.server.remoteui.RemoteUIMouseReleaseEvent;

public class Packet45ReceiveRemoteUIButtonPressEvent extends Packet {

	int container, item, button, x, y, b;

	public Packet45ReceiveRemoteUIButtonPressEvent(NetPlayer p, int c, int i, int x, int y, int b) {
		super(p);
		container = c;
		item = i;
		button = b;
		this.x = x;
		this.y = y;
	}

	public String getDataString() {
		return "45:" + container + ":" + item + ":" + x + ":" + y + ":" + b;
	}

	public void process() {
		RemoteUIContainer c = p.getPlayer().getContainer(container);
		if(c != null) {
			RemoteUIItem i = c.getItem(item);
			if(i != null) {
				RemoteUIEventManager em = i.getEventManager();
				if(em != null) {
					if(b != -1) {
						em.onClick(new RemoteUIClickEvent(p.getPlayer(), c, x, y, b));
					} else {
						em.onMouseRelease(new RemoteUIMouseReleaseEvent(p.getPlayer(), c, x, y));
					}
				}
			}
		}
	}

}
