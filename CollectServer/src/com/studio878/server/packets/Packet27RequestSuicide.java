package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Damage.DamageMethod;

public class Packet27RequestSuicide extends Packet{

	public Packet27RequestSuicide(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		return "27";
	}

	public void process() {
		p.getPlayer().damage(1000, DamageMethod.Suicide, p.getPlayer().getName());
	}

}
