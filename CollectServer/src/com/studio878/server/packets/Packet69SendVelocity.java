package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet69SendVelocity extends Packet {

	int id;
	double dx, dy;
	
	public Packet69SendVelocity(NetPlayer p, int id, double dx, double dy) {
		super(p);
		this.id = id;
		this.dx = dx;
		this.dy = dy;
	}

	public String getDataString() {
		return "69:" + id + ":" + dx + ":" + dy;
	}

	public void process() {
		
	}

}
