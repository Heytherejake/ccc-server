package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Settings;

public class Packet59ReceiveAbsolutePosition extends Packet {

	double x, y, dx, dy;
	int facing;
	
	public Packet59ReceiveAbsolutePosition(NetPlayer p, double x, double y, int facing, double dx, double dy) {
		super(p);
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.facing = facing;
	}

	public String getDataString() {
		return "59:" + x + ":" + y + ":" + facing + ":" + dx + ":" + dy;
	}

	public void process() {
		if(Settings.ClientSideMovement && !p.getPlayer().isDead()) {
			p.getPlayer().moveTo(x, y);
			p.getPlayer().setFacingDirection(facing);
			p.getPlayer().setXSpeed(dx);
			p.getPlayer().setYSpeed(dy);
		}
	}


}
