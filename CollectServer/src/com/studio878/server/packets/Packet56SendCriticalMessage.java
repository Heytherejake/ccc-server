package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet56SendCriticalMessage extends Packet {

	String message;
	
	public Packet56SendCriticalMessage(NetPlayer p, String message) {
		super(p);
		this.message = message;
	}

	public String getDataString() {
		return "56:" + message;
	}

	public void process() {
		
	}

}
