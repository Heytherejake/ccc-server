package com.studio878.server.packets;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;

import com.studio878.server.app.Main;
import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.crafting.BonusItem.BonusItemType;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.FallingBlock;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.world.Grid;

public class Packet52PlaceBlock extends Packet {

	int x, y;
	ItemID type;

	public Packet52PlaceBlock(NetPlayer p, int x, int y, ItemID type) {
		super(p);
		this.x = x;
		this.y = y;
		this.type = type;
	}

	public String getDataString() {
		return "52:" + x + ":" + y + ":" + type;
	}

	public void process() {
		Block bv = Grid.getBlockAt(x, y);
		if(!type.isBlock()) return;
		if(p.getPlayer().getInventory().contains(type)) {
			double px = getHandle().getPlayer().getX();
			double py = getHandle().getPlayer().getY();		
			if(x*16 >= px - 100 && x*16 <= px + 100 && y*16 >= py - 100 && y*16 <= py + 100) {
				int amt =  (int) getHandle().getPlayer().calculateBonus(BonusItemType.BuildRadius);
				ArrayList<Block> blocks = Grid.getBlockGroup(bv, amt, amt, 1);
				Collections.reverse(blocks);
				for(Block b : blocks) {
					if(!p.getPlayer().getInventory().contains(type)) continue;
					if(b == null || b.getType() != BlockType.lookupName("Air")) continue;
					Rectangle rs = new Rectangle(x*16, y*16, 16, 16);

					boolean canPlace = false;
					for(Block bd : b.getSurroundingBlocks()) {
						if(bd != null && !bd.getType().getPermiability(null)) {
							canPlace = true;
						}
					}
					if(b.getBackgroundType() != BlockType.lookupName("Air")) {
						canPlace = true;
					}
					for(Entity e: EntityManager.getEntities()) {
						if(e instanceof FallingBlock) continue;
						Rectangle r = new Rectangle((int) e.getX(), (int) e.getY(), e.getWidth(), e.getHeight());
						if(r.intersects(rs)) {
							canPlace = false;
						}
					}
					if(!canPlace) continue;
					b.setType(type.getSource());
					b.setCondition((byte) 10);
					if(type.getSource().isFluid()) {
						b.setData((byte) 16);
					}
					Block bsd = b.getBlockAdjacent(0, 1);
					if(b.getType().isPhysicsEnabled() && bsd != null && bsd.getType() == BlockType.lookupName("Air")) {
						Grid.forceFall(b);
					}
					p.getPlayer().getInventory().subtract(type, 0, 1);

				}
				if(Main.getDispatch().getClients().size() != 0) {
					Main.getDispatch().broadcastPacket(new Packet54PlayPlaceSound(Main.getDispatch().getClients().get(0), bv));
				}
				p.getSender().sendPacket(new Packet21SendInventory(p));
			}
		}	
	}
}
