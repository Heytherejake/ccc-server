package com.studio878.server.packets;

import com.studio878.server.block.Block;
import com.studio878.server.block.BlockType;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Grenade;
import com.studio878.server.entities.Landmine;
import com.studio878.server.entities.PhysicsAffectedEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.events.ProjectileThrownEvent;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.world.Grid;

public class Packet17ThrowProjectile extends Packet {

	double angle;
	ItemID item;
	public Packet17ThrowProjectile(NetPlayer p, double angle, ItemID id) {
		super(p);
		this.angle = angle + Math.toRadians(90);
		this.item = id;

	}

	public String getDataString() {
		return "17:" + angle + ":" + item;
	}

	public void process() {
		PhysicsAffectedEntity e;
		if(item.getName().equals("Grenade")) {
			e = new Grenade(p.getPlayer().getX(), p.getPlayer().getY(), p.getPlayer().getName());
		} else if(item.getName().equals("Landmine")) {
			e = new Landmine(p.getPlayer().getX(), p.getPlayer().getY(), p.getPlayer().getName());
		} else {
			return;
		}
		if(p.getPlayer().getInventory().contains(item)) {
			Player ps = p.getPlayer();
			Block b = Grid.getClosestBlock((int) ps.getX(), (int) ps.getY() + ps.getHeight() - 16);
			if(b != null && (b.getType() == BlockType.lookupName("Blue Barrier") || b.getType() == BlockType.lookupName("Red Barrier"))) {
				return;
			}
			if(!Hooks.call(Hook.PROJECTILE_THROWN, new ProjectileThrownEvent(p.getPlayer(), item))) {
				return;
			}
			p.getPlayer().getInventory().subtract(item, 0, 1);
			EntityManager.registerEntity(e);
			e.setVelocity(4*Math.cos(angle), 7*Math.sin(angle));
			getHandle().getDispatch().broadcastPacket(new Packet15AddEntity(getHandle(), e));
			getHandle().getSender().sendPacket(new Packet21SendInventory(p));
		}

	}

}
