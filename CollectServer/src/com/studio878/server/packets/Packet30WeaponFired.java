package com.studio878.server.packets;

import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;

public class Packet30WeaponFired extends Packet{

	double x, y;
	Item id;
	
	public Packet30WeaponFired(NetPlayer p, double x, double y, Item id) {
		super(p);
		this.x = x;
		this.y = y;
		this.id = id;
	}

	public String getDataString() {
		return "30:" + x + ":" + y + ":" + id.getType().getId();
	}

	public void process() {
		
	}

}
