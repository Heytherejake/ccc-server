package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet53SetPlayerMode extends Packet {

	boolean mode;
	
	public Packet53SetPlayerMode(NetPlayer p, boolean mode) {
		super(p);
		this.mode = mode;
	}

	public String getDataString() {
		return "53:" + mode;
	}

	public void process() {
		p.getPlayer().setPlacementMode(mode);
		
	}

}
