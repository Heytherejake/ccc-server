package com.studio878.server.packets;

import java.util.ArrayList;

import com.studio878.server.block.Block;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.logging.Console;
import com.studio878.server.thread.WorldBuilderThread;
import com.studio878.server.util.Settings;
import com.studio878.server.world.Grid;

public class Packet04SendGrid extends Packet{

	public StringBuilder[] threads;
	public boolean[] finished;
	public Packet04SendGrid(NetPlayer p) {
		super(p);
		Block[][] blocks = Grid.getGrid();
		finished = new boolean[Settings.WorldBuildThreads];
		threads = new StringBuilder[Settings.WorldBuildThreads];
	}

	public String getDataString() {
		Block[][] blocks = Grid.getGrid();
		StringBuilder msg = new StringBuilder(blocks.length*blocks[0].length*40);
		msg.append("04:");
		long start = System.currentTimeMillis();
		for(int i = 0; i < Settings.WorldBuildThreads; i++) {
			new WorldBuilderThread(i, this).start();
		}
		boolean canContinue = false;
		while(!canContinue) {
			boolean cs = true;
			for(boolean f : finished) {
				if(!f) cs = false;
			}
			canContinue = cs;
		}
		for(int i = 0; i < Settings.WorldBuildThreads; i++) {
			msg.append(threads[i]);
		}

		Console.log("Map built in " + (System.currentTimeMillis() - start) + "ms");
		return msg.substring(0, msg.length() - 1);
	}

	public void process() {
	}

}
