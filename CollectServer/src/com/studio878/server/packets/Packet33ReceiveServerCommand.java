package com.studio878.server.packets;

import com.studio878.server.input.CommandParser;
import com.studio878.server.input.CommandParser.CommandSource;
import com.studio878.server.io.NetPlayer;

public class Packet33ReceiveServerCommand extends Packet {

	String command;
	
	public Packet33ReceiveServerCommand(NetPlayer p, String s) {
		super(p);
		command = s;
	}
	public String getDataString() {
		return "33:" + command;
	}

	public void process() {
		if(getHandle().getPlayer().isOp()) {
		CommandParser.processCommand(p, command, CommandSource.Client);
		} else {
			getHandle().getSender().sendPacket(new Packet34SendServerCommandResponse(p, " Error - You are not an operator of this server"));
		}
	}

}
