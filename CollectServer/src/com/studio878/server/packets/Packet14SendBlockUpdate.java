package com.studio878.server.packets;

import com.studio878.server.block.Block;
import com.studio878.server.io.NetPlayer;

public class Packet14SendBlockUpdate extends Packet {
	Block block;
	
	public Packet14SendBlockUpdate(NetPlayer p, Block b) {
		super(p);
		block = b;
	}

	public String getDataString() {

		return "14:" + block.getX() + ":" + block.getY() + ":" + block.getType().getId() + ":" + block.getData() + ":" + block.getCondition() + ":" + block.getOffset() + ":" + block.getWireId() + ":" + block.getWireData();
	}

	public void process() {
	}

}
