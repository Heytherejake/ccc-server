package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;

public class Packet40HandshakeResponse extends Packet{

	long time;

	public Packet40HandshakeResponse(NetPlayer p, long time) {
		super(p);
		this.time = time;
	}

	public String getDataString() {
		return "40:" + time;
	}

	public void process() {
		if(p.getPlayer() != null) {
			p.getPlayer().setLatency(System.currentTimeMillis() - time);
		}
	}

}
