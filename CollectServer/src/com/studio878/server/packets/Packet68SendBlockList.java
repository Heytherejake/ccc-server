package com.studio878.server.packets;

import com.studio878.server.block.BlockType;
import com.studio878.server.io.NetPlayer;

public class Packet68SendBlockList extends Packet {

	public Packet68SendBlockList(NetPlayer p) {
		super(p);
	}

	public String getDataString() {
		String msg = "";
		for(BlockType block : BlockType.getBlockList().values()) {
			msg += block.getId() + "_!_" + block.getName() + "_!_" + block.isPhysicsEnabled() + "_!_" + block.getDurability() + "_!_" + block.getPermiability(null) + "_!_" + block.getSoundId() + "_!_" + block.getPosition() + "_!_" + block.isTransparent() + "_!_" + block.isRounded() + "_!_" + block.isFluid() + "_!_" + block.isElectric() + "_!_" + block.getLuminocity() + "_!_" + (block.getDrop() != null ? block.getDrop().getName() : "null") + "~";
		}
		return "68:" + msg.substring(0, msg.length() - 1);
	}

	public void process() {
		
	}

}
