package com.studio878.server.packets;

import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.crafting.CraftingRecipeList;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.ItemEntity;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;

public class Packet22RelocateItem extends Packet{

	ItemID type;
	int slot, initialSlot, amount;
	public Packet22RelocateItem(NetPlayer p, ItemID type, int slot, int islot , int amount) {
		super(p);
		this.type = type;
		this.slot = slot;
		initialSlot = islot;
		this.amount = amount;
	}

	public String getDataString() {
		return "22:" + type.getId() + ":" + slot + ":" + initialSlot + ":" + amount;
	}

	public void process() {
		if(p.getPlayer().getInventory().containsSlot(type, initialSlot)) {
			if(slot != -1) {
				Item is = p.getPlayer().getInventory().getItemFromSlot(slot);
				if(is == null || is.getAmount() == 0) {
					p.getPlayer().getInventory().getItemFromSlot(type, initialSlot).setSlot(slot);
					new Packet20RequestInventory(p).process();
				}
			} else {
				int fd = 1;
				if(p.getPlayer().getFacingDirection() == -1 || p.getPlayer().getFacingDirection() == 2) {
					fd *= -1;
				}
				String data = p.getPlayer().getInventory().getItem(type).getData();
				Item is = p.getPlayer().getInventory().getItemFromSlot(initialSlot);
				p.getPlayer().getInventory().removeItemFromSlot(initialSlot);
				if(type.getWeaponType() == 1) {
					CraftingRecipe r = CraftingRecipeList.lookupRecipe(type);
					if(r != null && p.getPlayer().hasCrafted(r)) {
						p.getPlayer().removeCrafted(r);
						data += "~" + p.getPlayer().getName();
					}
				}
				if(amount <= type.getStackSize()) {
					for(int i = 0; i < amount; i++) {
						ItemEntity es = new ItemEntity(p.getPlayer().getX(), p.getPlayer().getY(), is);
						if(type.getWeaponType() == 1) {
							es.setData("" + type.getId() + "," + is.getLevel() + ";" + data);
						}
						es.setVelocity(fd*(Math.random()*1 + 3), -2 - Math.random());
						EntityManager.registerEntity(es);
						getHandle().getDispatch().broadcastPacket(new Packet15AddEntity(getHandle(), es));
					}
				}


			}
		}
	}

}
