package com.studio878.server.packets;

import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Movement.MovementType;

public class Packet13SendMovement extends Packet {

	double x, y;
	int id;
	int facing;
	double rotation;
	
	public Packet13SendMovement(NetPlayer p, double x, double y, int id, int t, double rotation) {
		super(p);
		this.x = x;
		this.y = y;
		this.id = id;
		facing = t;
		this.rotation = rotation;
	}

	public String getDataString() {
		return "13:" + x + ":" + y + ":" + id + ":" + facing + ":" + rotation;
	}

	public void process() {
		
	}

}
