package com.studio878.server.packets;

import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.util.Settings;

public class Packet10SendEntityList extends Packet{

	
	public Packet10SendEntityList(NetPlayer p) {
		super(p);

	}
	public String getDataString() {
		StringBuilder msg = new StringBuilder();
		msg.append("10");
		for(Entity e : EntityManager.getEntities()) {
			msg.append(":" + e.getId() + "," + e.getType().getId() + "," + e.getX() + "," + e.getY() + "," + e.getData());
		}

		return msg.toString();
	}
	
	public void process() {

		
	}

}
