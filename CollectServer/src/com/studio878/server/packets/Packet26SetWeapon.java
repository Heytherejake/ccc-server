package com.studio878.server.packets;

import com.studio878.server.inventory.Item;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.logging.Console;
import com.studio878.server.weapons.EmptyHand;
import com.studio878.server.weapons.Weapon;


public class Packet26SetWeapon extends Packet{
	int slot;
	int level;

	public Packet26SetWeapon(NetPlayer p, int slot) {
		super(p);
		this.slot = slot;
	}

	public String getDataString() {
		return "26:" + slot;
	}

	public void process() {
		Item i = p.getPlayer().getInventory().getItemFromSlot(slot);
		if(i != null) {
			Weapon w = Weapon.matchWeapon(p.getPlayer(), i);
			if(w != null) {
				p.getPlayer().setWeapon(w, slot);
				return;
			}
		} else {
			p.getPlayer().setWeapon(new EmptyHand(p.getPlayer()), -1);
			return;
		}
		Console.error("Invalid weapon");
	}
}
