package com.studio878.server.input;

import com.studio878.server.inventory.Item;

public class ReloadThread implements Runnable{


	Item s;
	public ReloadThread(Item s) {
		this.s = s;
	}
	
	public void run() {
		s.reload();
	}

}
