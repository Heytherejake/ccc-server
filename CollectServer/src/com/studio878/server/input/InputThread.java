package com.studio878.server.input;

import com.studio878.server.input.CommandParser.CommandSource;


public class InputThread extends Thread {
	public void run() {
		try {
			while(!isInterrupted()) {
				String input = System.console().readLine();
				if(input != null) {
					CommandParser.processCommand(null, input, CommandSource.Server);
				}
				
			}
		} catch (Exception e) {

		}

	}
}
