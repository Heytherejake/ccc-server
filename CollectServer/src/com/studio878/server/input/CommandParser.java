package com.studio878.server.input;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.TimerTask;

import com.studio878.server.app.Main;
import com.studio878.server.app.Server;
import com.studio878.server.block.Block;
import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;
import com.studio878.server.events.ServerCommandEvent;
import com.studio878.server.inventory.Item;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.Hooks;
import com.studio878.server.listeners.Hooks.Hook;
import com.studio878.server.listeners.Plugin;
import com.studio878.server.listeners.PluginDispatch;
import com.studio878.server.logging.Console;
import com.studio878.server.multiplayer.MultiplayerAbility;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.packets.Packet15AddEntity;
import com.studio878.server.packets.Packet34SendServerCommandResponse;
import com.studio878.server.packets.Packet51RoundOver;
import com.studio878.server.util.ChatColors;
import com.studio878.server.util.Damage.DamageMethod;
import com.studio878.server.util.Settings;
import com.studio878.server.util.TeamManager;
import com.studio878.server.util.TeamManager.Team;
import com.studio878.server.world.Grid;

public class CommandParser {
	public enum CommandSource {
		Client,
		Server;
	}
	public static String previousCommand = "";
	public static String newVersion = "";
	public static String newBuild = "";

	public static void processCommand(NetPlayer p, String input, CommandSource s) {
		try {
			if(input.length() == 0) {
				return;
			}
			String[] split = input.split(" ");
			String command = split[0];		
			String[] parts = new String[split.length - 1];
			for(int i = 1; i < split.length; i++) {
				parts[i-1] = split[i];
			}
			if(!Hooks.call(Hook.SERVER_COMMAND, new ServerCommandEvent(split[0], parts))) {
				return;
			}

			if(previousCommand.equalsIgnoreCase("update")) {
				if(input.equalsIgnoreCase("n") || input.equalsIgnoreCase("no")) {
					sendMessageToSource(p, "Update aborted.", s);
					previousCommand = "";
					return;
				} else if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")) {
					sendMessageToSource(p, "Updating server to version " + newVersion + " (Build " + newBuild + ").", s);
					try {
						URL url = new URL("http://dl.studio878software.com/ccc_server.jar");
						ReadableByteChannel rbc = Channels.newChannel(url.openStream());
						FileOutputStream fos = new FileOutputStream("ccc_server.jar");
						fos.getChannel().transferFrom(rbc, 0, 1 << 24);
						sendMessageToSource(p, "Download complete. Server is shutting down.", s);
						Main.shutdown();
					} catch (Exception e) {
						sendMessageToSource(p, "Failed to download ccc_server.jar.", s);
					}
				} else {
					previousCommand = "update";
					sendMessageToSource(p, "Update to server version " + newVersion + " (Build " + newBuild + ") (Y/N)?", s);
					return;
				}
			}
			if(command.equalsIgnoreCase("stop")) {
				Main.shutdown();
			} else if (command.equalsIgnoreCase("who")) {
				ArrayList<NetPlayer> players = Main.getDispatch().getClients();
				String msg = "Players online (" + players.size() + "): ";
				for(int i = 0; i < players.size(); i++) {
					msg += players.get(i).getPlayer().getName() + ", ";
				}
				if(players.size() > 0) {
					sendMessageToSource(p, msg.substring(0, msg.length() - 2), s);
				} else {
					sendMessageToSource(p, "No players are connected.", s);
				}
			} else if (command.equalsIgnoreCase("say")) {
				String msg = ChatColors.Red + "[Server] " + ChatColors.White;
				String psm = "";
				for(int i = 1; i < split.length; i++) {
					msg += split[i] + " ";
					psm += split[i] + " ";
				}
				sendMessageToSource(p, "[Server] " + psm, s);
				Main.getDispatch().broadcastPacket(new Packet08SendMessage(new NetPlayer(), msg));
			} else if (command.equalsIgnoreCase("enableplugin")) {
				if(split.length == 2) {
					if(PluginDispatch.loadPlugin(split[1] + ".jar")) {
						sendMessageToSource(p, "Plugin loaded.", s);
					} else {
						sendMessageToSource(p, split[1] + " is already registered, or crashed on load.", s);
					}
				} else {
					sendMessageToSource(p, "Invalid argument.", s);
				}
			} else if (command.equalsIgnoreCase("disableplugin")) {
				if(split.length == 2) {
					if(PluginDispatch.offloadPlugin(split[1])) {
						sendMessageToSource(p, "Plugin disabled.", s);
					} else {
						sendMessageToSource(p, "Plugin does not exist.", s);
					}
				} else {
					sendMessageToSource(p, "Invalid argument.", s);
				}
			} else if (command.equalsIgnoreCase("op")) {
				if(split.length >= 2) {
					if(split[1].equalsIgnoreCase("add") && split.length == 3) {
						Server.ops.add(split[2]);
						Server.saveOps();
						sendMessageToSource(p, split[2] + " has been opped.", s);
					} else if(split[1].equalsIgnoreCase("remove") && split.length == 3) {
						String res = "";
						for(String sd : Server.ops) {
							if(sd.equalsIgnoreCase(split[2])) {
								Server.ops.remove(sd);
								res = sd;
							}
						}
						Server.saveOps();
						sendMessageToSource(p, res + " has been deopped.", s);
					} else {
						sendMessageToSource(p, "Invalid command", s);
					}
				} else {
					String msg = "Operators: ";
					for(String sd : Server.ops) {
						msg += sd + ", ";
					}
					msg = msg.substring(0, msg.length() - 2);
					sendMessageToSource(p, msg, s);
				}
			} else if (command.equalsIgnoreCase("update")) {
				try {
					sendMessageToSource(p, "Retrieving version information from studio878software.com", s);
					URL url = new URL("http://dl.studio878software.com/server.version");
					BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
					newVersion = in.readLine();
					newBuild = in.readLine();
				} catch (Exception e) {
					sendMessageToSource(p, "Failed to connect to studio878software.com", s);
					return;
				}
				if(!newVersion.equals(Settings.Version) || !newBuild.equals(Settings.Build)) {
					sendMessageToSource(p, "Update to server version " + newVersion + " (Build " + newBuild + ") (Y/N)?", s);
					previousCommand = "update";
					return;
				} else {
					sendMessageToSource(p, "Server is up to date.", s);
				}

			} else if (command.equalsIgnoreCase("reloadplugin")) {
				if(split.length == 2) {
					if(PluginDispatch.reloadPlugin(split[1])) {
						sendMessageToSource(p, "Plugin reloaded.", s);
					} else {
						sendMessageToSource(p, "Plugin does not exist.", s);
					}
				} else {
					sendMessageToSource(p, "Invalid argument", s);
				}
			} else if (command.equalsIgnoreCase("listplugins")) {
				ArrayList<Plugin> plugins = PluginDispatch.getListeners();
				if(plugins.size() == 0) {
					sendMessageToSource(p, "No plugins registered", s);
				}
				String msg = "Enabled plugins: ";
				for(int i = 0; i < plugins.size(); i++) {
					msg += plugins.get(i).getPluginInfo().getPluginName() + ", ";
				}
				sendMessageToSource(p, msg.substring(0, msg.length() - 2), s);
			} else if (command.equalsIgnoreCase("slap")) {
				Player pd = Main.getServer().matchPlayer(split[1]);

				if(pd != null) {
					NetPlayer toSlap = pd.getHandle();
					if(split.length > 2) {
						toSlap.getPlayer().setVelocity(-5, -3);
					} else {
						toSlap.getPlayer().setVelocity(5, -3);

					}
					sendMessageToSource(p, "[Slap] " + toSlap.getPlayer().getName() + " has been slapped.", s);

				}
			} else if (command.equalsIgnoreCase("newround") && split.length == 2) {
				Team t = TeamManager.matchTeam(split[1]);
				if(t != null) {
					sendMessageToSource(p, "Restarting round...", s);
					if(Main.getDispatch().getClients().size() != 0) {
						Main.getDispatch().broadcastPacket(new Packet51RoundOver(Main.getDispatch().getClients().get(0), t, Settings.NewRoundTime));
					}
					Main.registerTimerTask(new TimerTask() {
						public void run() {
							Server.startNewRound();
						}
					}, Settings.NewRoundTime*1000);
				} else {
					sendMessageToSource(p, "Invalid team.", s);
				}
			} else if (command.equalsIgnoreCase("gethealth") && split.length == 2) {
				try {
					int id = Integer.parseInt(split[1]);
					Entity e = EntityManager.getEntity(id);
					if(e != null && e instanceof HealthEntity) {
						HealthEntity h = (HealthEntity) e;
						sendMessageToSource(p, "Entity " + id + "'s health is " + h.getHealth(), s);
					} else {
						sendMessageToSource(p, "Invalid entity", s);

					}
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid values", s);
				}
			} else if (command.equalsIgnoreCase("tp") && split.length == 4) {
				try {
					String player = split[1];
					int x = Integer.parseInt(split[2]);
					int y = Integer.parseInt(split[3]);
					Player ps = Main.getServer().matchPlayer(player);
					if(ps != null) {
						ps.moveTo(new Point(x, y));
						sendMessageToSource(p, ps.getName() + " has been teleported to (" + x + "," + y + ")", s);
					}
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid values", s);
				}
			} else if (command.equalsIgnoreCase("sethealth") && split.length == 3) {
				try {
					int id = Integer.parseInt(split[1]);
					int value = Integer.parseInt(split[2]);
					Entity e = EntityManager.getEntity(id);
					if(e != null && e instanceof HealthEntity) {
						HealthEntity h = (HealthEntity) e;
						int hp = h.getHealth();
						String source = "Server";
						if(p != null) {
							source = p.getPlayer().getName();
						}
						h.damage(hp - value, DamageMethod.Generic, source);
						sendMessageToSource(p, "Health of " + id + " changed to " + value, s);
					} else {
						sendMessageToSource(p, "Invalid entity", s);

					}
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid values", s);

				}
			} else if (command.equalsIgnoreCase("entity") && split.length == 2) {
				try {
					Entity e = EntityManager.getEntity(Integer.parseInt(split[1]));
					sendMessageToSource(p, "- - Entity Status - -", s);
					sendMessageToSource(p, "    Type: " + e.getType().name(), s);
					sendMessageToSource(p, "    Location: (" + e.getX() + ", " + e.getY() + ")", s);
					sendMessageToSource(p, "    Name: " + e.getName(), s);
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid values", s);
				}
			} else if (command.equalsIgnoreCase("binfo") && split.length == 3) {
				int x;
				int y;
				try {
					x = Integer.parseInt(split[1]);
					y = Integer.parseInt(split[2]);
				} catch (NumberFormatException e) {
					sendMessageToSource(p, "Invalid coordinates.", s);
					return;
				}
				Block b = Grid.getBlockAt(x, y);
				sendMessageToSource(p, "Information for block at (" + x +", " + y + "): |T| " + b.getType() + " |BGT| " + b.getBackgroundType() + " |C| " + b.getCondition() + " |D| " + b.getData(), s);
			} else if (command.equalsIgnoreCase("give") && split.length > 2) {
				Player pd = Main.getServer().matchPlayer(split[1]);
				if(pd != null) {
					ItemID item; 
					try {
						item = ItemID.lookupId(Integer.parseInt(split[2]));
					} catch (Exception e) {
						item =  ItemID.lookupName(split[2]);
					}
					if(item != null) {
						int amt = 0;
						if(split.length == 4) {
							try {
								amt = Integer.parseInt(split[3]);
							} catch (NumberFormatException e) {
								sendMessageToSource(p, "Unable to give: Invalid amount", s);
								return;
							}
						} else {
							amt = 1;
						}
						pd.getInventory().add(item, 0, amt);
						sendMessageToSource(p, amt + " units of " + item.toString() + " was given to " + pd.getName(), s);
						pd.sendMessage(ChatColors.Green + "You were given " + ChatColors.White + amt + ChatColors.Green + " units of " + ChatColors.White + item.toString() + ChatColors.Green + " by the server.");
						pd.sendInventoryUpdate();
						return;
					} else {
						sendMessageToSource(p, "Unable to give: Invalid item name", s);
						return;
					}
				} else {
					sendMessageToSource(p, "Unable to give: Player \""+split[1]+"\" not found.", s);
					return;
				}
			} else if (command.equalsIgnoreCase("status")) {
				sendMessageToSource(p, "- - Server Status - -", s);
				sendMessageToSource(p, "    Version: " + Settings.Version + " (Build " + Settings.Build + ")", s);
				try {
					URL url = new URL("http://dl.studio878software.com/server.version");
					BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
					newVersion = in.readLine();
					newBuild = in.readLine();
				} catch (Exception e) {
					sendMessageToSource(p, "Failed to connect to studio878software.com", s);
					return;
				}
				String nl = "    Update Available: ";
				if(newVersion.equals(Settings.Version) && newBuild.equals(Settings.Build)) {
					nl += "No";
				} else {
					nl += "Yes (" + newVersion + " Build " + newBuild + ")";
				}
				sendMessageToSource(p, nl, s);
				sendMessageToSource(p, "    Players Online: " + Main.getServer().getPlayers().size() + "/" + Settings.MaxClients, s);
				long time = System.currentTimeMillis() - Settings.LaunchTime;
				int seconds = (int) ((double) time/ (double) 1000);
				int minutes = (int) Math.floor(((double) seconds / (double) 60));
				int hours = (int) Math.floor(((double) minutes / (double) 60));
				int rem = (seconds - minutes*60)%60;
				String rst = "" + rem;
				if(rem < 10) {
					rst = "0" + rst;
				}
				String min = "" + minutes;
				if(minutes < 10) {
					min = "0" + min;
				}
				sendMessageToSource(p, "    Uptime: " + hours + ":" + min + ":" + rst, s);
			} else if(command.equalsIgnoreCase("player") && split.length == 2) {
				try {
					Player pv = Main.getServer().matchPlayer(split[1]);
					sendMessageToSource(p, "- - Player Multiplayer Status - -", s);
					sendMessageToSource(p, "    Name: " + pv.getName(), s);
					sendMessageToSource(p, "    Session ID: " + pv.getSessionID(), s);
					sendMessageToSource(p, "    Character: " + pv.getCharacter().getFullName(), s);
					sendMessageToSource(p, "    Abilities:", s);
					for(MultiplayerAbility i : pv.multiplayerAbilities) {
						long seconds = i.getTimeRemaining();
						int minutes = (int) Math.floor(((double) seconds / (double) 60));
						int hours = (int) Math.floor(((double) minutes / (double) 60));
						int rem = (int) ((seconds - minutes*60)%60);
						String rst = "" + rem;
						if(rem < 10) {
							rst = "0" + rst;
						}
						String min = "" + minutes;
						if(minutes < 10) {
							min = "0" + min;
						}
						sendMessageToSource(p, "        " + i.getType() + ":" + i.getMultiplier() + " (" + hours + ":" + min + ":" + rst + ")" , s);
					}
					sendMessageToSource(p, "    Loadout:", s);
					for(Item i : pv.Loadout) {
						sendMessageToSource(p, "        " + i.getType() + " Level " + i.getLevel() + " (x" + i.getAmount() + ")", s);
					}
					
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid player", s);
				}
			} else if (command.equalsIgnoreCase("spawn") && split.length == 2) {
				try {
					EntityName en = EntityName.lookupId(Integer.parseInt(split[1]));
					if(s == CommandSource.Client) {
						Entity e = EntityManager.cloneEntity(en, (int) p.getPlayer().getX(), (int) p.getPlayer().getY(), "");
						EntityManager.registerEntity(e);
						if(Main.getDispatch().getClients().size() > 0) {
							Main.getDispatch().broadcastPacket(new Packet15AddEntity(Main.getDispatch().getClients().get(0), e));
						}
					}
				} catch (Exception e) {
					sendMessageToSource(p, "Invalid NPC", s);
				}
			} else if (command.equalsIgnoreCase("kick") && split.length >= 2) {
				Player toKick = Main.getServer().matchPlayer(split[1]);
				if(toKick != null) {
					String msg = "";
					if(split.length >= 3) {
						for(int i = 2; i < split.length; i++) {
							msg += split[i] + " ";
						}
					} else {
						msg = "You have been kicked from the server.";
					}
					toKick.kick(msg);
					sendMessageToSource(p, "You have kicked " + toKick.getName() + " (Reason: " + msg + ")", s);
				} else {
					sendMessageToSource(p, "[Kick] Player is not on the server", s);
				}
			} else if (command.equalsIgnoreCase("setteam") && split.length == 3) {
				Player ps = Main.getServer().matchPlayer(split[1]);
				if(ps != null) {
					String sd = split[2];
					if(sd.equalsIgnoreCase("red")) {
						ps.setTeam(Team.Red);
						sendMessageToSource(p, ps.getName() + " has been switched to the Red team.", s);
					} else if (sd.equalsIgnoreCase("blue")) {
						ps.setTeam(Team.Blue);
						sendMessageToSource(p, ps.getName() + " has been switched to the Blue team.", s);
					} else {
						sendMessageToSource(p, "Invalid team name. (Red or Blue)", s);
					}
				} else {
					sendMessageToSource(p, "Player does not exist.", s);
				}
			} else if (command.equalsIgnoreCase("help")) {
				sendMessageToSource(p, "- - Help - -", s);
				sendMessageToSource(p, "    status - View server information", s);
				sendMessageToSource(p, "    stop - Stops the server cleanly", s);
				sendMessageToSource(p, "    who - Lists all online players", s);
				sendMessageToSource(p, "    say [Message] - Send a message to all players", s);
				sendMessageToSource(p, "    kick [Player] - Kicks a player", s);
				sendMessageToSource(p, "    give [Player] [Item] [Amount] - Give a player an item", s);
				sendMessageToSource(p, "    setteam [Player] [Red/Blue] - Set a player's team", s);
				sendMessageToSource(p, "    enableplugin [Name] - Enable a plugin", s);
				sendMessageToSource(p, "    disableplugin [Name] - Disable a plugin", s);
				sendMessageToSource(p, "    reloadplugin [Name] - Reload a plugin", s);
				sendMessageToSource(p, "    listplugins - List all plugins", s);
				sendMessageToSource(p, "    update - Update the server", s);





			} else {
				sendMessageToSource(p, "Command not found. Type 'help' for a list of commands.", s);
			}
		} catch (Exception e) {
			sendMessageToSource(p, "An error occured while processing this command.", s);
			e.printStackTrace();

		}
	}

	public static void sendMessageToSource(NetPlayer p, String s, CommandSource sc) {
		switch(sc) {
		case Server:
			Console.write(s);
			break;
		case Client:
			p.getSender().sendPacket(new Packet34SendServerCommandResponse(p, s));
			break;
		}
	}
}
