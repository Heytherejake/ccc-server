package com.studio878.server.input;

import java.util.ArrayList;

import com.studio878.server.app.Main;
import com.studio878.server.entities.Player;
import com.studio878.server.io.NetPlayer;
import com.studio878.server.listeners.PluginDispatch;
import com.studio878.server.listeners.Plugin;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.util.ChatColors;

public class PlayerCommandParser {
	public static boolean processCommand(Player player, String input) {
		if(input.length() == 0) {
			return true;
		}
		String[] split = input.split(" ");
		String command = split[0];
		if (command.equalsIgnoreCase("/enableplugin")) {
			if(split.length == 2) {
				if(PluginDispatch.loadPlugin(split[1] + ".jar")) {
					player.sendMessage(ChatColors.Green + "Plugin enabled.");
					Console.write(player.getName() + " enabled " + split[1] + ".jar");
				} else {
					player.sendMessage(ChatColors.Red + "Plugin is already enabled.");
				}
			} else {
				player.sendMessage(ChatColors.Red + "Invalid arguments");
			}
			return true;
		} else if (command.equalsIgnoreCase("/all")) {
			try {
			String message = input.split("/all ")[1];
			for(NetPlayer ps : Main.getDispatch().getClients()) {
				if(ps.getPlayer().getTeam() == player.getTeam()) {
					ps.getSender().sendPacket(new Packet08SendMessage(ps, ChatColors.Green + "[All] " +  player.getName() + ": " + ChatColors.White + message));
				} else {
					ps.getSender().sendPacket(new Packet08SendMessage(ps, ChatColors.Red + "[All] " +  player.getName() + ": " + ChatColors.White + message));
				}
			}
			} catch (Exception e) {
			}
			return true;
		} else if (command.equalsIgnoreCase("/slap")) {
			player.setVelocity(5, -3);
			return true;
		
		} else if (command.equalsIgnoreCase("/disableplugin")) {
			if(split.length == 2) {

				if(PluginDispatch.offloadPlugin(split[1])) {
					player.sendMessage(ChatColors.Green + "Plugin disabled.");
					Console.write(player.getName() + " disabled " + split[1] + ".jar");
				} else {
					player.sendMessage(ChatColors.Red + "Plugin is not registered.");
				}
			} else {
				player.sendMessage(ChatColors.Red + "Inavlid arguments");
			}
			return true;
		} else if (command.equalsIgnoreCase("/listplugins")) {
			ArrayList<Plugin> plugins = PluginDispatch.getListeners();
			if(plugins.size() == 0) {
				player.sendMessage(ChatColors.LightRed + "No plugins registered.");
			}
			String msg = "Enabled plugins: ";
			for(int i = 0; i < plugins.size(); i++) {
				msg += plugins.get(i).getPluginInfo().getPluginName() + ", ";
			}
			player.sendMessage(ChatColors.Green + msg.substring(0, msg.length() - 2));
			return true;
		} else if (command.equalsIgnoreCase("/reloadplugin")) {
			if(split.length == 2) {
					if(PluginDispatch.reloadPlugin(split[1])) {
					player.sendMessage(ChatColors.Green + "Plugin reloaded.");
					Console.write(player.getName() + " reloaded " + split[1] + ".jar");
				} else {
					player.sendMessage(ChatColors.Red + "Plugin is not registered.");
				}
			} else {
				player.sendMessage(ChatColors.Red + "Inavlid arguments");
			}
			return true;

		}
		return false;
	}
}
