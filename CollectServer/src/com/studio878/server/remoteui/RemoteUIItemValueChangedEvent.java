package com.studio878.server.remoteui;

import com.studio878.server.entities.Player;

public class RemoteUIItemValueChangedEvent extends RemoteUIEvent {

	int button;
	RemoteUIItem item;
	public RemoteUIItemValueChangedEvent(Player p, int x, int y, RemoteUIItem item) {
		super(p, x, y);
		this.item = item;
	}

	public RemoteUIItem getItem() {
		return item;
	}
	
	public int getButtonPressed() {
		return button;
	}
}
