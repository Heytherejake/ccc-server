package com.studio878.server.remoteui;

public interface RemoteUIEventManager {
	public void onClick(RemoteUIClickEvent ev);
	public void onMouseRelease(RemoteUIMouseReleaseEvent ev);

}
