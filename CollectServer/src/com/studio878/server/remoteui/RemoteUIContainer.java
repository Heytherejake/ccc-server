package com.studio878.server.remoteui;

import java.awt.Container;
import java.util.ArrayList;

import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet41SendRemoteUIContainer;
import com.studio878.server.packets.Packet42SendRemoteUIItemUpdate;
import com.studio878.server.packets.Packet43SendRemoteUIContainerCloseRequest;
import com.studio878.server.packets.Packet44SendRemoteUIContainerOpenRequest;

public class RemoteUIContainer {

	ArrayList<RemoteUIItem> items = new ArrayList<RemoteUIItem>();
	Object lock = new Object();
	int id;
	RemoteUIContainerEventManager eventManager;

	public RemoteUIContainer() {
		id = RemoteUIManager.getNextHighestContainerId();
		RemoteUIManager.registerContainer(this);
	}

	public RemoteUIContainer copy() {
		RemoteUIContainer copy = new RemoteUIContainer();
		for(RemoteUIItem i: this.items) {
			RemoteUIItem s = i.copy(copy);
			s.id = i.id;
			copy.items.add(s);
		}
		copy.id = this.id;
		copy.eventManager = this.eventManager;
		return copy;
	}

	public void addItem(RemoteUIItem s) {
		synchronized (lock) {
			s.setContainer(this);
			items.add(s);

		}
	}

	public RemoteUIItem getItem(int id) {
		synchronized (lock) {
			for(RemoteUIItem i : items) {
				if(i.getId() == id) {
					return i;
				}
			}
		}
		return null;
	}

	public RemoteUIItem getItem(RemoteUIItem is) {
		synchronized (lock) {
			for(RemoteUIItem i : items) {
				if(i.getId() == is.getId()) {
					return i;
				}
			}
		}
		return null;
	}

	public void removeItem(RemoteUIItem s) {
		synchronized (lock) {
			s.setContainer(null);
			items.remove(s);
		}
	}

	public void resetContainerForPlayer(Player p) {
		p.removeContainer(this);
		p.addContainer(this);
	}
	public void setEventManager(RemoteUIContainerEventManager em) {
		eventManager = em;
	}

	public RemoteUIContainerEventManager getEventManager() {
		return eventManager;
	}

	public int getId() {
		return id;
	}

	public void sendToPlayer(Player p) {
		synchronized(lock) {
			RemoteUIContainer c = copy();
			p.addContainer(c);
			p.getHandle().getSender().sendPacket(new Packet41SendRemoteUIContainer(p.getHandle(), c));

		}
	}

	public void sendItemUpdateToPlayer(Player p, RemoteUIItem item) {
		synchronized(lock) {
			RemoteUIContainer c = p.getContainer(id);
			if(c != null) {
				if(c.getItem(item.id) != null) {
					p.getHandle().getSender().sendPacket(new Packet42SendRemoteUIItemUpdate(p.getHandle(), item));
				} else {
					Console.error("[RemoteUI] Container with id: " + id + " does not containe item with id:" + item.getId());
				}
			}

		}
	}

	public void sendCloseRequestToPlayer(Player p) {
		synchronized (lock) {
			p.getHandle().getSender().sendPacket(new Packet43SendRemoteUIContainerCloseRequest(p.getHandle(), this));

		}
	}

	public void sendOpenRequestToPlayer(Player p) {
		synchronized (lock) {
			p.getHandle().getSender().sendPacket(new Packet44SendRemoteUIContainerOpenRequest(p.getHandle(), this));

		}
	}



	public String getDataString() {
		synchronized (lock) {
			StringBuilder sb = new StringBuilder();
			sb.append(id + ":");
			for(RemoteUIItem i: items) {
				sb.append(i.getDataString() + ":");
			}
			return sb.substring(0, sb.length() - 1);

		}
	}

}
