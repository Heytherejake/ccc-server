package com.studio878.server.remoteui;

import java.awt.Color;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUISolidColorBox extends RemoteUIItem{


	public RemoteUISolidColorBox(int x, int y, int width, int height, Color c) {
		super(x, y, width, height);
		if(c != null) {
			this.setValue(c.getRed() + "-" + c.getGreen() + "-" + c.getBlue() + "-" + c.getAlpha());
		}
		this.type = RemoteUIItemType.SolidColorBox;
	}

}
