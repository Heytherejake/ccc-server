package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;


public abstract class RemoteUIItem {

	int x, y, z, width, height, id;
	String value;
	boolean centered;
	int state;
	RemoteUIItemType type;
	RemoteUIItemEventManager eventManager;
	RemoteUIContainer container;

	public RemoteUIItem(int x, int y, int width, int height) {
		id = RemoteUIManager.getNextHighestItemId();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.state = 0;
		this.value = "";
		this.centered = false;

	}

	public RemoteUIItem copy(RemoteUIContainer c) {
		RemoteUIItem copy = getItemForType(type);
		copy.x = x;
		copy.y = y;
		copy.z = z;
		copy.width = width;
		copy.height = height;
		copy.id = id;
		copy.value = value;
		copy.centered = centered;
		copy.state = state;
		copy.type = type;
		copy.eventManager = eventManager;
		copy.container = c;
		return copy;
	}
	public int getId() {
		return id;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}


	public boolean isCentered() {
		return centered;
	}

	public RemoteUIItemType getType() {
		return type;
	}

	public int getState() {
		return state;
	}

	public RemoteUIItemEventManager getEventManager() {
		return eventManager;
	}

	public String getValue() {
		return value;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public void setState(int s) {
		state = s;
	}

	public void setWidth(int w) {
		width = w;
	}

	public void setHeight(int h) {
		height = h;
	}

	public void setValue(String s) {
		s = s.replace(":", "/U+003A");
		s = s.replace(",", "/U+002C");
		value = s;
	}

	public void setCentered(boolean c) {
		centered = c;
	}
	
	public RemoteUIContainer getParent() {
		return container;
	}

	public void setEventManager(RemoteUIItemEventManager ev) {
		eventManager = ev;
	}

	protected void setContainer(RemoteUIContainer container) {
		this.container = container;
	}
	public String getDataString() {
		return type.getId() + "," + id + "," + x + "," + y + "," + z + "," + width + "," + height + "," + state + "," + value + "," + centered;
	}

	public static RemoteUIItem getItemForType(RemoteUIItemType t) {
		switch(t) {
		case Button:
			return new RemoteUIButton(0, 0, 0, 0, null);
		case HttpImage:
			return new RemoteUIHttpImage(0, 0, "");
		case SolidColorBox:
			return new RemoteUISolidColorBox(0, 0, 0, 0, null);
		case TextField:
			return new RemoteUITextField(0, 0, 0, 0);
		case Slider:
			return new RemoteUISlider(0, 0, 0, 0);
		case Label:
			return new RemoteUILabel(0, 0, 0, 0, null);
		default:
			return null;
		}		
	}

}
