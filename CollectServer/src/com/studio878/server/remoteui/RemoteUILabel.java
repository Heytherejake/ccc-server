package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUILabel extends RemoteUIItem {

	public RemoteUILabel(int x, int y, int width, int height, String value) {
		super(x, y, width, height);
		this.value = value;
		this.type = RemoteUIItemType.Label;
	}

}
