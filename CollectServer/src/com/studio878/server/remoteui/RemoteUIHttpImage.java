package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUIHttpImage extends RemoteUIItem{

	
	public RemoteUIHttpImage(int x, int y, String url) {
		super(x, y, 0, 0);
		this.setValue(url);
		this.type = RemoteUIItemType.HttpImage;

	}

}
