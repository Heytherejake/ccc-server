package com.studio878.server.remoteui;

import com.studio878.server.entities.Player;

public class RemoteUIEvent {

	int x, y;
	Player player;
	
	public RemoteUIEvent(Player player, int x, int y) {
		this.x = x;
		this.y = y;
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
}
