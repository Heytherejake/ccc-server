package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUISlider extends RemoteUIItem {

	public static final int HIDE_AMOUNT = 0;
	public static final int SHOW_AMOUNT = 1;
	
	public RemoteUISlider(int x, int y, int width, int height) {
		super(x, y, width, height);
		this.type = RemoteUIItemType.Slider;
		this.value = "0";
	}
	
	public void setValue(int i) {
		value = "" + i;
	}
}
