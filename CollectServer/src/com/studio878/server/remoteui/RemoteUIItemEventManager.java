package com.studio878.server.remoteui;

public interface RemoteUIItemEventManager extends RemoteUIEventManager{
	public void onItemValueChanged(RemoteUIItemValueChangedEvent ev);
}
