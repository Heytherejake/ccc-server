package com.studio878.server.remoteui;

public interface RemoteUIContainerEventManager extends RemoteUIEventManager {
	public void onKeyPress(RemoteUIKeyPressEvent ev);
}
