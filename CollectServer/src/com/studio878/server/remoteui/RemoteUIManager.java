package com.studio878.server.remoteui;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet48ReceiveRemoteUIItemValue;

public class RemoteUIManager {

	public enum RemoteUIItemType {
		Button (0),
		HttpImage (1),
		SolidColorBox (2),
		TextField (3),
		Slider (4),
		Label (5),
		;

		int id;

		private RemoteUIItemType(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

	}

	private static CopyOnWriteArrayList<RemoteUIContainer> containers = new CopyOnWriteArrayList<RemoteUIContainer>();

	private static int highestItemId = 0;
	private static int highestContainerId = 0;


	public static RemoteUIContainer getContainer(int id) {
		for(RemoteUIContainer c : containers) {
			if(c.getId() == id) {
				return c;
			}
		}
		return null;
	}

	protected static void registerContainer(RemoteUIContainer c) {
		containers.add(c);
	}
	public static int getNextHighestItemId() {
		return highestItemId++;
	}

	public static int getNextHighestContainerId() {
		return highestContainerId++;
	}





}
