package com.studio878.server.remoteui;

import com.studio878.server.entities.Player;

public class RemoteUIKeyPressEvent extends RemoteUIEvent {

	int key;
	RemoteUIContainer container;
	public RemoteUIKeyPressEvent(Player player, RemoteUIContainer c, int x, int y, int key) {
		super(player, x, y);
		this.key = key;
		this.container = c;
	}
	
	public RemoteUIContainer getContainer() {
		return container;
	}
	
	public int getKeyPressed() {
		return key;
	}

}
