package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUITextField extends RemoteUIItem {

	public RemoteUITextField(int x, int y, int width, int height) {
		super(x, y, width, height);
		this.type = RemoteUIItemType.TextField;
	}

}
