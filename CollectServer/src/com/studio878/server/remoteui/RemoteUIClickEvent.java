package com.studio878.server.remoteui;

import com.studio878.server.entities.Player;

public class RemoteUIClickEvent extends RemoteUIEvent {

	int button;
	RemoteUIContainer container;
	public RemoteUIClickEvent(Player p, RemoteUIContainer c, int x, int y, int button) {
		super(p, x, y);
		this.button = button;
		this.container = c;
	}

	public RemoteUIContainer getContainer() {
		return container;
	}
	
	public int getButtonPressed() {
		return button;
	}
}
