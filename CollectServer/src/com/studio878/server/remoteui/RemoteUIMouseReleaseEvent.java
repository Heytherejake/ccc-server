package com.studio878.server.remoteui;

import com.studio878.server.entities.Player;

public class RemoteUIMouseReleaseEvent extends RemoteUIEvent {

	RemoteUIContainer container;
	public RemoteUIMouseReleaseEvent(Player p, RemoteUIContainer c, int x, int y) {
		super(p, x, y);
		this.container = c;
	}

	public RemoteUIContainer getContainer() {
		return container;
	}
	
}
