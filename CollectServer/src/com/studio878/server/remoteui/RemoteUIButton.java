package com.studio878.server.remoteui;

import com.studio878.server.remoteui.RemoteUIManager.RemoteUIItemType;

public class RemoteUIButton extends RemoteUIItem{
	public final int STATE_ENABLED = 0;
	public final int STATE_DISABLED = 1;

	public RemoteUIButton(int x, int y, int width, int height, String value) {
		super(x, y, width, height);
		this.value = value;
		this.type = RemoteUIItemType.Button;
	}
}
