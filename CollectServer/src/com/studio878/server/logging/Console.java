package com.studio878.server.logging;

import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.util.ChatColors;
import com.studio878.server.util.Settings;

public class Console {
	static CopyOnWriteArrayList<String> messages = new CopyOnWriteArrayList<String>();
	static boolean hasChanged = false;
	public static void write(String m) {
		if(m.indexOf('�') != -1) {
			m = m.replaceAll("�.", "");
		}
		
		if(hasChanged) {
			m = ChatColors.Yellow + "[Server] " + m;
		}
		messages.add(m);
		System.out.println(m);

	}
	
	public static void info(String m) {
		write("[INFO] " + m);
	}
	
	public static void warning(String m) {
		write("[WARNING] " + m);
	}
	public static void error(String m) {
		write("[ERROR] " + m);
	}
	public static void urgent(String m) {
		write("[URGENT] " + m);
	}
	
	public static void log(String m) {
		if(Settings.DebugMode) {
			write("[DEBUG] " + m);
		}
	}
	
	public static void setMessageQueue(CopyOnWriteArrayList<String> msg) {
		hasChanged = true;
		messages = msg;
	}
}
