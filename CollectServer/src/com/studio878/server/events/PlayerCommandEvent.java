package com.studio878.server.events;

import com.studio878.server.entities.Player;

public class PlayerCommandEvent extends Event {
	
	Player player;
	String[] split;
	String command;

	public PlayerCommandEvent(Player p, String command, String[] split) {
		player = p;
		this.split = split;
		this.command = command;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public String[] getCommandArray() {
		return split;
	}
	
	public String getCommand() {
		return command;
	}

}
