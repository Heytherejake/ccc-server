package com.studio878.server.events;

import com.studio878.server.entities.HealthEntity;

public class EntityKilledEvent extends Event{

	HealthEntity entity;
	
	public EntityKilledEvent(HealthEntity e) {
		entity = e;
	}
	
	public HealthEntity getEntity() {
		return entity;
	}
}
