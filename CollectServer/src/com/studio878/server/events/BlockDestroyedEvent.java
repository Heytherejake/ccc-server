package com.studio878.server.events;

import com.studio878.server.block.Block;
import com.studio878.server.entities.Player;

public class BlockDestroyedEvent extends Event{
	Player player;
	Block block;
	
	public BlockDestroyedEvent(Player player, Block block) {
		this.player = player;
		this.block = block;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Block getBlock() {
		return block;
	}

}
