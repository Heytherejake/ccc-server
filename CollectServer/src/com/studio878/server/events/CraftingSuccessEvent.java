package com.studio878.server.events;

import com.studio878.server.crafting.CraftingRecipe;
import com.studio878.server.entities.Player;

public class CraftingSuccessEvent extends Event{

	Player player;
	CraftingRecipe recipe;
	String message = "Crafting Failed";
	public CraftingSuccessEvent(Player p, CraftingRecipe r) {
		player = p;
		recipe = r;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public CraftingRecipe getRecipe() {
		return recipe;
	}
	
	public void setFailMessage(String s) {
		message = s;
	}
	
	public String getFailMessage() {
		return message;
	}
}
