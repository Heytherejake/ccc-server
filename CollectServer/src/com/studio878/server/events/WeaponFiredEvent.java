package com.studio878.server.events;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Item;

public class WeaponFiredEvent extends Event{
	Player player;
	Item item;
	
	public WeaponFiredEvent(Player player, Item item) {
		this.player = player;
		this.item = item;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Item getItem() {
		return item;
	}
}
