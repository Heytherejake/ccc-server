package com.studio878.server.events;

import com.studio878.server.entities.HealthEntity;

public class EntityHealEvent extends Event {

	HealthEntity entity;
	int amount;
	
	public EntityHealEvent(HealthEntity e, int amount) {
		this.entity = e;
		this.amount = amount;
	}
	
	public HealthEntity getEntity() {
		return entity;
	}
	
	public int getAmountChange() {
		return amount;
	}
}
