package com.studio878.server.events;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.Inventory;

public class InventoryRequestEvent extends Event{
	
	Player player;
	Inventory inventory;
	
	public InventoryRequestEvent(Player player, Inventory inv) {
		this.player = player;
		inventory = inv;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Inventory getInventory() {
		return inventory;
	}

}
