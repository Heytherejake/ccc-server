package com.studio878.server.events;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;

public class ProjectileThrownEvent extends Event{
	Player player;
	ItemID item;
	
	public ProjectileThrownEvent(Player player, ItemID item) {
		this.player = player;
		this.item = item;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public ItemID getItem() {
		return item;
	}
}
