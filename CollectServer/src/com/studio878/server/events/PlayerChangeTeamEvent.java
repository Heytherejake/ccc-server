package com.studio878.server.events;

import com.studio878.server.entities.Player;
import com.studio878.server.util.TeamManager.Team;

public class PlayerChangeTeamEvent extends Event{

	Player player;
	Team team;
	public PlayerChangeTeamEvent(Player p, Team team) {
		player = p;
		this.team = team;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Team getTeam() {
		return team;
	}
}
