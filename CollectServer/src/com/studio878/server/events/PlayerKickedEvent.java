package com.studio878.server.events;

import com.studio878.server.entities.Player;

public class PlayerKickedEvent extends Event{

	Player entity;
	
	public PlayerKickedEvent(Player e) {
		entity = e;
	}
	
	public Player getPlayer() {
		return entity;
	}
}
