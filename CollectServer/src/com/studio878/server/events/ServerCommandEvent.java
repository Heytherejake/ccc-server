package com.studio878.server.events;

public class ServerCommandEvent extends Event {
	
	String[] split;
	String command;

	public ServerCommandEvent(String command, String[] split) {
		this.split = split;
		this.command = command;
	}

	
	public String[] getCommandArray() {
		return split;
	}
	
	public String getCommand() {
		return command;
	}

}
