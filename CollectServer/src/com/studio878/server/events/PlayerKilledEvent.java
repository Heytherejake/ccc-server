package com.studio878.server.events;

import com.studio878.server.entities.HealthEntity;
import com.studio878.server.entities.Player;

public class PlayerKilledEvent extends Event{

	Player entity;
	
	public PlayerKilledEvent(Player e) {
		entity = e;
	}
	
	public Player getPlayer() {
		return entity;
	}
}
