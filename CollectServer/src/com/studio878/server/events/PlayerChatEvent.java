package com.studio878.server.events;

import com.studio878.server.entities.Player;

public class PlayerChatEvent extends Event {
	
	Player player;
	String message;
	public PlayerChatEvent(Player p, String s) {
		player = p;
		message = s;
	}

	public Player getPlayer() {
		return player;
	}
	
	public String getMessage() {
		return message;
	}
}
