package com.studio878.server.events;

public class PlayerJoinEvent extends Event{

	String username, kick;
	
	public PlayerJoinEvent(String user) {
		username = user;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setKickMessage(String message) {
		kick = message;
	}
	
	public String getKickMessage() {
		return kick;
	}
}
