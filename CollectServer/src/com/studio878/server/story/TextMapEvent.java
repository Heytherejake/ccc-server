package com.studio878.server.story;

import java.util.HashMap;

import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet61SendMapEvent;
import com.studio878.server.util.Damage.DamageMethod;

public class TextMapEvent extends MapEvent {

	public enum StoryTextSource {
		Unknown (0),
		;
		
		static HashMap<Integer, StoryTextSource> lookupId = new HashMap<Integer, StoryTextSource>();
		private int id;
		
		private StoryTextSource(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public static StoryTextSource lookupId(int id) {
			return lookupId.get(id);
		}
		
		static {
			for(StoryTextSource s: values()) {
				lookupId.put(s.getId(), s);
			}
		}
	}
	String message;
	StoryTextSource source;
	public TextMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		this.source = StoryTextSource.lookupId(Integer.parseInt(split[0]));
		this.message = split[1];
	}

	public boolean process(Player p) {
		sendToPlayer(p);
		return true;
	}

}
