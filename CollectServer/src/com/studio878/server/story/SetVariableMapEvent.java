package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.util.Settings;

public class SetVariableMapEvent extends MapEvent{
	int id;
	int amount;
	public SetVariableMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		id = Integer.parseInt(split[0]);
		amount = Integer.parseInt(split[1]);
	}

	public boolean process(Player p) {
		try {
			Settings.StoryVariables[id] = amount;
		} catch (Exception e) {
			Console.error("Error with Set Variable event");
			e.printStackTrace();
		}
		p.processNextMapEvent();

		return true;
	}
}
