package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.util.Settings;

public class GetVariableMapEvent extends MapEvent {
	int id;
	int amount;

	public GetVariableMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		id = Integer.parseInt(data.split("~")[0]);
		amount = Integer.parseInt(data.split("~")[1]);
	}

	public boolean process(Player p) {
		try {
			return Settings.StoryVariables[id] == amount;
		} catch (Exception e) {
			Console.error("Invalid Story Variable: " + id);
			
		}
		return false;
	}
}
