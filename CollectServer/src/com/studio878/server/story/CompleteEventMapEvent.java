package com.studio878.server.story;

import com.studio878.server.entities.Player;

public class CompleteEventMapEvent extends MapEvent {

	public CompleteEventMapEvent(MapEventType type, int level, MapEventChain parent) {
		super(type, " ", level, parent);
	}

	public boolean process(Player p) {
		parent.acted.add(p);
		if(p.getActiveChain() == parent) {
			p.setActiveChain(null);
		}
		p.processNextMapEvent();
		return true;
	}

}
