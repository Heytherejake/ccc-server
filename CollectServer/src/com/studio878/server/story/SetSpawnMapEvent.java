package com.studio878.server.story;

import java.awt.Point;

import com.studio878.server.entities.Player;
import com.studio878.server.util.TeamManager;
import com.studio878.server.util.TeamManager.Team;

public class SetSpawnMapEvent extends MapEvent {

	int x, y;
	
	public SetSpawnMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		x = Integer.parseInt(split[0]);
		y = Integer.parseInt(split[1]);
	}

	public boolean process(Player p) {
		TeamManager.setTeamRespawnPoint(Team.Blue, new Point(x, y));
		TeamManager.setTeamRespawnPoint(Team.Red, new Point(x, y));
		p.processNextMapEvent();
		return true;
	}

}
