package com.studio878.server.story;

import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;


public class IsEntityDeadMapEvent extends MapEvent {

	public IsEntityDeadMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
	}

	public boolean process(Player p) {
		try {
			for(Entity e : EntityManager.getEntities()) {
				if(e.getName().equalsIgnoreCase(data)) {
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			Console.error("Something stranged happened. -- IsEntityDeadMapEvent");
			
		}
		return false;
	}
}
