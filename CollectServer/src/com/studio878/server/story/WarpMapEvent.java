package com.studio878.server.story;

import com.studio878.server.entities.Player;

public class WarpMapEvent extends MapEvent {

	int x, y;
	
	public WarpMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		x = Integer.parseInt(split[0]);
		y = Integer.parseInt(split[1]);
	}

	public boolean process(Player p) {
		p.moveTo(x*16, y*16);
		p.processNextMapEvent();
		return true;
	}

}
