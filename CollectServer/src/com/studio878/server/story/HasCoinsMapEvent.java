package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;

public class HasCoinsMapEvent extends MapEvent {
	int amount;

	public HasCoinsMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		amount = Integer.parseInt(data);
	}

	public boolean process(Player p) {
		try {
			return p.getInventory().contains(ItemID.lookupName("Coins"), amount);
		} catch (Exception e) {
			Console.error("Invalid amount: " + amount);	
		}
		return false;
	}
}
