package com.studio878.server.story;

import java.util.HashMap;

import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet08SendMessage;
import com.studio878.server.packets.Packet56SendCriticalMessage;
import com.studio878.server.packets.Packet61SendMapEvent;
import com.studio878.server.util.Damage.DamageMethod;

public class ChatMessageMapEvent extends MapEvent {

	String message;
	public ChatMessageMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		this.message = data;
	}

	public boolean process(Player p) {
		p.getHandle().getSender().sendPacket(new Packet08SendMessage(p.getHandle(), message));
		p.processNextMapEvent();
		return true;
	}

}
