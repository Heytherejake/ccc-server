package com.studio878.server.story;

import com.studio878.server.entities.Player;

public class ElseMapEvent extends MapEvent {

	public ElseMapEvent(MapEventType type, int level, MapEventChain parent) {
		super(type, " ", level, parent);
	}

	public boolean process(Player p) {
		p.processNextMapEvent();
		return true;
	}
}
