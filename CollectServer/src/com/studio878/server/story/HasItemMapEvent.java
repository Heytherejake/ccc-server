package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;

public class HasItemMapEvent extends MapEvent {
	
	String item;
	int amount;

	public HasItemMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		item = data.split("~")[0];
		amount = Integer.parseInt(data.split("~")[1]);
	}

	public boolean process(Player p) {
		try {
			return p.getInventory().contains(ItemID.lookupName(item), amount);
		} catch (Exception e) {
			Console.error("Invalid Item ID: " + item);
			
		}
		return false;
	}
}
