package com.studio878.server.story;

import java.awt.Rectangle;
import java.util.concurrent.CopyOnWriteArrayList;

import com.studio878.server.entities.Player;

public class MapEventChain {

	int x1, y1, x2, y2;
	CopyOnWriteArrayList<MapEvent> events = new CopyOnWriteArrayList<MapEvent>();
	CopyOnWriteArrayList<Player> acted = new CopyOnWriteArrayList<Player>();
	public MapEventChain(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	public MapEventChain clone() {
		MapEventChain msc = new MapEventChain(x1, y1, x2, y2);
		msc.events = (CopyOnWriteArrayList<MapEvent>) events.clone();
		msc.acted = acted;
		return msc;
	}

	public CopyOnWriteArrayList<MapEvent> getEvents() {
		return events;
	}

	public void addEvent(MapEvent e) {
		events.add(e);
	}

	public boolean doesIntersect(Player p) {
		int x = x1*16;
		int y = y1*16;
		int w = (x2 - x1)*16;
		int h = (y2 - y1)*16;
		if(w <= 0) {
			x += w;
			w = -1*w;
		}
		if(h <= 0) {
			y += h;
			h = -1 * h;
		}
		Rectangle r1 = new Rectangle(x, y, w, h);
		Rectangle r2 = new Rectangle((int) p.getX(), (int) p.getY(), p.getWidth(), p.getHealth());
		return(r1.intersects(r2) && !acted.contains(p));
	}

	public void onIntersection(Player p) {
		if(!acted.contains(p)) {
			p.setActiveChain(this);
		}
	}
}
