package com.studio878.server.story;

import com.studio878.server.entities.Entity;
import com.studio878.server.entities.EntityManager;
import com.studio878.server.entities.EntityManager.EntityName;
import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet15AddEntity;

public class SpawnEntityMapEvent extends MapEvent{
	int x, y, id;
	String data = " ";
	public SpawnEntityMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		x = Integer.parseInt(split[0]);
		y = Integer.parseInt(split[1]);
		id = Integer.parseInt(split[2]);
		try {
			data = split[3];
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public boolean process(Player p) {
		Entity e = EntityManager.cloneEntity(EntityName.lookupId(id), x, y, data);
		if(e != null) {
			EntityManager.registerEntity(e);
			p.getHandle().getDispatch().broadcastPacket(new Packet15AddEntity(p.getHandle(), e));
		}
		p.processNextMapEvent();

		return true;
	}
}
