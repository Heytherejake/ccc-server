package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.util.Damage.DamageMethod;

public class DamageMapEvent extends MapEvent {

	int amount;
	
	public DamageMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		amount = Integer.parseInt(data);
	}

	public boolean process(Player p) {
		p.damage(amount, DamageMethod.Generic, p.getName());
		p.processNextMapEvent();

		return true;
	}

}
