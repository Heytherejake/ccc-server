package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.util.Damage.DamageMethod;

public class HealMapEvent extends MapEvent {

	int amount;
	
	public HealMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		amount = Integer.parseInt(data);
	}

	public boolean process(Player p) {
		p.heal(amount, DamageMethod.Generic, p.getName());
		p.processNextMapEvent();

		return true;
	}

}
