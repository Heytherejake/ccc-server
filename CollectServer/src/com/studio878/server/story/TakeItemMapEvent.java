package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;

public class TakeItemMapEvent extends MapEvent{
	
	String item;
	int amount;
	
	public TakeItemMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		item = split[0];
		amount = Integer.parseInt(split[1]);
	}

	public boolean process(Player p) {
		try {
			p.getInventory().subtract(ItemID.lookupName(item), 0, amount);
		} catch (Exception e) {
			Console.error("Error with TakeItemEvent");
			e.printStackTrace();
		}
		p.processNextMapEvent();

		return true;
	}
}
