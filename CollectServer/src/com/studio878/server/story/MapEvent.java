package com.studio878.server.story;

import java.util.HashMap;
import java.util.Map;

import com.studio878.server.entities.Player;
import com.studio878.server.logging.Console;
import com.studio878.server.packets.Packet61SendMapEvent;

public abstract class MapEvent {


	public enum MapEventType {
		Warp (0, false),
		SetVelocity(1, false),
		Damage(2, false),
		Heal(3, false),
		Text(4, false),
		Kill(5, false), 
		SetBlock(6, false),
		SetBackground(7, false),
		CompleteEvent(8, false), 
		LockControls(9, false),
		FreeControls(10, false),
		HasItem(12, true),
		Else(13, true),
		HasCoins(14, true),
		TakeItem(15, false),
		TakeCoins(16, false),
		GiveItem(17, false),
		CriticalMessage(18, false),
		ChatMessage(19, false),
		GetVariable(20, true),
		SetVariable(21, false),
		SpawnEntity(22, false),
		ShowInstructions(23, false),
		SetSpawn(24, false),
		IfEntityDead(25, true),

		;
		

		private int id;
		private static final Map<Integer, MapEventType> lookupId = new HashMap<Integer, MapEventType>();
		private boolean conditional;
		
		private MapEventType(int id, boolean conditional) {
			this.id = id;
			this.conditional = conditional;
		}
		
		public boolean isConditional() {
			return conditional;
		}
		
		public int getId() {
			return id;
		}
		
		public static MapEventType lookupId(int i) {
			return lookupId.get(i);
		}

		static {
			for(MapEventType t: values()) {
				lookupId.put(t.getId(), t);
			}
		}
	}
	
	
	MapEventType type;
	String data;
	int level;
	
	MapEventChain parent;
	
	public MapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		this.type = type;
		this.data = data;
		this.parent = parent;
		this.level = level;
	}
	
	public MapEventChain getParent() {
		return parent;
	}
	
	public MapEventType getType() {
		return type;
	}
	
	public String getData() {
		return data;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void sendToPlayer(Player p) {
		p.getHandle().getSender().sendPacket(new Packet61SendMapEvent(p.getHandle(), this));
	}
	
	public abstract boolean process(Player p);

	public static MapEvent matchEvent(MapEventType type, String data, int level, MapEventChain parent) {
		switch(type) {
		case Warp:
			return new WarpMapEvent(type, data, level, parent);
		case SetVelocity:
			return new SetVelocityMapEvent(type, data, level, parent);
		case Damage:
			return new DamageMapEvent(type, data, level, parent);
		case Heal:
			return new HealMapEvent(type, data, level, parent);
		case Text:
			return new TextMapEvent(type, data, level, parent);
		case Kill:
			return new KillMapEvent(type, level, parent);
		case SetBlock:
			return new SetBlockMapEvent(type, data, level, parent);
		case SetBackground:
			return new SetBackgroundMapEvent(type, data, level, parent);
		case CompleteEvent:
			return new CompleteEventMapEvent(type, level, parent);
		case LockControls:
			return new LockControlsMapEvent(type, level, parent);
		case FreeControls:
			return new FreeControlsMapEvent(type, level, parent);
		case HasItem:
			return new HasItemMapEvent(type, data, level, parent);
		case Else:
			return new ElseMapEvent(type, level, parent);
		case HasCoins:
			return new HasCoinsMapEvent(type, data, level, parent);
		case TakeItem:
			return new TakeItemMapEvent(type, data, level, parent);
		case TakeCoins:
			return new TakeCoinsMapEvent(type, data, level, parent);
		case GiveItem:
			return new GiveItemMapEvent(type, data, level, parent);
		case CriticalMessage:
			return new CriticalMessageMapEvent(type, data, level, parent);
		case ChatMessage:
			return new ChatMessageMapEvent(type, data, level, parent);
		case GetVariable:
			return new GetVariableMapEvent(type, data, level, parent);
		case SpawnEntity:
			return new SpawnEntityMapEvent(type, data, level, parent);
		case ShowInstructions:
			return new ShowInstructionsMapEvent(type, data, level, parent);
		case SetVariable:
			return new SetVariableMapEvent(type, data, level, parent);
		case SetSpawn:
			return new SetSpawnMapEvent(type, data, level, parent);
		case IfEntityDead:
			return new IsEntityDeadMapEvent(type, data, level, parent);
		default:
			Console.error("Unmatched event type: " + type);
		}
		return null;
	}

}
