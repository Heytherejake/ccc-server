package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.util.Damage.DamageMethod;

public class KillMapEvent extends MapEvent {

	
	public KillMapEvent(MapEventType type, int level,  MapEventChain parent) {
		super(type, "", level, parent);

	}

	public boolean process(Player p) {
		p.damage(1000, DamageMethod.Generic, p.getName());
		p.processNextMapEvent();

		return false;
	}
}
