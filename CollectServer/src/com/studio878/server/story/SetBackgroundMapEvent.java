package com.studio878.server.story;

import com.studio878.server.block.BlockType;
import com.studio878.server.entities.Player;
import com.studio878.server.world.Grid;

public class SetBackgroundMapEvent extends MapEvent{
	int x, y;
	BlockType block;
	public SetBackgroundMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		String[] split = data.split("~");
		x = Integer.parseInt(split[0]);
		y = Integer.parseInt(split[1]);
		block = BlockType.lookupId(Integer.parseInt(split[2]));
	}

	public boolean process(Player p) {
		Grid.getBlockAt(x, y).setBackgroundType(block);
		p.processNextMapEvent();

		return true;
	}
}
