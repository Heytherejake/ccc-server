package com.studio878.server.story;

import com.studio878.server.entities.Player;

public class FreeControlsMapEvent extends MapEvent {

	public FreeControlsMapEvent(MapEventType type, int level, MapEventChain parent) {
		super(type, " ", level, parent);
	}

	public boolean process(Player p) {
		sendToPlayer(p);
		p.processNextMapEvent();
		return true;
	}
}
