package com.studio878.server.story;

import com.studio878.server.entities.Player;
import com.studio878.server.inventory.ItemID;
import com.studio878.server.logging.Console;

public class TakeCoinsMapEvent extends MapEvent{
	int amount;
	public TakeCoinsMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		amount = Integer.parseInt(data);
	}

	public boolean process(Player p) {
		try {
			p.getInventory().subtract(ItemID.lookupName("Coins"), 0, amount);
		} catch (Exception e) {
			Console.error("Error with TakeCoinsEvent");
			e.printStackTrace();
		}
		p.processNextMapEvent();

		return true;
	}
}
