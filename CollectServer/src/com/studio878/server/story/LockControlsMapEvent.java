package com.studio878.server.story;

import com.studio878.server.entities.Player;

public class LockControlsMapEvent extends MapEvent {

	public LockControlsMapEvent(MapEventType type, int level, MapEventChain parent) {
		super(type, " ", level, parent);
	}

	public boolean process(Player p) {
		sendToPlayer(p);
		p.processNextMapEvent();
		return true;
	}
}
