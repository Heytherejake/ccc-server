package com.studio878.server.story;

import java.util.HashMap;

import com.studio878.server.entities.Player;
import com.studio878.server.packets.Packet61SendMapEvent;
import com.studio878.server.util.Damage.DamageMethod;

public class ShowInstructionsMapEvent extends MapEvent {

	String message;
	public ShowInstructionsMapEvent(MapEventType type, String data, int level, MapEventChain parent) {
		super(type, data, level, parent);
		this.message = data;
	}

	public boolean process(Player p) {
		sendToPlayer(p);
		return true;
	}

}
